package com.jiubanqingchen.wish.model.models.base;

import com.jfinal.plugin.activerecord.IBean;
import com.jfinal.plugin.activerecord.Model;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings({"serial", "unchecked"})
public abstract class BaseUserGroupRole<M extends BaseUserGroupRole<M>> extends Model<M> implements IBean {

	public M setUserGroupRoleId(java.lang.String userGroupRoleId) {
		set("userGroupRoleId", userGroupRoleId);
		return (M)this;
	}
	
	public java.lang.String getUserGroupRoleId() {
		return getStr("userGroupRoleId");
	}
	
	public M setRoleId(java.lang.String roleId) {
		set("roleId", roleId);
		return (M)this;
	}
	
	public java.lang.String getRoleId() {
		return getStr("roleId");
	}
	
	public M setUserGroupId(java.lang.String userGroupId) {
		set("userGroupId", userGroupId);
		return (M)this;
	}
	
	public java.lang.String getUserGroupId() {
		return getStr("userGroupId");
	}
	
	public M setUserGroupType(java.lang.String userGroupType) {
		set("userGroupType", userGroupType);
		return (M)this;
	}
	
	public java.lang.String getUserGroupType() {
		return getStr("userGroupType");
	}
	
	public M setCreatePerson(java.lang.String createPerson) {
		set("createPerson", createPerson);
		return (M)this;
	}
	
	public java.lang.String getCreatePerson() {
		return getStr("createPerson");
	}
	
	public M setCreateTime(java.util.Date createTime) {
		set("createTime", createTime);
		return (M)this;
	}
	
	public java.util.Date getCreateTime() {
		return getDate("createTime");
	}
	
	public M setModifyPerson(java.lang.String modifyPerson) {
		set("modifyPerson", modifyPerson);
		return (M)this;
	}
	
	public java.lang.String getModifyPerson() {
		return getStr("modifyPerson");
	}
	
	public M setModifyTime(java.util.Date modifyTime) {
		set("modifyTime", modifyTime);
		return (M)this;
	}
	
	public java.util.Date getModifyTime() {
		return getDate("modifyTime");
	}
	
}
