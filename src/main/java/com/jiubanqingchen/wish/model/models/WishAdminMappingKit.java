package com.jiubanqingchen.wish.model.models;

import com.jfinal.plugin.activerecord.ActiveRecordPlugin;

/**
 * Generated by JFinal, do not modify this file.
 * <pre>
 * Example:
 * public void configPlugin(Plugins me) {
 *     ActiveRecordPlugin arp = new ActiveRecordPlugin(...);
 *     WishAdminMappingKit.mapping(arp);
 *     me.add(arp);
 * }
 * </pre>
 */
public class WishAdminMappingKit {
	
	public static void mapping(ActiveRecordPlugin arp) {
		arp.addMapping("wish_app_action", "actionId", AppAction.class);
		arp.addMapping("wish_attachment", "fileId", Attachment.class);
		arp.addMapping("wish_cache", "cacheId", Cache.class);
		arp.addMapping("wish_cron_task", "cronTaskId", CronTask.class);
		arp.addMapping("wish_data_source", "dataSourceId", DataSource.class);
		arp.addMapping("wish_demo", "demoId", Demo.class);
		arp.addMapping("wish_dept", "deptId", Dept.class);
		arp.addMapping("wish_dictionary", "dictionaryId", Dictionary.class);
		arp.addMapping("wish_dictionary_type", "dictionaryTypeId", DictionaryType.class);
		arp.addMapping("wish_login_log", "loginLogId", LoginLog.class);
		arp.addMapping("wish_menu", "menuId", Menu.class);
		arp.addMapping("wish_notice", "noticeId", Notice.class);
		arp.addMapping("wish_resource_authority", "resourceAuthorityId", ResourceAuthority.class);
		arp.addMapping("wish_role", "roleId", Role.class);
		arp.addMapping("wish_user", "userId", User.class);
		arp.addMapping("wish_user_dept", "userDeptId", UserDept.class);
		arp.addMapping("wish_user_group_role", "userGroupRoleId", UserGroupRole.class);
	}
}

