package com.jiubanqingchen.wish.model.models.base;

import com.jfinal.plugin.activerecord.IBean;
import com.jfinal.plugin.activerecord.Model;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings({"serial", "unchecked"})
public abstract class BaseUserDept<M extends BaseUserDept<M>> extends Model<M> implements IBean {

	public M setUserDeptId(java.lang.String userDeptId) {
		set("userDeptId", userDeptId);
		return (M)this;
	}
	
	public java.lang.String getUserDeptId() {
		return getStr("userDeptId");
	}
	
	public M setUserId(java.lang.String userId) {
		set("userId", userId);
		return (M)this;
	}
	
	public java.lang.String getUserId() {
		return getStr("userId");
	}
	
	public M setDeptId(java.lang.String deptId) {
		set("deptId", deptId);
		return (M)this;
	}
	
	public java.lang.String getDeptId() {
		return getStr("deptId");
	}
	
	public M setMain(java.lang.Boolean main) {
		set("main", main);
		return (M)this;
	}
	
	public java.lang.Boolean getMain() {
		return get("main");
	}
	
	public M setValid(java.lang.Boolean valid) {
		set("valid", valid);
		return (M)this;
	}
	
	public java.lang.Boolean getValid() {
		return get("valid");
	}
	
	public M setTimeEnd(java.util.Date timeEnd) {
		set("timeEnd", timeEnd);
		return (M)this;
	}
	
	public java.util.Date getTimeEnd() {
		return getDate("timeEnd");
	}
	
	public M setCreateTime(java.util.Date createTime) {
		set("createTime", createTime);
		return (M)this;
	}
	
	public java.util.Date getCreateTime() {
		return getDate("createTime");
	}
	
	public M setCreatePerson(java.lang.String createPerson) {
		set("createPerson", createPerson);
		return (M)this;
	}
	
	public java.lang.String getCreatePerson() {
		return getStr("createPerson");
	}
	
	public M setModifyTime(java.util.Date modifyTime) {
		set("modifyTime", modifyTime);
		return (M)this;
	}
	
	public java.util.Date getModifyTime() {
		return getDate("modifyTime");
	}
	
	public M setModifyPerson(java.lang.String modifyPerson) {
		set("modifyPerson", modifyPerson);
		return (M)this;
	}
	
	public java.lang.String getModifyPerson() {
		return getStr("modifyPerson");
	}
	
}
