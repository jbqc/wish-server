package com.jiubanqingchen.wish.model.models;

import com.jiubanqingchen.wish.model.models.base.BaseDataSource;

/**
 * Generated by JFinal.
 */
@SuppressWarnings("serial")
public class DataSource extends BaseDataSource<DataSource> {
	public static final DataSource dao = new DataSource().dao();
}
