package com.jiubanqingchen.wish.model.models;


import com.jiubanqingchen.wish.model.models.base.BaseDept;

/**
 * Generated by JFinal.
 */
@SuppressWarnings({"serial"})
public class Dept extends BaseDept<Dept> {
	public static final Dept dao = new Dept().dao();
}
