package com.jiubanqingchen.wish.model.generator;

import com.jfinal.plugin.activerecord.generator.MetaBuilder;

import javax.sql.DataSource;

public class _QcMetaBuilder extends MetaBuilder {

    private String tableNamePrefixes =null;

    public _QcMetaBuilder(DataSource dataSource,String _tableName) {
        super(dataSource);
        this.tableNamePrefixes=_tableName;
    }

    /**
     * 通过继承并覆盖此方法，跳过一些不希望处理的 table，定制更加灵活的 table 过滤规则
     * @return 返回 true 时将跳过当前 tableName 的处理
     * true : 不处理
     * false ：处理
     */
    @Override
    protected boolean isSkipTable(String tableName) {
        if (tableNamePrefixes == null) return true;
        return !tableName.startsWith(tableNamePrefixes);
    }
}
