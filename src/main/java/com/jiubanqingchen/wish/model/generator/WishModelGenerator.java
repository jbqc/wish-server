package com.jiubanqingchen.wish.model.generator;

import com.jfinal.kit.PathKit;

public class WishModelGenerator {

    public static void main(String[] args) {
        create();
    }

    public static void create() {
        // base model 所使用的包名
        String baseModelPackageName = "com.jiubanqingchen.wish.model.models.base";
        // base model 文件保存路径
        String baseModelOutputDir = PathKit.getWebRootPath() + "/src/main/java/com/jiubanqingchen/wish/model/models/base";
        // model 所使用的包名 (MappingKit 默认使用的包名)
        String modelPackageName = "com.jiubanqingchen.wish.model.models";
        // model 文件保存路径 (MappingKit 与 DataDictionary 文件默认保存路径)
        String modelOutputDir = baseModelOutputDir + "/..";
        QcGenerator.generate(baseModelPackageName, baseModelOutputDir, modelPackageName, modelOutputDir,
                "wish_", "wish_", "WishAdminMappingKit");
    }
}
