package com.jiubanqingchen.wish.model.generator;

import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.dialect.MysqlDialect;
import com.jfinal.plugin.activerecord.generator.Generator;
import com.jfinal.plugin.druid.DruidPlugin;

import javax.sql.DataSource;

public class QcGenerator {

    /**
     * 得到数据源
     *
     * @return
     */
    public static DataSource getDataSource() {
        Prop p = PropKit.use("application.server.properties");
        DruidPlugin druidPlugin = new DruidPlugin(p.get("wish.datasource.jdbcUrl"), p.get("wish.datasource.user"), p.get("wish.datasource.password"));
        druidPlugin.start();
        return druidPlugin.getDataSource();
    }

    /**
     * 生成文件
     *
     * @param baseModelPackageName     baseModel 所在包地址
     * @param baseModelOutputDir       baseModel 文件保存路径
     * @param modelPackageName         model 所使用的包名 (MappingKit 默认使用的包名)
     * @param modelOutputDir           文件保存路径 (MappingKit 与 DataDictionary 文件默认保存路径)
     * @param tableNamePrefixes        生成表前缀(future_:只生成前缀为future_的表)
     * @param removedTableNamePrefixes 去除的前缀
     */
    public static void generate(String baseModelPackageName, String baseModelOutputDir, String modelPackageName, String modelOutputDir, String tableNamePrefixes, String removedTableNamePrefixes, String mappingKitClassName) {
        // 创建生成器
        Generator generator = new Generator(getDataSource(), baseModelPackageName, baseModelOutputDir, modelPackageName, modelOutputDir);

        // 配置是否生成备注
        generator.setGenerateRemarks(true);

        // 设置数据库方言
        generator.setDialect(new MysqlDialect());

        // 设置是否生成链式 setter 方法
        generator.setGenerateChainSetter(true);

        // 添加不需要生成的表名
        //generator.addExcludedTable("adv");

        //设置生成前缀为tableNamePrefixes的表
        generator.setMetaBuilder(new _QcMetaBuilder(getDataSource(), tableNamePrefixes));

        // 设置是否在 Model 中生成 dao 对象
        generator.setGenerateDaoInModel(true);

        // 设置是否生成字典文件
        generator.setGenerateDataDictionary(false);

        //设置mappingkit class文件名称
        if (StrKit.notBlank(mappingKitClassName)) generator.setMappingKitClassName(mappingKitClassName);

        // 设置需要被移除的表名前缀用于生成modelName。例如表名 "osc_user"，移除前缀 "osc_"后生成的model名为 "User"而非 OscUser
        generator.setRemovedTableNamePrefixes(removedTableNamePrefixes);

        // 生成
        generator.generate();
    }
}
