package com.jiubanqingchen.wish.publish;

import com.jfinal.server.undertow.UndertowServer;
import com.jiubanqingchen.wish.config.WishConfig;

/**
 * @author light_dust
 * @since 2021/3/11 11:36
 */
public class Run {
    public static void main(String[] args) {
        UndertowServer.create(WishConfig.class, "undertow.txt").configWeb(builder -> {
            builder.addWebSocketEndpoint("com.jiubanqingchen.wish.kit.WebSocketKit");
        }).start();
    }
}
