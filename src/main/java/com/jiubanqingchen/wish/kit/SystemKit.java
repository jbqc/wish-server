package com.jiubanqingchen.wish.kit;

import cn.hutool.http.useragent.UserAgent;
import cn.hutool.http.useragent.UserAgentUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.kit.HttpKit;
import com.jfinal.kit.PropKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Record;
import com.jiubanqingchen.wish.framework.exception.WishException;
import com.jiubanqingchen.wish.framework.exception.WishExceptionKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;

/**
 * @author light_dust
 * @since 2021/6/24 17:15
 */
public class SystemKit {

    private static final Logger logger = LoggerFactory.getLogger(SystemKit.class);

    private static final String[] HEADERS = {
            "X-Forwarded-For",
            "Proxy-Client-IP",
            "WL-Proxy-Client-IP",
            "HTTP_X_FORWARDED_FOR",
            "HTTP_X_FORWARDED",
            "HTTP_X_CLUSTER_CLIENT_IP",
            "HTTP_CLIENT_IP",
            "HTTP_FORWARDED_FOR",
            "HTTP_FORWARDED",
            "HTTP_VIA",
            "REMOTE_ADDR",
            "X-Real-IP"
    };

    /**
     * 判断ip是否为空，空返回true
     *
     * @param ip ip
     */
    public static boolean isEmptyIp(final String ip) {
        return (ip == null || ip.length() == 0 || ip.trim().equals("") || "unknown".equalsIgnoreCase(ip));
    }


    /**
     * 判断ip是否不为空，不为空返回true
     *
     * @param ip ip
     */
    public static boolean isNotEmptyIp(final String ip) {
        return !isEmptyIp(ip);
    }

    /***
     * 获取客户端ip地址(可以穿透代理)
     * @param request HttpServletRequest
     */
    public static String getClientIpAddress(HttpServletRequest request) {
        String ip = "";
        for (String header : HEADERS) {
            ip = request.getHeader(header);
            if (isNotEmptyIp(ip)) {
                break;
            }
        }
        if (isEmptyIp(ip)) {
            ip = request.getRemoteAddr();
        }
        if (isNotEmptyIp(ip) && ip.contains(",")) {
            ip = ip.split(",")[0];
        }
        if ("0:0:0:0:0:0:0:1".equals(ip)) {
            ip = "127.0.0.1";
        }
        return ip;
    }


    /**
     * 得到客户端ip地址区域信息
     *
     * @param ip ip
     */
    public static String getClientIpArea(String ip) throws Exception {
        String amapUrl=PropKit.get("wish.amap.url","https://restapi.amap.com/v3/ip");
        if(StrKit.isBlank(amapUrl)){
            throw new WishException(WishExceptionKey.KIT_AMAP_URL_NOT_FOUND);
        }
        String key =  PropKit.get("wish.amap.key");
        if(StrKit.isBlank(amapUrl)){
            throw new WishException(WishExceptionKey.KIT_AMAP_KEY_NOT_FOUND);
        }
        String url = amapUrl+"?ip=" + ip + "&key=" +key;
        String jsonString = HttpKit.get(url);
        JSONObject json = JSON.parseObject(jsonString);
        if ("1".equals(json.getString("status"))) {//高德 status 1:成功 0 失败
            return json.getString("province") + " " + json.getString("city");
        } else {
            throw new Exception(json.get("info").toString());
        }
    }

    /**
     * 获取请求UserAgent
     *
     * @param request 请求
     */
    public static UserAgent getUserAgent(HttpServletRequest request) {
        //获取浏览器信息
        String ua = request.getHeader("User-Agent");
        //转成UserAgent对象
        return  UserAgentUtil.parse(ua);
    }

    /**
     * 得到ip信息
     *
     * @param request 请求
     */
    public static Record getIpInfo(HttpServletRequest request) {
        //获取用户ip地址
        String ip= getClientIpAddress(request);
        Record record = new Record().set("ipAddress", ip);
        if (!"127.0.0.1".equals(ip)) { //获取ip信息
            try {
                record.set("ipArea", getClientIpArea(ip));
            }catch (Exception e){
                logger.error(e.getMessage());
                e.printStackTrace();
            }
        } else {
            record.set("ipArea", "局域网");
        }
        return record;
    }
}
