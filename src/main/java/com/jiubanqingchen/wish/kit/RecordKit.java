package com.jiubanqingchen.wish.kit;

import cn.hutool.core.util.ArrayUtil;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Record;

import java.util.List;

/**
 * record 支持类
 *
 * @author light_dust
 * @since 2021/1/2 20:28
 */
public class RecordKit {
    private static final RecordKit me = new RecordKit();

    public static RecordKit me() {
        return me;
    }

    /**
     * 显示record中的指定字段
     *
     * @param record  record记录
     * @param columns 显示列 ,分割
     */
    public void showColumnsForRecord(Record record, String columns) {
        if (record == null || StrKit.isBlank(columns)) return;
        String[] showColumnArr = columns.split(",");
        String[] columnArr = record.getColumnNames();
        for (String column : columnArr) {
            if (!ArrayUtil.contains(showColumnArr, column)) {
                record.remove(column);
            }
        }
    }

    /**
     * 显示record集合中的指定字段
     *
     * @param recordList record集合
     * @param columns    显示列 ,分割
     */
    public void showColumnsForRecordList(List<Record> recordList, String columns) {
        if (BlankKit.isBlankList(recordList) || StrKit.isBlank(columns)) return;
        recordList.forEach(record -> showColumnsForRecord(record, columns));
    }

    /**
     * 隐藏record中的指定字段
     *
     * @param record  record记录
     * @param columns 隐藏列 ,分割
     */
    public void hideColumnsForRecord(Record record, String columns) {
        if (record == null || StrKit.isBlank(columns)) return;
        String[] hideColumnArr = columns.split(",");
        String[] columnArr = record.getColumnNames();
        for (String column : columnArr) {
            if (ArrayUtil.contains(hideColumnArr, column)) {
                record.remove(column);
            }
        }
    }

    /**
     * 隐藏record集合中的指定字段
     *
     * @param recordList record集合
     * @param columns    隐藏列 ,分割
     */
    public void hideColumnsForRecordList(List<Record> recordList, String columns) {
        if (BlankKit.isBlankList(recordList) || StrKit.isBlank(columns)) return;
        recordList.forEach(record -> hideColumnsForRecord(record, columns));
    }
}
