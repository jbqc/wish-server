package com.jiubanqingchen.wish.kit;

import java.util.List;

/**
 * 空值判断
 * @author light_dust
 * @since 2020/12/20 12:28
 */
public class BlankKit {

    /**
     *判断list是否为空
     * @param list 集合
     */
    public static boolean isBlankList(List<?> list) {
        return list == null || list.isEmpty();
    }

    /**
     * 判断list非空
     * @param list 集合

     */
    public static boolean notBlankList(List<?> list){
        return !isBlankList(list);
    }
}


