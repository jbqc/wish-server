package com.jiubanqingchen.wish.kit;

import java.text.NumberFormat;

/**
 * @author light_dust
 * @since 2021/5/17 17:20
 */
public class IntKit {

    public static double calcPercent(int num1, int num2) {
        return calcPercent(num1, num2, 2);
    }

    public static double calcPercent(int num1, int num2, int retain) {
        // 创建一个数值格式化对象
        NumberFormat numberFormat = NumberFormat.getInstance();
        // 设置精确到小数点后几位
        numberFormat.setMaximumFractionDigits(retain);
        return Double.parseDouble(numberFormat.format((float) num1 / (float) num2 * 100));
    }

}
