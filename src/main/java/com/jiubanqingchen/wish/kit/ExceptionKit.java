package com.jiubanqingchen.wish.kit;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * @author light_dust
 * @since 2021/5/27 22:03
 */
public class ExceptionKit {
    public static String getStackTrace(Throwable throwable) {
        if (throwable == null) return null;
        StringWriter sw = new StringWriter();
        try (PrintWriter pw = new PrintWriter(sw)) {
            throwable.printStackTrace(pw);
            return sw.toString();
        }
    }

}
