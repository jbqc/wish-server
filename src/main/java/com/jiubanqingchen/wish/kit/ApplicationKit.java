package com.jiubanqingchen.wish.kit;


import com.jfinal.kit.PropKit;

/**
 * 获取 application.properties 中的属性 为了方便调用
 *
 * @author light_dust
 * @since 2020/12/31 15:27
 */
public class ApplicationKit {
    private static final ApplicationKit me = new ApplicationKit();

    public static ApplicationKit me() {
        return me;
    }

    /**
     * 得到token生效时间
     */
    public int getTokenEffective() {
        return PropKit.getInt("wish.token.effective", 24 * 60 * 60);
    }

    /**
     * 得到同一账号同时最大在线数
     */
    public int getOnlineUserMax() {
        int max = PropKit.getInt("wish.online.user.max", 1);
        return max > 0 ? max : 1;
    }

    /**
     * 得到上传路径
     */
    public String getUploadPath() {
        return PropKit.get("wish.upload.path","C:/wish-server/upload");
    }

    /**
     * 得到定时任务是否开启
     */
    public boolean getTaskEnable() {
        return PropKit.getBoolean("wish.task.enable", false);
    }

    /**
     * 是否开启国际化
     */
    public boolean getI18nEnable() {
        return PropKit.getBoolean("wish.i18n.enable", false);
    }

    /**
     * 是否开启登录日志
     */
    public boolean getLogLoginEnable() {
        return PropKit.getBoolean("wish.log.login.enable", false);
    }

    /**
     * 用户默认初始化密码
     */
    public String getUserInitPassword(){
        return PropKit.get("wish.user.init.password", "wish@2021");
    }

    /**
     * 获取默认的redis缓存名称
     */
    public String getRedisCacheName(){
        return PropKit.get("wish.cache.redis.name", "system");
    }
}
