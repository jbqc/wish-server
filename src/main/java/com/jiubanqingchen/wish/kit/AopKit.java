package com.jiubanqingchen.wish.kit;

/**
 * aop 工具类
 * @author light_dust
 * @since 2020/10/9 17:54
 * @version 0.0.1
 */
public class AopKit {

    private static final AopKit me = new AopKit();

    public static AopKit me() {
        return me;
    }

    /**
     * 得到原始类型
     * @param clazz 被增强过的class
     * @since 2020/10/9 17:55
     **/
    public Class<?> getUsefulClass(Class<?> clazz) {
        return clazz.getName().indexOf("$$EnhancerBy") == -1 ? clazz : clazz.getSuperclass();
    }
}
