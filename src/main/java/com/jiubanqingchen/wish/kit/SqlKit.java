package com.jiubanqingchen.wish.kit;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.kit.Kv;
import com.jfinal.kit.StrKit;

/**
 * SQL支持
 * @author light_dust
 * @since 2020/12/24 17:06
 */
public class SqlKit {
    /**
     * 处理orderBy参数
     *
     */
    public static Kv orderHandle(Kv kv) {
        String orders = kv.getStr("orders");
        if (StrKit.notBlank(orders)) {
            JSONObject jb = JSONObject.parseObject(orders);
            String order = jb.getString("order");
            String field = jb.getString("field");
            if (StrKit.notBlank(field)) {
                if ("descend".equals(order)) {
                    order = "desc";
                } else {
                    order = "asc";
                }
                kv.set("prop", field);
                kv.set("sort", order);
            }
            kv.delete("orders");
        }
        return kv;
    }
}
