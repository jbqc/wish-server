package com.jiubanqingchen.wish.kit;

import com.jfinal.plugin.activerecord.Record;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author light_dust
 * @since 2021/4/27 11:50
 */
public class TreeKit {
    private String childCode;// 子 字段

    private String parentCode;// 父 字段

    private String childName;// 子 集合名称

    public TreeKit() {
        this.childCode = "id";
        this.parentCode = "parentId";
        this.childName = "children";
    }

    public TreeKit(String childCode) {
        this.childCode = childCode;
        this.parentCode = "parentId";
        this.childName = "children";
    }

    public TreeKit(String childCode, String childName) {
        this.childCode = childCode;
        this.parentCode = "parentId";
        this.childName = childName;
    }

    public TreeKit(String childCode, String parentCode, String childName) {
        this.childCode = childCode;
        this.parentCode = parentCode;
        this.childName = childName;
    }

    /**
     * 得到tree数据
     * @param list 列表集合
     * @param parentId 父亲id
     */
    public List<Record> getTreeData(List<Record> list, String parentId) {
        for (Record record : list) {
            String pid = record.getStr(parentCode);
            if (parentId == null) {//parentId 父id为空的情况需要支持
                if (pid != null) continue;
                List<Record> childList = getChildList(list, record.getStr(childCode));
                if (childList.size() > 0) record.set(childName, childList);
            } else if (parentId.equals(pid)) {
                List<Record> childList = getChildList(list, record.getStr(childCode));
                if (childList.size() > 0) record.set(childName, childList);
            }
        }
        return removeList(list, parentId);
    }

    private List<Record> getChildList(List<Record> list, String parentId) {
        List<Record> childLists = new ArrayList<>();
        for (Record record : list) {
            String pid = record.getStr(parentCode);
            if (parentId == null) {//parentId 父id为空的情况需要支持
                if (pid != null) continue;
                List<Record> childList = getChildList(list, record.getStr(childCode));
                if (childList.size() > 0) record.set(childName, childList);
                childLists.add(record);
            } else if (parentId.equals(pid)) {
                List<Record> childList = getChildList(list, record.getStr(childCode));
                if (childList.size() > 0) record.set(childName, childList);
                childLists.add(record);
            }
        }
        return childLists;
    }

    private List<Record> removeList(List<Record> list, String parentId) {
        Iterator<Record> it = list.iterator();
        while (it.hasNext()) {
            Record record = it.next();
            String pid = record.getStr(parentCode);
            if (parentId == null) {
                if (pid != null) it.remove();
            } else if (!parentId.equals(pid)) {
                it.remove();
            }
        }
        return list;
    }

    public String getChildCode() {
        return childCode;
    }

    public void setChildCode(String childCode) {
        this.childCode = childCode;
    }

    public String getParentCode() {
        return parentCode;
    }

    public void setParentCode(String parentCode) {
        this.parentCode = parentCode;
    }

    public String getChildName() {
        return childName;
    }

    public void setChildName(String childName) {
        this.childName = childName;
    }
}
