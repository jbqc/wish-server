package com.jiubanqingchen.wish.kit;

/**
 * 表达式工具类支持
 *
 * @author light_dust
 * @since 2021/5/16 20:45
 */
public class ExpressionKit {
    /**
     * 替换表达式
     *
     * @param text       字符串
     * @param expression 被替换的表达式
     * @param paras      参数
     */
    public static String replaceExpressions(String text, String expression, Object... paras) {
        String s = text;
        for (Object para : paras) {
            s = s.replaceFirst(expression, para.toString());
        }
        return s;
    }

    /**
     * 替换括号
     *
     * @param text       字符串
     * @param paras      参数
     */
    public static String replaceBrackets(String text, Object... paras) {
        return replaceExpressions(text, "\\{\\}", paras);
    }
}
