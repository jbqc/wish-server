package com.jiubanqingchen.wish.kit;

import cn.hutool.core.util.ArrayUtil;
import com.jfinal.kit.JsonKit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 实时通信
 * @author light_dust
 * @since 2021/5/14 10:17
 */
@ServerEndpoint(value = "/webSocket.ws/{socketId}")
public class WebSocketKit {
    private static final Logger logger = LoggerFactory.getLogger(WebSocketKit.class);
    private static final Map<String, WebSocketKit> clients = new ConcurrentHashMap<>();
    private Session session;
    private String socketId;

    /**
     * 发送信息给某个人
     * @param   message 信息
     * @param    socketId 在线id
     * @author light_dust
     * @since 2019年10月21日 下午9:38:38
     */
    public static void sendMessageTo(Object message, String socketId)  {
        for (WebSocketKit item : clients.values()) {
            if (item.socketId.equals(socketId))
                item.session.getAsyncRemote().sendText(JsonKit.toJson(message));
        }
    }

    /**
     * 发送消息给某些用户
     * @param message 信息
     * @param socketIds 在线
     */
    public static void sendMessageTo(Object message, String[] socketIds)  {
        if (ArrayUtil.isEmpty(socketIds)) return;
        for (String socketId : socketIds) sendMessageTo(message, socketId);
    }

    /**
     * 给所有在线的webSocket用户发送信息
     * @param   message 信息
     * @author light_dust
     * @since 2019年10月21日 下午9:41:15
     */
    public static void sendMessageAll(Object message) {
        for (WebSocketKit item : clients.values()) {
            item.session.getAsyncRemote().sendText(JsonKit.toJson(message));
        }
    }

    /**
     * 得到在线的用户id信息
     *
     */
    public static List<String> getCurrentSocketIds() {
        return new ArrayList<>(clients.keySet());
    }

    @OnOpen
    public void onOpen(@PathParam("socketId") String socketId, Session session)  {
        this.socketId = socketId;
        this.session = session;
        clients.put(socketId, this);
        logger.info("webSocket {} 已连接",socketId);
    }

    @OnClose
    public void onClose()  {
        clients.remove(socketId);
        logger.info("webSocket {} 连接已关闭",socketId);
    }

    @OnMessage
    public void onMessage(String message)  {
        logger.info(message);
    }

    @OnError
    public void onError(Session session, Throwable error) {
        error.printStackTrace();
    }
}
