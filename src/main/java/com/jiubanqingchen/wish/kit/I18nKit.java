package com.jiubanqingchen.wish.kit;

import com.jfinal.i18n.I18n;
import com.jfinal.i18n.Res;
import com.jiubanqingchen.wish.framework.context.locale.LocaleContextHolder;

import java.util.Locale;

/**
 * 国际化工具
 *
 * @author light_dust
 * @since 2021/5/26 10:15
 */
public class I18nKit {

    public static Res getRes() {
        return I18n.use(LocaleContextHolder.getLocale().toString());
    }

    /**
     * 获取国际化后的值
     * @param key 国际化的key
     */
    public static String getLocalMessage(String key) {
        return getRes().get(key);
    }

    /**
     * 获取国际化后的值
     * @param key 国际化的key
     * @param paras 参数 可以多次使用
     * <pre>
     *       我是{0},我喜欢{1},我爱{1}  =>getLocalMessage(key,'轻尘','睡觉') =>我是轻尘,我喜欢睡觉,我爱睡觉
     * </pre>
     */
    public static String getLocalMessage(String key, Object... paras) {
        return getRes().format(key, paras);
    }

    public static Locale getLocaleFromString(String localeString) {
        //这里统一纠正一下 i18n i10n导致的问题 vue-i18n本地化 玛德一会_ 一会-
        String[] array = localeString.replace("_", "-").split("-");
        String language = array[0];
        String country = array.length > 1 ? array[1] : "";
        return new Locale(language, country);
    }
}
