package com.jiubanqingchen.wish.external.jxhx;

import com.jfinal.aop.Inject;
import com.jfinal.core.Path;
import com.jiubanqingchen.wish.framework.annotation.AuthorityKey;
import com.jiubanqingchen.wish.framework.base.controller.BaseController;

/**
 * @author light_dust
 * @since 2021/6/30 17:34
 */
@AuthorityKey("external:jxhxBlog")
@Path("/external/jxhxBlog")
public class JxhxBlogController extends BaseController {
    @Inject
    JxhxBlogService jxhxBlogService;

    @AuthorityKey("view")
    public void getBlogList() {
        renderJson(jxhxBlogService.getBlogList());
    }
}
