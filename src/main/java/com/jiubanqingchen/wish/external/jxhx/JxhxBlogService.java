package com.jiubanqingchen.wish.external.jxhx;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Record;
import com.jiubanqingchen.wish.framework.annotation.SqlKey;
import com.jiubanqingchen.wish.framework.base.service.BaseService;
import com.jiubanqingchen.wish.kit.BlankKit;

import java.util.List;

/**
 * @author light_dust
 * @since 2021/6/30 17:41
 */
@SqlKey("external_jxhxBlog.list")
public class JxhxBlogService extends BaseService {

    public List<Record> getBlogList() {
        List<Record> recordList = list("WISH-BLOG", null);
        if (BlankKit.notBlankList(recordList)) recordList.forEach(this::articleHandler);
        return recordList;
    }

    /**
     * 处理文章信息
     *
     * @param record 文章对象
     */
    private void articleHandler(Record record) {
        String cdnUrl = "https://cdn.jiubanqingchen.cn";
        //文章图片
        if (StrKit.isBlank(record.get("imgUrl"))) {
            record.set("articleImageUrl", cdnUrl + "/wish-blog/images/jiangshuying001.jpeg?imageMogr2/format/webP");
        } else {
            record.set("articleImageUrl", cdnUrl + record.get("imgUrl") + "?imageMogr2/format/webP");
        }
        //文章访问地址
        record.set("visitUrl", "https://www.jiubanqingchen.cn/article/" + record.get("visitName") + ".html");
    }
}
