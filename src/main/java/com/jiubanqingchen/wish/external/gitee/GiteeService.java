package com.jiubanqingchen.wish.external.gitee;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.kit.HttpKit;
import com.jfinal.kit.PropKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Record;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author light_dust
 * @since 2021/7/5 8:47
 */
public class GiteeService {

    /**
     * 获取公开的仓库信息
     * @return
     */
    public List<Record> getPublicRepos() {
        Map<String, String> queryParam = new HashMap<>();
        String repoUrl = PropKit.get("gitee.user.repos.url","https://gitee.com/api/v5/user/repos");
        String accessToken = PropKit.get("gitee.access.token");
        if (!StrKit.notBlank(repoUrl, accessToken)) return null;
        queryParam.put("access_token", accessToken);
        queryParam.put("visibility", "public");
        queryParam.put("sort", "full_name");
        queryParam.put("page", "1");
        queryParam.put("per_page", "20");
        String message = HttpKit.get(repoUrl, queryParam);
        JSONArray jsonArray = JSONObject.parseArray(message);
        if (jsonArray == null || jsonArray.isEmpty()) return null;

        List<Record> recordList = new ArrayList<>();
        Record record;
        for (int i = 0; i < jsonArray.size(); i++) {
            JSONObject jb = jsonArray.getJSONObject(i);
            record = new Record();
            record.set("name", jb.getString("name"));
            record.set("language", jb.getString("language"));
            record.set("description", jb.getString("description"));
            record.set("url", jb.getString("html_url").replace(".git",""));
            //关注 分支 star
            record.set("watchers", jb.getString("watchers_count"));
            record.set("forks", jb.getString("forks_count"));
            record.set("stargazers", jb.getString("stargazers_count"));

            record.set("forksUrl", jb.getString("forks_url"));
            record.set("stargazersUrl", jb.getString("stargazers_url"));
            recordList.add(record);
        }
        return recordList;
    }
}
