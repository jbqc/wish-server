package com.jiubanqingchen.wish.external.gitee;

import com.jfinal.aop.Aop;
import com.jfinal.aop.Clear;
import com.jfinal.core.Path;
import com.jiubanqingchen.wish.framework.annotation.AuthorityKey;
import com.jiubanqingchen.wish.framework.base.controller.BaseController;

/**
 * @author light_dust
 * @since 2021/7/5 8:46
 */
@Clear
@Path("/external/gitee")
@AuthorityKey("external:gitee")
public class GiteeController extends BaseController {
    @AuthorityKey("view")
    public void getPublicRepos() {
        renderJson(Aop.get(GiteeService.class).getPublicRepos());
    }
}
