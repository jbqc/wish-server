package com.jiubanqingchen.wish.job;

import com.jfinal.ext.kit.DateKit;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.util.Date;

/**
 * @author light_dust
 * @since 2021/5/7 22:59
 */
public class TestJob implements Job {
    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        System.out.println(DateKit.toStr(new Date(), DateKit.timeStampPattern) + " :: " + "hello jiubanqingchen");
    }
}
