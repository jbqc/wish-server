package com.jiubanqingchen.wish.job;

import com.jfinal.ext.kit.DateKit;
import com.jfinal.plugin.activerecord.Record;
import com.jiubanqingchen.wish.kit.WebSocketKit;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.util.Date;

/**
 * @author light_dust
 * @since 2021/5/8 21:25
 */
public class MessageJob implements Job {
    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        Record record=new Record().set("date",DateKit.toStr(new Date(),DateKit.timeStampPattern)).set("msg","ha ha ha ha");
        WebSocketKit.sendMessageAll(record);
    }
}
