package com.jiubanqingchen.wish.org.dept;

import com.jfinal.core.Path;
import com.jiubanqingchen.wish.framework.annotation.AuthorityKey;
import com.jiubanqingchen.wish.framework.base.controller.ModelController;
import com.jiubanqingchen.wish.model.models.Dept;

/**
 * @author light_dust
 * @since 2021/1/4 11:48
 */
@Path("/org/dept")
@AuthorityKey("org:dept")
public class DeptController extends ModelController<DeptService, Dept> {

}
