package com.jiubanqingchen.wish.org.dept;


import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Record;
import com.jiubanqingchen.wish.framework.annotation.SqlKey;
import com.jiubanqingchen.wish.framework.base.service.ModelService;
import com.jiubanqingchen.wish.framework.base.user.IUser;
import com.jiubanqingchen.wish.model.models.Dept;

/**
 * @author light_dust
 * @since 2021/1/4 11:48
 */
@SqlKey("org_dept.list")
public class DeptService extends ModelService<Dept> {
    @Override
    public Dept newModel(Record record, IUser _usr) throws Exception {
        Dept dept = super.newModel(record, _usr);
        String parentDeptId = record.getStr("parentDeptId");
        if (StrKit.notBlank(parentDeptId)) {
            Dept parentDept = Dept.dao.findById(parentDeptId);
            dept.put("parentId", parentDeptId);
            dept.put("parentDeptName", parentDept.getDeptName());
        }
        return dept;
    }
}
