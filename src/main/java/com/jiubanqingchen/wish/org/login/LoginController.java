package com.jiubanqingchen.wish.org.login;

import com.jfinal.aop.Clear;
import com.jfinal.aop.Inject;
import com.jfinal.core.Path;
import com.jiubanqingchen.wish.framework.base.controller.BaseController;
import com.jiubanqingchen.wish.interceptors.AuthorityInterceptor;
import com.jiubanqingchen.wish.interceptors.LoginInterceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author light_dust
 * @since 2020/12/30 15:40
 */
@Path("/login")
public class LoginController extends BaseController {
    private final Logger logger = LoggerFactory.getLogger(LoginController.class);
    @Inject
    LoginService loginService;

    @Clear({LoginInterceptor.class, AuthorityInterceptor.class})
    public void index() throws Exception {
        renderJson(loginService.login(getRequest(),getKv()));
    }
}
