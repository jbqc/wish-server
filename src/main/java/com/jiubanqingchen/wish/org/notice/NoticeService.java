package com.jiubanqingchen.wish.org.notice;

import com.jfinal.plugin.activerecord.Record;
import com.jiubanqingchen.wish.framework.annotation.SqlKey;
import com.jiubanqingchen.wish.framework.base.service.ModelService;
import com.jiubanqingchen.wish.framework.base.user.IUser;
import com.jiubanqingchen.wish.kit.BlankKit;
import com.jiubanqingchen.wish.model.models.Notice;

import java.util.List;

/**
 * @author light_dust_generator
 * @since 2021-08-23 14:21:49
 */
@SqlKey("org_notice.list")
public class NoticeService extends ModelService<Notice> {
    @Override
    public Notice newModel(Record record, IUser _usr) throws Exception {
        Notice notice = super.newModel(record, _usr);
        notice.setStatus(true);
        notice.setType("NOTICE");
        return notice;
    }

    /**
     * 获取最新的通知
     */
    public Notice getNewsNotice() {
        List<Notice> noticeList = where("status =? order by createTime", 1);
        return BlankKit.isBlankList(noticeList) ? null : noticeList.get(0);
    }

}