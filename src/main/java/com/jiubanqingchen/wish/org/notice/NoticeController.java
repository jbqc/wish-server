package com.jiubanqingchen.wish.org.notice;

import com.jfinal.core.Path;
import com.jiubanqingchen.wish.framework.annotation.AuthorityKey;
import com.jiubanqingchen.wish.framework.base.controller.ModelController;
import com.jiubanqingchen.wish.model.models.Notice;

/**
 * @author light_dust_generator
 * @since 2021-08-23 14:21:49
 */
@Path("/org/notice")
@AuthorityKey("org:notice")
public class NoticeController extends ModelController<NoticeService, Notice> {
    @AuthorityKey("view")
    public void getNewsNotice() {
        renderJson(service.getNewsNotice());
    }
}