package com.jiubanqingchen.wish.org.cronTask;

import com.jfinal.core.Path;
import com.jiubanqingchen.wish.framework.annotation.AuthorityKey;
import com.jiubanqingchen.wish.framework.base.controller.ModelController;
import com.jiubanqingchen.wish.model.models.CronTask;

/**
 * @author light_dust
 * @since 2021/5/7 10:57
 */
@Path("/org/cronTask")
@AuthorityKey("org:cronTask")
public class CronTaskController extends ModelController<CronTaskService, CronTask> {
    @AuthorityKey("edit")
    public void changeActive() throws Exception {
        renderJson(service.changeActive(get("cronTaskId"), getBoolean("active")));
    }

    @AuthorityKey("edit")
    public void runOnceJob() throws Exception {
        renderJson(service.runOnceJob(get("cronTaskId")));
    }
}
