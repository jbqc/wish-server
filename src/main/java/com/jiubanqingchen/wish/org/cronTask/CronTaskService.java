package com.jiubanqingchen.wish.org.cronTask;

import com.jfinal.plugin.activerecord.Record;
import com.jiubanqingchen.wish.framework.annotation.SqlKey;
import com.jiubanqingchen.wish.framework.base.service.ModelService;
import com.jiubanqingchen.wish.framework.base.user.IUser;
import com.jiubanqingchen.wish.framework.exception.WishException;
import com.jiubanqingchen.wish.framework.exception.WishExceptionKey;
import com.jiubanqingchen.wish.model.models.CronTask;
import com.jiubanqingchen.wish.plugin.quartz.QuartzManager;

/**
 * @author light_dust
 * @since 2021/5/7 10:57
 */
@SqlKey("org_cronTask.list")
public class CronTaskService extends ModelService<CronTask> {

    /**
     * 修改任务状态执行
     *
     * @param cronTaskId 定时任务id
     * @param active     是否激活
     */
    public boolean changeActive(String cronTaskId, boolean active) throws WishException {
        CronTask cronTask = CronTask.dao.findById(cronTaskId);
        if (cronTask == null) throw new WishException(WishExceptionKey.JOB_NOT_FOUND, cronTaskId);
        if (cronTask.getActive() != active) {
            updateJobState(cronTask, active);
        }
        //开始执行状态更新
        cronTask.setActive(active).update();
        return true;
    }

    /**
     * 执行一次定时任务
     *
     * @param cronTaskId 任务id
     */
    public boolean runOnceJob(String cronTaskId) throws Exception {
        CronTask cronTask = CronTask.dao.findById(cronTaskId);
        if (cronTask == null) throw new WishException(WishExceptionKey.JOB_NOT_FOUND, cronTaskId);
        QuartzManager.runOnceJob(cronTask);
        return true;
    }

    /**
     * 更新任务状态
     *
     * @param cronTask 任务
     * @param flag     是否更新状态
     */
    private void updateJobState(CronTask cronTask, boolean flag) throws WishException {
        if ("NONE".equals(QuartzManager.getTriggerState(cronTask))) {//当前任务不在任务队列
            if (flag) QuartzManager.addJob(cronTask);//新增新任务
        } else { //当前任务在任务队列
            if (flag) QuartzManager.resumeJob(cronTask);
            else QuartzManager.pauseJob(cronTask);
        }
    }

    @Override
    public void preUpdate(CronTask cronTask, Record record, IUser _usr) throws WishException {
        String cronTaskId = cronTask.getCronTaskId();//得到任务id
        //得到老的cronTask
        CronTask oldCronTask = CronTask.dao.findById(cronTaskId);
        if (oldCronTask == null) throw new WishException(WishExceptionKey.JOB_NOT_FOUND, cronTaskId);
        //比对新老数据 修改任务时间均修改任务
        if (!oldCronTask.getCronExpression().equals(cronTask.getCronExpression()))
            QuartzManager.updateCronExpression(cronTask);
        //修改任务状态
        boolean active = cronTask.getActive();
        if (oldCronTask.getActive() != active) updateJobState(cronTask, active);
        super.preUpdate(cronTask, record, _usr);
    }

    @Override
    public void afterInsert(CronTask cronTask, Record record, IUser _usr) throws WishException {
        if (cronTask.getActive()) {
            QuartzManager.addJob(cronTask);//添加定时任务
        }
        super.afterInsert(cronTask, record, _usr);
    }

    @Override
    public void preDelete(CronTask cronTask, Record record, IUser _usr) throws WishException {
        QuartzManager.removeJob(cronTask);
        super.preDelete(cronTask, record, _usr);
    }

    @Override
    public CronTask newModel(Record record, IUser _usr) throws Exception {
        CronTask cronTask = super.newModel(record, _usr);
        cronTask.setActive(false);
        return cronTask;
    }
}
