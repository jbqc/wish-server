package com.jiubanqingchen.wish.org.dictionary;

import com.jiubanqingchen.wish.framework.annotation.SqlKey;
import com.jiubanqingchen.wish.framework.base.service.ModelService;
import com.jiubanqingchen.wish.model.models.Dictionary;

/**
 * @author light_dust
 * @since 2021/2/1 15:11
 */
@SqlKey("org_dictionary.list")
public class DictionaryService extends ModelService<Dictionary> {
}
