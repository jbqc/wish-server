package com.jiubanqingchen.wish.org.dictionary;

import com.jfinal.core.Path;
import com.jiubanqingchen.wish.framework.annotation.AuthorityKey;
import com.jiubanqingchen.wish.framework.base.controller.ModelController;
import com.jiubanqingchen.wish.model.models.Dictionary;

/**
 * @author light_dust
 * @since 2021/2/1 15:11
 */
@Path("/org/dictionary")
@AuthorityKey("org:dictionary")
public class DictionaryController extends ModelController<DictionaryService, Dictionary> {

}
