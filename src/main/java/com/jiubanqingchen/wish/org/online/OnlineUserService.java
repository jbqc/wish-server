package com.jiubanqingchen.wish.org.online;

import com.jfinal.plugin.redis.Cache;
import com.jiubanqingchen.wish.kit.ApplicationKit;
import com.jiubanqingchen.wish.plugin.cache.redis.kit.CacheKit;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * 在线用户服务
 *
 * @author light_dust
 * @since 2021/1/2 11:35
 */
public class OnlineUserService {
    private final String cacheKey = "wish:online_user:";

    /**
     * 获取在线用户集合
     */
    public List<OnlineUser> getOnLineUserList() {
        Set<String> keys = CacheKit.me().getCache().keys(cacheKey + "*");
        List<OnlineUser> onLineUserList;
        if (keys != null) {
            onLineUserList = new ArrayList<>();
            keys.forEach(key -> onLineUserList.add(CacheKit.me().getCache().get(key)));
        } else {
            onLineUserList = null;
        }
        return onLineUserList;
    }

    /**
     * 通过用户id获取在线用户
     *
     * @param userId 用户id
     */
    public OnlineUser getOnLineUser(String userId) {
        return CacheKit.me().getCache().get(getOnlineUserCacheKey(userId));
    }

    /**
     * 添加在线用户到缓存
     *
     * @param userId      用户id
     * @param userAccount 用户账号
     * @param userName    用户名称
     */
    public void addOnlineUserToCache(String userId, String userAccount, String userName, String token) {
        Cache cache = CacheKit.me().getCache();
        String cacheKey = getOnlineUserCacheKey(userId);
        OnlineUser onlineUser = cache.get(cacheKey);
        if (onlineUser == null) {
            onlineUser = new OnlineUser(userId, userAccount, userName, token);
        } else {
            onlineUser.setToken(token);
        }
        cache.setex(cacheKey, ApplicationKit.me().getTokenEffective(), onlineUser);
    }

    /**
     * 根据用户id删除在线用户信息
     *
     * @param userId 用户id
     */
    public boolean deleteOnlineUserForCache(String userId) {
        CacheKit.me().getCache().del(getOnlineUserCacheKey(userId));
        return true;
    }

    /**
     * 通过用户id获取用户缓存key
     *
     * @param userId 用户id
     */
    public String getOnlineUserCacheKey(String userId) {
        return cacheKey + userId;
    }
}
