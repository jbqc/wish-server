package com.jiubanqingchen.wish.org.online;

import com.jfinal.aop.Inject;
import com.jfinal.core.Path;
import com.jiubanqingchen.wish.framework.annotation.AuthorityKey;
import com.jiubanqingchen.wish.framework.base.controller.BaseController;

/**
 * @author light_dust
 * @since 2021/1/2 13:19
 */
@Path("/online")
@AuthorityKey("org:online")
public class OnlineController extends BaseController {
    @Inject
    OnlineUserService onlineUserService;

    @AuthorityKey("view")
    public void getOnlineUserList() {
        renderJson(onlineUserService.getOnLineUserList());
    }

    @AuthorityKey("view")
    public void getOnlineUser() {
        renderJson(onlineUserService.getOnLineUser(getPara()));
    }

    @AuthorityKey("delete")
    public void deleteOnlineUser() {
        renderJson(onlineUserService.deleteOnlineUserForCache(getPara()));
    }
}
