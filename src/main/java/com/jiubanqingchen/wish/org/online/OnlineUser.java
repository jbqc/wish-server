package com.jiubanqingchen.wish.org.online;

import com.jiubanqingchen.wish.kit.ApplicationKit;
import com.jiubanqingchen.wish.kit.BlankKit;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 在线用户
 *
 * @author light_dust
 * @since 2020/12/31 17:14
 */
public class OnlineUser implements Serializable {
    private String userId;
    private String userAccount;
    private String userName;
    private long loginTime;
    private List<String> tokenList;//储存 token 配置文件配置允许同时最大在线数

    public OnlineUser() {

    }

    public OnlineUser(String userId, String userAccount, String userName, String token) {
        this.userId = userId;
        this.userAccount = userAccount;
        this.userName = userName;
        this.loginTime = System.currentTimeMillis();
        setToken(token);
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(String userAccount) {
        this.userAccount = userAccount;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(long loginTime) {
        this.loginTime = loginTime;
    }

    public List<String> getTokenList() {
        return tokenList;
    }

    public void setToken(String token) {
        if (BlankKit.isBlankList(this.tokenList)) {
            tokenList = new ArrayList<>();
            tokenList.add(token);
        } else {
            int length = ApplicationKit.me().getOnlineUserMax();
            if (tokenList.size() > length) {
                tokenList =new ArrayList<>(tokenList.subList(0, tokenList.size() - 1)) ;
                tokenList.add(token);
            } else if (tokenList.size() == length) {
                for (int i = 0; i < length - 1; i++) {
                    tokenList.set(i, tokenList.get(i + 1));
                }
                tokenList.set(length - 1, token);
            } else {
                tokenList.add(token);
            }
        }
    }
}
