package com.jiubanqingchen.wish.org.menu;

import com.jiubanqingchen.wish.framework.annotation.SqlKey;
import com.jiubanqingchen.wish.framework.base.service.ModelService;
import com.jiubanqingchen.wish.model.models.Menu;

/**
 * @author light_dust
 * @since 2021/4/25 17:16
 */
@SqlKey("org_menu.list")
public class MenuService extends ModelService<Menu> {
}
