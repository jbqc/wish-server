package com.jiubanqingchen.wish.org.menu;

import com.jfinal.core.Path;
import com.jiubanqingchen.wish.framework.annotation.AuthorityKey;
import com.jiubanqingchen.wish.framework.base.controller.ModelController;
import com.jiubanqingchen.wish.model.models.Menu;

/**
 * @author light_dust
 * @since 2021/4/25 17:16
 */
@Path("/org/menu")
@AuthorityKey("org:menu")
public class MenuController extends ModelController<MenuService, Menu> {

}
