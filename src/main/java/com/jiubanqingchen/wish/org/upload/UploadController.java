package com.jiubanqingchen.wish.org.upload;

import com.jfinal.aop.Clear;
import com.jfinal.aop.Inject;
import com.jfinal.core.Path;
import com.jfinal.upload.UploadFile;
import com.jiubanqingchen.wish.framework.annotation.AuthorityKey;
import com.jiubanqingchen.wish.framework.base.controller.BaseController;
import com.jiubanqingchen.wish.framework.enums.SystemStatus;
import com.jiubanqingchen.wish.framework.exception.WishException;
import com.jiubanqingchen.wish.framework.exception.WishExceptionKey;
import com.jiubanqingchen.wish.interceptors.AuthorityInterceptor;
import com.jiubanqingchen.wish.kit.ApplicationKit;
import com.jiubanqingchen.wish.model.models.Attachment;

import java.io.File;

/**
 * @author light_dust
 * @since 2021/3/19 15:06
 */
@Path("/org/fileService")
@AuthorityKey("/org/fileService")
public class UploadController extends BaseController {
    @Inject
    UploadService uploadService;

    /*
        public void uploadImg() {
            UploadFile uploadFile = getFile();
            if (!FileTool.isImage(uploadFile.getFile())) {
                renderJson(SystemStatus.ERROR, "只能上传图片类型");
                return;
            }
            Attachment attachment = uploadService.uploadFile(uploadFile, getPara("busiId"), getPara("itemType"),
                    getCurrentUser().getUserId());
            renderJson(SystemStatus.SUCCESS, attachment);
        }
    */
    @Clear
    public void downloadFile() throws WishException {
        String fileId = getPara("fileId");
        Attachment attachment = Attachment.dao.findById(fileId);
        if (attachment == null) throw new WishException(WishExceptionKey.FILE_NOT_FOUND);
        renderFile(new File(ApplicationKit.me().getUploadPath() + attachment.getFilePath()));
    }

    @Clear(AuthorityInterceptor.class)
    public void upload() {
        UploadFile uploadFile = getFile();
        Attachment attachment = uploadService.uploadFile(uploadFile, getPara("busiId"), getPara("itemType"),
                getCurrentUser().getUserId());
        renderJson(SystemStatus.SUCCESS, attachment);
    }
}
