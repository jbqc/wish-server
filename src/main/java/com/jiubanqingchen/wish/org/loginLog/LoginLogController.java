package com.jiubanqingchen.wish.org.loginLog;

import com.jfinal.core.Path;
import com.jiubanqingchen.wish.framework.annotation.AuthorityKey;
import com.jiubanqingchen.wish.framework.base.controller.ModelController;
import com.jiubanqingchen.wish.model.models.LoginLog;

/**
 * @author light_dust_generator
 * @since 2021-06-23 19:12:53
 */
@Path("/org/loginLog")
@AuthorityKey("org:loginLog")
public class LoginLogController extends ModelController<LoginLogService, LoginLog> {

}