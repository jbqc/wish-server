package com.jiubanqingchen.wish.org.loginLog;

import com.jiubanqingchen.wish.framework.annotation.SqlKey;
import com.jiubanqingchen.wish.framework.base.service.ModelService;
import com.jiubanqingchen.wish.model.models.LoginLog;

/**
 * @author light_dust_generator
 * @since 2021-06-23 19:12:53
 */
@SqlKey("org_loginLog.list")
public class LoginLogService extends ModelService<LoginLog> {

}