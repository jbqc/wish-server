package com.jiubanqingchen.wish.org.user;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author light_dust
 * @since 2021/5/17 10:41
 */
public class UserListener extends AnalysisEventListener<UserBean> {
    private static final Logger logger = LoggerFactory.getLogger(UserListener.class);
    @Override
    public void invoke(UserBean userBean, AnalysisContext analysisContext) {
        int i=analysisContext.readSheetHolder().getApproximateTotalRowNumber();
        logger.info("总行数：{}",i);
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {

    }
}
