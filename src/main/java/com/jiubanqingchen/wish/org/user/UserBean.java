package com.jiubanqingchen.wish.org.user;

import com.alibaba.excel.annotation.ExcelProperty;

import java.io.Serializable;

/**
 * @author light_dust
 * @since 2021/5/17 10:43
 */
public class UserBean implements Serializable {
    @ExcelProperty(index = 0)
    private String userAccount;
    @ExcelProperty(index = 1)
    private String userName;
    @ExcelProperty(index = 2)
    private String phone;
    @ExcelProperty(index = 3)
    private String email;
    @ExcelProperty(index = 4)
    private String userBirthday;

    public String getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(String userAccount) {
        this.userAccount = userAccount;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserBirthday() {
        return userBirthday;
    }

    public void setUserBirthday(String userBirthday) {
        this.userBirthday = userBirthday;
    }
}
