package com.jiubanqingchen.wish.org.user;

import com.jfinal.aop.Aop;
import com.jfinal.kit.Kv;
import com.jfinal.kit.PropKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.upload.UploadFile;
import com.jiubanqingchen.wish.framework.annotation.HideColumn;
import com.jiubanqingchen.wish.framework.annotation.SqlKey;
import com.jiubanqingchen.wish.framework.annotation.UniqueColumn;
import com.jiubanqingchen.wish.framework.base.service.ModelService;
import com.jiubanqingchen.wish.framework.base.user.IUser;
import com.jiubanqingchen.wish.framework.exception.WishException;
import com.jiubanqingchen.wish.framework.exception.WishExceptionKey;
import com.jiubanqingchen.wish.framework.security.AuthService;
import com.jiubanqingchen.wish.framework.support.EncryptionSupport;
import com.jiubanqingchen.wish.kit.ApplicationKit;
import com.jiubanqingchen.wish.model.models.User;
import com.jiubanqingchen.wish.org.resourceAuthority.ResourceAuthorityService;
import com.jiubanqingchen.wish.org.userDept.UserDeptService;
import com.jiubanqingchen.wish.org.userGroupRole.UserGroupRoleService;
import com.jiubanqingchen.wish.plugin.cos.qcloud.CosRegionName;
import com.jiubanqingchen.wish.plugin.cos.qcloud.CosUpload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author light_dust
 * @since 2020/12/27 20:19
 */
@SqlKey("org_user.list")
@HideColumn("userPassword")
@UniqueColumn(column = "userAccount", errorKeyArray = {WishExceptionKey.USER_ACCOUNT_ALREADY_EXISTS})
public class UserService extends ModelService<User> {
    private static final Logger logger = LoggerFactory.getLogger(UserService.class);

    public User info(IUser adminUser) {
        //获取用户的基本信息
        User user = findById(adminUser.getUserId(), new Kv().set("hideColumns", "userPassword,createTime,createPerson,modifyTime,modifyPerson"));
        //获取用户的菜单信息(存放的是角色的菜单Id)
        user.put("menus", Aop.get(ResourceAuthorityService.class).getRoleListMenuResource(adminUser.getRoleList()));
        return user;
    }

    /**
     * 根据账号获取用户信息
     *
     * @param userAccount 用户账号
     * @return 如有账号 存在 返回用户信息 反之 为null
     */
    public User getUserForAccount(String userAccount) throws WishException {
        return whereFirst("userAccount=?", userAccount);
    }

    @Override
    public void afterDelete(User user, Record record, IUser _usr) throws WishException {
        //删除用户的同时删除 用户角色 用户部门相关信息
        String userId = user.getUserId();
        String userName = user.getUserName();

        //删除用户角色关联信息
        UserGroupRoleService userGroupRoleService = Aop.get(UserGroupRoleService.class);
        userGroupRoleService.deleteWhere("userGroupId=? and userGroupType='USER'", userId);
        logger.info("正在删除用户 [{}] ({}) 角色关联信息...", userName, userId);
        //删除用户部门关联信息
        UserDeptService userDeptService = Aop.get(UserDeptService.class);
        userDeptService.deleteWhere("userId=?", userId);
        logger.info("正在删除用户 [{}] ({}) 部门关联信息...", userName, userId);
        //删除用户缓存
        AuthService authService = Aop.get(AuthService.class);
        authService.deleteUserCache(userId);
        super.afterDelete(user, record, _usr);
    }

    public void uploadExcel(UploadFile uploadFile, IUser _usr) {

    }

    @Override
    public void preInsert(User user, Record record, IUser _usr) throws WishException {
        if (StrKit.isBlank(user.getUserPassword())) {
            user.setUserPassword(EncryptionSupport.me().encrypPassword(ApplicationKit.me().getUserInitPassword()));
        }
        super.preInsert(user, record, _usr);
    }

    /**
     * 上传用户头像
     * @param uploadFile 上传文件
     * @param userId 用户id
     */
    public void uploadAvatar(UploadFile uploadFile, String userId) throws WishException{
        User user=User.dao.findById(userId);
        if(user==null) throw new WishException(WishExceptionKey.MODEL_ID_QUERY_FAIL_CAUSE_NULL,userId);
        CosUpload cosUpload = new CosUpload(CosRegionName.GUANG_ZHOU, PropKit.get("tencent.cloud.cos.bucketName"), PropKit.get("wish.upload.user.avatar.path"));
        String avatarUrl = cosUpload.upload(uploadFile);
        user.setUserAvatar(avatarUrl);
        user.update();
    }
}
