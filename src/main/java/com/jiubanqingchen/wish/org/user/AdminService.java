package com.jiubanqingchen.wish.org.user;

import com.jiubanqingchen.wish.framework.base.controller.IAdminService;
import com.jiubanqingchen.wish.framework.base.user.IUser;
import com.jiubanqingchen.wish.framework.consts.BaseKey;
import com.jiubanqingchen.wish.framework.consts.CacheKey;
import com.jiubanqingchen.wish.plugin.cache.redis.kit.CacheKit;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @author light_dust
 * @since 2021/1/4 17:26
 */
public class AdminService implements IAdminService {

    @Override
    public IUser getAdminUser(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session == null) return null;
        Object userId = session.getAttribute(BaseKey.CURRENT_USER_ID_KEY);
        return userId != null ? CacheKit.me().getCache().get(CacheKey.ADMIN_USER.replace("#(userId)", userId.toString())) : null;
    }

    @Override
    public String getCurrentToken(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session == null) return null;
        Object currentToken = session.getAttribute(BaseKey.CURRENT_TOKEN);
        return currentToken == null ? null : currentToken.toString();
    }
}
