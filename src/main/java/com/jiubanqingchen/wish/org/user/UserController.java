package com.jiubanqingchen.wish.org.user;

import com.alibaba.excel.EasyExcel;
import com.jfinal.aop.Clear;
import com.jfinal.core.Path;
import com.jfinal.upload.UploadFile;
import com.jiubanqingchen.wish.framework.annotation.AuthorityKey;
import com.jiubanqingchen.wish.framework.base.controller.ModelController;
import com.jiubanqingchen.wish.interceptors.AuthorityInterceptor;
import com.jiubanqingchen.wish.model.models.User;
import com.jiubanqingchen.wish.plugin.easyexcel.listener.MapDataListener;

/**
 * @author light_dust
 * @since 2020/12/27 20:11
 */
@Path("/org/user")
@AuthorityKey("org:user")
public class UserController extends ModelController<UserService, User> {

    @Clear(AuthorityInterceptor.class)
    public void info() {
        renderJson(service.info(getCurrentUser()));
    }

    @AuthorityKey("upload")
    public void uploadExcel() {
        UploadFile uploadFile = getFile();
        String socketId = getPara("socketId");
        MapDataListener mapDataListener = new MapDataListener(socketId);
        EasyExcel.read(uploadFile.getFile(), mapDataListener).sheet().doRead();
        service.uploadExcel(uploadFile, getCurrentUser());
        renderJson(true);
    }

    @AuthorityKey("upload")
    public void uploadAvatar() {
        UploadFile uploadFile = getFile();
        service.uploadAvatar(uploadFile,getPara("userId"));
        renderJson(true);
    }
}
