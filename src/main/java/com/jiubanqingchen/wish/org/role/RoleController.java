package com.jiubanqingchen.wish.org.role;

import com.jfinal.core.Path;
import com.jiubanqingchen.wish.framework.annotation.AuthorityKey;
import com.jiubanqingchen.wish.framework.base.controller.ModelController;
import com.jiubanqingchen.wish.model.models.Role;

/**
 * @author light_dust
 * @since 2021/1/4 11:44
 */
@Path("/org/role")
@AuthorityKey("org:role")
public class RoleController extends ModelController<RoleService, Role> {

}
