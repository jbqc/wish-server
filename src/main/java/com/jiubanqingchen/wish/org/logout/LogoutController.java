package com.jiubanqingchen.wish.org.logout;

import com.jfinal.aop.Clear;
import com.jfinal.core.Path;
import com.jiubanqingchen.wish.framework.base.controller.BaseController;
import com.jiubanqingchen.wish.framework.security.admin.AdminUser;
import com.jiubanqingchen.wish.interceptors.AuthorityInterceptor;

/**
 * @author light_dust
 * @since 2021/1/14 17:26
 */
@Path("/logout")
public class LogoutController extends BaseController {

    @Clear({AuthorityInterceptor.class})
    public void index() {
        LogoutService logoutService = new LogoutService();
        renderJson(logoutService.clear((AdminUser) getCurrentUser(),getCurrentToken()));
    }
}
