package com.jiubanqingchen.wish.org.logout;

import com.jfinal.aop.Aop;
import com.jiubanqingchen.wish.framework.security.admin.AdminUser;
import com.jiubanqingchen.wish.framework.security.admin.AdminUserService;
import com.jiubanqingchen.wish.org.online.OnlineUser;
import com.jiubanqingchen.wish.org.online.OnlineUserService;

import java.util.List;

/**
 * @author light_dust
 * @since 2021/1/14 17:26
 */
public class LogoutService {

    /**
     * 清理相关信息
     *
     * @param adminUser 用户信息
     */
    public boolean clear(AdminUser adminUser, String token)  {
        String userId = adminUser.getUserId();
        OnlineUserService onlineUserService = Aop.get(OnlineUserService.class);
        OnlineUser onlineUser = onlineUserService.getOnLineUser(userId);
        List<String> list = onlineUser.getTokenList();
        list.removeIf(child -> child.equals(token));
        System.out.println(list.size());
        if (list.size() == 0) {//如果当前在线用户token被全部删除则删除当前在线用户和admin user缓存
            onlineUserService.deleteOnlineUserForCache(userId);
            Aop.get(AdminUserService.class).deleteAdminUser(userId);
        }
        return true;
    }
}
