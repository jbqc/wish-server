package com.jiubanqingchen.wish.org.appAction;

import com.jfinal.core.Path;
import com.jiubanqingchen.wish.framework.annotation.AuthorityKey;
import com.jiubanqingchen.wish.framework.base.controller.ModelController;
import com.jiubanqingchen.wish.model.models.AppAction;

/**
 * @author light_dust
 * @since 2021/4/25 17:52
 */
@Path("/org/appAction")
@AuthorityKey("org:appAction")
public class AppActionController extends ModelController<AppActionService, AppAction> {
    @AuthorityKey("add")
    public void initModelControllerAction() {
        service.initModelControllerAction(getPara("actionId"), getCurrentUser());
        renderJson(true);
    }

    @AuthorityKey("view")
    public void tree(){
        renderJson(service.tree(getQueryParams()));
    }
}
