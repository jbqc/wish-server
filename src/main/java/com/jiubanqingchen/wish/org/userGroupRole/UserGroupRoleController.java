package com.jiubanqingchen.wish.org.userGroupRole;

import com.jfinal.core.Path;
import com.jiubanqingchen.wish.framework.annotation.AuthorityKey;
import com.jiubanqingchen.wish.framework.base.controller.ModelController;
import com.jiubanqingchen.wish.model.models.UserGroupRole;

/**
 * @author light_dust
 * @since 2021/4/7 10:48
 */
@Path("/org/userGroupRole")
@AuthorityKey("org:userGroupRole")
public class UserGroupRoleController extends ModelController<UserGroupRoleService, UserGroupRole> {
    @AuthorityKey("view")
    public void userRoleList() {
        renderJson(service.userRoleList(getQueryParams()));
    }
}
