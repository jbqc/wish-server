package com.jiubanqingchen.wish.org.userGroupRole;

import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Page;
import com.jiubanqingchen.wish.framework.annotation.SqlKey;
import com.jiubanqingchen.wish.framework.base.service.ModelService;
import com.jiubanqingchen.wish.model.models.UserGroupRole;

/**
 * @author light_dust
 * @since 2021/4/7 10:48
 */
@SqlKey("org_userGroupRole.list")
public class UserGroupRoleService extends ModelService<UserGroupRole> {

    /**
     * 获取用户的角色列表信息
     *
     * @param kv 参数
     */
    public Page<UserGroupRole> userRoleList(Kv kv) {
       return  findPage("org_userGroupRole.userRoleList",kv);
    }
}
