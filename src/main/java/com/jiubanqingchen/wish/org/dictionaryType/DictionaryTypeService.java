package com.jiubanqingchen.wish.org.dictionaryType;


import com.jfinal.aop.Aop;
import com.jfinal.plugin.activerecord.Record;
import com.jiubanqingchen.wish.framework.annotation.SqlKey;
import com.jiubanqingchen.wish.framework.base.service.ModelService;
import com.jiubanqingchen.wish.framework.base.user.IUser;
import com.jiubanqingchen.wish.framework.exception.WishException;
import com.jiubanqingchen.wish.model.models.DictionaryType;
import com.jiubanqingchen.wish.org.dictionary.DictionaryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author light_dust
 * @since 2021/2/1 15:11
 */
@SqlKey("org_dictionaryType.list")
public class DictionaryTypeService extends ModelService<DictionaryType> {
    private static final Logger logger = LoggerFactory.getLogger(DictionaryTypeService.class);

    @Override
    public void afterDelete(DictionaryType dictionaryType, Record record, IUser _usr) throws WishException {
        //数据字典类型删除后 删除类型下面的数据字典项
        String codeItemId = dictionaryType.getCodeItemId();

        //删除数据字典项
        DictionaryService dictionaryService = Aop.get(DictionaryService.class);
        dictionaryService.deleteWhere("codeItemId=?", codeItemId);
        logger.info("正在删除字典类型 [{}] 的字典项信息...", codeItemId);
        super.afterDelete(dictionaryType, record, _usr);
    }
}
