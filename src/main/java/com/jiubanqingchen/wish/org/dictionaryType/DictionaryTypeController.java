package com.jiubanqingchen.wish.org.dictionaryType;

import com.jfinal.core.Path;
import com.jiubanqingchen.wish.framework.annotation.AuthorityKey;
import com.jiubanqingchen.wish.framework.base.controller.ModelController;
import com.jiubanqingchen.wish.model.models.DictionaryType;

/**
 * @author light_dust
 * @since 2021/2/1 15:11
 */
@Path("/org/dictionaryType")
@AuthorityKey("org:dictionaryType")
public class DictionaryTypeController extends ModelController<DictionaryTypeService, DictionaryType> {
}
