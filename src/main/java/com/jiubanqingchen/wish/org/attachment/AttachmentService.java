package com.jiubanqingchen.wish.org.attachment;

import cn.hutool.core.io.FileUtil;
import com.jfinal.plugin.activerecord.Record;
import com.jiubanqingchen.wish.framework.base.service.ModelService;
import com.jiubanqingchen.wish.framework.base.user.IUser;
import com.jiubanqingchen.wish.framework.exception.WishException;
import com.jiubanqingchen.wish.kit.ApplicationKit;
import com.jiubanqingchen.wish.model.models.Attachment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author light_dust
 * @since 2021/3/19 14:50
 */
public class AttachmentService extends ModelService<Attachment> {
    private static final Logger logger = LoggerFactory.getLogger(AttachmentService.class);
    @Override
    public void afterDelete(Attachment attachment, Record record, IUser _usr) throws WishException {
        String baseDir = ApplicationKit.me().getUploadPath();// 获取上传文件路径
        String dir = baseDir + attachment.getFilePath();
        logger.info("正在进行文件删除......文件路径:" + dir);
        // 删除硬盘文件
        FileUtil.del(dir);
        super.afterDelete(attachment, record, _usr);
    }
}
