package com.jiubanqingchen.wish.org.userDept;

import com.jfinal.core.Path;
import com.jiubanqingchen.wish.framework.annotation.AuthorityKey;
import com.jiubanqingchen.wish.framework.base.controller.ModelController;
import com.jiubanqingchen.wish.model.models.UserDept;

/**
 * @author light_dust
 * @since 2021/1/4 11:51
 */
@Path("/org/userDept")
@AuthorityKey("org:userDept")
public class UserDeptController extends ModelController<UserDeptService, UserDept> {
}
