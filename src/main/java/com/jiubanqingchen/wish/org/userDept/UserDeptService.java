package com.jiubanqingchen.wish.org.userDept;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.jiubanqingchen.wish.framework.annotation.SqlKey;
import com.jiubanqingchen.wish.framework.base.service.ModelService;
import com.jiubanqingchen.wish.framework.base.user.IUser;
import com.jiubanqingchen.wish.model.models.UserDept;

/**
 * @author light_dust
 * @since 2021/1/4 11:52
 */
@SqlKey("org_userDept.list")
public class UserDeptService extends ModelService<UserDept> {
    @Override
    public void preUpdate(UserDept userDept, Record record, IUser _usr) {
        String updateKey = record.getStr("updateKey");
        if ("main".equals(updateKey))
            Db.update("update wish_user_dept set main=0 where userId=?", userDept.getUserId());
        super.preUpdate(userDept, record, _usr);
    }
}
