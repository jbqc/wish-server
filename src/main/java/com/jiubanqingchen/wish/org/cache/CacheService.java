package com.jiubanqingchen.wish.org.cache;

import com.jiubanqingchen.wish.framework.annotation.SqlKey;
import com.jiubanqingchen.wish.framework.base.service.ModelService;
import com.jiubanqingchen.wish.model.models.Cache;
import com.jiubanqingchen.wish.plugin.cache.redis.kit.CacheKit;

/**
 * @author light_dust_generator
 * @since 2021-07-10 19:39:32
 */
@SqlKey("org_cache.list")
public class CacheService extends ModelService<Cache> {

    /**
     * 获取当前前缀的所有缓存key
     *
     * @param cacheName 缓存名称
     * @param prefixKey 缓存key前缀
     */
    public String[] getKeysByPrefixKey(String cacheName, String prefixKey) {
        return CacheKit.me().getKeysByPrefixKey(cacheName, prefixKey);
    }

    /**
     * 删除当前前缀的所有缓存key
     *
     * @param cacheName 缓存名称
     * @param prefixKey 缓存key前缀
     */
    public void deleteKeysByPrefixKey(String cacheName, String prefixKey) {
        CacheKit.me().deleteKeysByPrefixKey(cacheName, prefixKey);
    }

    /**
     * 删除所有缓存
     * @param cacheName 缓存名称
     */
    public void deleteAllCache(String cacheName) {
        CacheKit.me().deleteAllCache(cacheName);
    }
}