package com.jiubanqingchen.wish.org.cache;

import com.jfinal.core.Path;
import com.jiubanqingchen.wish.framework.annotation.AuthorityKey;
import com.jiubanqingchen.wish.framework.base.controller.ModelController;
import com.jiubanqingchen.wish.model.models.Cache;

/**
 * @author light_dust_generator
 * @since 2021-07-10 19:39:32
 */
@Path("/org/cache")
@AuthorityKey("org:cache")
public class CacheController extends ModelController<CacheService, Cache> {

    @AuthorityKey("view")
    public void getKeysByPrefixKey() {
        renderJson(service.getKeysByPrefixKey(getPara("cacheName"), getPara("prefix")));
    }

    @AuthorityKey("delete")
    public void deleteKeysByPrefixKey() {
        service.deleteKeysByPrefixKey(getPara("cacheName"), getPara("prefix"));
        renderJson(true);
    }

    @AuthorityKey("delete")
    public void deleteAllCache() {
        service.deleteAllCache(getPara("cacheName"));
        renderJson(true);
    }
}