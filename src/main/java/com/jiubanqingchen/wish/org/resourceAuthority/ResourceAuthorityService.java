package com.jiubanqingchen.wish.org.resourceAuthority;

import com.jfinal.aop.Aop;
import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.jiubanqingchen.wish.framework.annotation.SqlKey;
import com.jiubanqingchen.wish.framework.base.service.ModelService;
import com.jiubanqingchen.wish.framework.base.user.IUser;
import com.jiubanqingchen.wish.framework.consts.BaseKey;
import com.jiubanqingchen.wish.framework.exception.WishException;
import com.jiubanqingchen.wish.framework.security.AuthService;
import com.jiubanqingchen.wish.kit.BlankKit;
import com.jiubanqingchen.wish.model.models.ResourceAuthority;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author light_dust
 * @since 2021/4/16 16:40
 */
@SqlKey("org_resourceAuthority.list")
public class ResourceAuthorityService extends ModelService<ResourceAuthority> {
    private static final Logger logger = LoggerFactory.getLogger(ResourceAuthorityService.class);

    /**
     * 得到角色可使用菜单权限
     *
     * @param roleId 角色id
     */
    public List<String> getRoleMenuResource(String roleId) {
        //得到可使用权限
        return Aop.get(AuthService.class).getRoleUseMenuResource(roleId);
    }

    /**
     * 得到一组角色可使用菜单权限
     *
     * @param roleIdList 角色id集合
     */
    public List<String> getRoleListMenuResource(List<String> roleIdList) {
        if (BlankKit.isBlankList(roleIdList)) return null;
        List<String> menuIds = new ArrayList<>();
        roleIdList.forEach(roleId -> menuIds.addAll(getRoleMenuResource(roleId)));
        return menuIds.stream().distinct().collect(Collectors.toList());
    }

    /**
     * 得到角色可以使用的接口
     *
     * @param roleId 角色id
     */
    public List<String> getRoleInterfaceResource(String roleId) {
        return Aop.get(AuthService.class).getRoleUseInterfaceResource(roleId);
    }

    @Override
    public boolean insertBatch(List<ResourceAuthority> modelList, Record record, IUser _usr) throws WishException {
        String roleId = record.get("roleId");
        ResourceAuthority resourceAuthority = modelList.get(0);
        String resourceType = resourceAuthority.getResourceType();
        //在插入之前先删除当前角色的权限
        deleteWhere("roleId=? and resourceType=? and resourceCategory=?", roleId, resourceType, resourceAuthority.getResourceCategory());
        //执行插入语句
        boolean flag = super.insertBatch(modelList, record, _usr);
        //清理角色的缓存
        AuthService authService = Aop.get(AuthService.class);
        if (BaseKey.RESOURCE_TYPE_MENU.equals(resourceType)) {
            authService.deleteRoleMenuAuthorityCache(roleId);
        } else {
            authService.deleteRoleInterfaceAuthorityCache(roleId);
        }
        return flag;
    }

    /**
     * 根据资源id数组和类型得到与角色的关联信息
     *
     * @param resourceId 资源id数组
     */
    public List<String> findRoleIdByResourceId(String resourceId) {
        List<String> resourceIds = new ArrayList<>();
        resourceIds.add(resourceId);
        return findRoleIdByResourceIds(resourceIds);
    }

    /**
     * 根据资源id数组和类型得到与角色的关联信息
     *
     * @param resourceIds 资源id数组
     */
    public List<String> findRoleIdByResourceIds(List<String> resourceIds) {
        return Db.template("org_resourceAuthority.findRoleIdByResourceIds", new Kv().set("resourceIds", resourceIds)).query();
    }

}
