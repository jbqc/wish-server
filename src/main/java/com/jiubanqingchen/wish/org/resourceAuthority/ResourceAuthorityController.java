package com.jiubanqingchen.wish.org.resourceAuthority;

import com.jfinal.core.Path;
import com.jiubanqingchen.wish.framework.annotation.AuthorityKey;
import com.jiubanqingchen.wish.framework.base.controller.ModelController;
import com.jiubanqingchen.wish.model.models.ResourceAuthority;

/**
 * @author light_dust
 * @since 2021/4/16 16:39
 */
@Path("/org/resourceAuthority")
@AuthorityKey("org:resourceAuthority")
public class ResourceAuthorityController extends ModelController<ResourceAuthorityService, ResourceAuthority> {
    @AuthorityKey("view")
    public void getRoleMenuResource() {
        renderJson(service.getRoleMenuResource(getPara("roleId")));
    }

    @AuthorityKey("view")
    public void getRoleInterfaceResource(String roleId){
        renderJson(service.getRoleInterfaceResource(getPara("roleId")));
    }
}
