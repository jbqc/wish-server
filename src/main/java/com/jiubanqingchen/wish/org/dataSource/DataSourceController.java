package com.jiubanqingchen.wish.org.dataSource;

import com.jfinal.aop.Clear;
import com.jfinal.core.Path;
import com.jiubanqingchen.wish.framework.annotation.AuthorityKey;
import com.jiubanqingchen.wish.framework.base.controller.ModelController;
import com.jiubanqingchen.wish.model.models.DataSource;

/**
 * @author light_dust_generator
 * @since 2021-06-30 11:44:50
 */
@Path("/org/dataSource")
@AuthorityKey("org:dataSource")
public class DataSourceController extends ModelController<DataSourceService, DataSource> {
    @Clear
    public void getBlog() {
        renderJson(service.getBlog());
    }
}