package com.jiubanqingchen.wish.org.dataSource;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.jiubanqingchen.wish.framework.annotation.HideColumn;
import com.jiubanqingchen.wish.framework.annotation.SqlKey;
import com.jiubanqingchen.wish.framework.annotation.UniqueColumn;
import com.jiubanqingchen.wish.framework.base.service.ModelService;
import com.jiubanqingchen.wish.framework.exception.WishExceptionKey;
import com.jiubanqingchen.wish.model.models.DataSource;

import java.util.List;

/**
 * @author light_dust_generator
 * @since 2021-06-30 11:44:50
 */
@SqlKey("org_dataSource.list")
@HideColumn("userPassword,userName")
@UniqueColumn(column = "dataSourceCode", errorKeyArray = {WishExceptionKey.ORG_DATA_SOURCE_CODE_ALREADY_EXISTS})
public class DataSourceService extends ModelService<DataSource> {
    public List<Record> getBlog() {
        return Db.use("JXHX").find("select * from blog_news");
    }
}