package com.jiubanqingchen.wish.config;

import com.jfinal.aop.Aop;
import com.jfinal.aop.AopManager;
import com.jiubanqingchen.wish.framework.base.controller.IAdminService;
import com.jiubanqingchen.wish.framework.support.DataSourceSupport;
import com.jiubanqingchen.wish.org.user.AdminService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 实现一些在系统启动后的功能
 *
 * @author light_dust
 * @since 2021/1/4 17:59
 */
public class WishAdminStart {
    private static final Logger logger = LoggerFactory.getLogger(WishAdminStart.class);
    private static final WishAdminStart me = new WishAdminStart();

    public static WishAdminStart me() {
        return me;
    }

    public void init() {
        addAopMapping();
        Aop.get(DataSourceSupport.class).addDynamicDatasource();
    }

    /**
     * 添加aop 注入映射
     */
    public void addAopMapping() {
        AopManager.me().addMapping(IAdminService.class, AdminService.class);
    }


}
