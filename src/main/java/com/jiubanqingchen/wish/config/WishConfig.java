package com.jiubanqingchen.wish.config;

import com.alibaba.druid.filter.logging.Log4jFilter;
import com.jfinal.config.*;
import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.plugin.redis.RedisPlugin;
import com.jfinal.template.Engine;
import com.jiubanqingchen.wish.framework.directive.ParaLikeDirective;
import com.jiubanqingchen.wish.framework.render.renderFactory.WishRenderFactory;
import com.jiubanqingchen.wish.interceptors.AuthorityInterceptor;
import com.jiubanqingchen.wish.interceptors.GlobalInterceptor;
import com.jiubanqingchen.wish.interceptors.LoginInterceptor;
import com.jiubanqingchen.wish.kit.ApplicationKit;
import com.jiubanqingchen.wish.model.models.WishAdminMappingKit;
import com.jiubanqingchen.wish.plugin.cache.redis.interceptor.CacheRemoveInterceptor;
import com.jiubanqingchen.wish.plugin.cache.redis.interceptor.CacheableInterceptor;
import com.jiubanqingchen.wish.plugin.easyexcel.EasyExcelPlugin;
import com.jiubanqingchen.wish.plugin.quartz.QuartzPlugin;

public class WishConfig extends JFinalConfig {

    protected Prop serverProp;//数据源

    /**
     * PropKit.useFirstFound(...) 使用参数中从左到右最先被找到的配置文件
     * 从左到右依次去找配置，找到则立即加载并立即返回，后续配置将被忽略
     */
    protected void loadConfig() {
        prop = PropKit.useFirstFound("application.properties");
        serverProp = PropKit.useFirstFound("application.server.properties");
    }

    @Override
    public void configConstant(Constants constants) {
        //加载配置
        loadConfig();
        //设置开发模式
        constants.setDevMode(prop.getBoolean("devMode", true));
        // 开启对 jFinal web 项目组件 Controller、Interceptor、Validator 的注入
        constants.setInjectDependency(true);
        // 开启对超类的注入。不开启时可以在超类中通过 Aop.get(...) 进行注入
        constants.setInjectSuperClass(true);
        //设置自定义renderFactory
        constants.setRenderFactory(new WishRenderFactory());
        //设置工厂为slf4j
        constants.setToSlf4jLogFactory();
        //4.6 版本新增配置方式 设置代理方式为cglib
        constants.setToCglibProxyFactory();
    }

    @Override
    public void configRoute(Routes routes) {
        routes.setMappingSuperClass(true);
        routes.scan("com.jiubanqingchen");
    }

    @Override
    public void configEngine(Engine engine) {
        engine.setToClassPathSourceFactory();
        // 支持模板热加载，绝大多数生产环境下也建议配置成 true，除非是极端高性能的场景
        engine.setDevMode(prop.getBoolean("engineDevMode", true));
        // 配置极速模式，性能提升 13%
        Engine.setFastMode(prop.getBoolean("fastMode", true));
        // 开启 HTML 压缩功能，分隔字符参数可配置为：'\n' 与 ' '
        engine.setCompressorOn(' ');
    }

    @Override
    public void configPlugin(Plugins plugins) {
        //配置 wish druid 插件
        DruidPlugin wishDruidPlugin = createDruidPlugin("wish.datasource");
        //对druid做其他配置
        configDruidPlugin(wishDruidPlugin);
        plugins.add(wishDruidPlugin);
        // 配置 wish ActiveRecord插件
        ActiveRecordPlugin wishArp = new ActiveRecordPlugin("wish", wishDruidPlugin);
        configWishArp(wishArp);
        plugins.add(wishArp);
        // 用于缓存系统模块的redis服务 这个为默认redis
        RedisPlugin systemRedis = new RedisPlugin(ApplicationKit.me().getRedisCacheName(), prop.get("wish.cache.redis.host","localhost"));
        plugins.add(systemRedis);
        //添加easy excel plugin
        plugins.add(new EasyExcelPlugin());
        //添加定时任务插件
        if (ApplicationKit.me().getTaskEnable()) plugins.add(new QuartzPlugin());
    }

    @Override
    public void configInterceptor(Interceptors interceptors) {
        //全局controller拦截器
        interceptors.addGlobalActionInterceptor(new GlobalInterceptor());
        interceptors.addGlobalActionInterceptor(new LoginInterceptor());
        interceptors.addGlobalActionInterceptor(new AuthorityInterceptor());
        //cache 拦截器
        interceptors.addGlobalServiceInterceptor(new CacheableInterceptor());
        interceptors.addGlobalServiceInterceptor(new CacheRemoveInterceptor());
    }

    @Override
    public void configHandler(Handlers handlers) {

    }

    /**
     * 创建druid plugin
     *
     * @param prefixName 前缀
     * @since 2020/9/19 9:24
     **/
    protected DruidPlugin createDruidPlugin(String prefixName) {
        return new DruidPlugin(serverProp.get(prefixName + ".jdbcUrl"), serverProp.get(prefixName + ".user"), serverProp.get(prefixName + ".password"));
    }

    protected void configDruidPlugin(DruidPlugin dbPlugin) {
        Log4jFilter logFilter = new Log4jFilter();
        logFilter.setStatementLogEnabled(false);
        logFilter.setStatementLogErrorEnabled(true);
        logFilter.setStatementExecutableSqlLogEnable(true);
        dbPlugin.addFilter(logFilter);
    }

    /**
     * 配置WishArp
     *
     * @param wishArp 默认数据源
     *///
    protected void configWishArp(ActiveRecordPlugin wishArp) {
        WishAdminMappingKit.mapping(wishArp);
        wishArp.addSqlTemplate("org/sql/index.sql");

        wishArp.setShowSql(prop.getBoolean("showSql", true));
        Engine engine = wishArp.getEngine();
        engine.setCompressorOn(' ');//设置sql 空格优化
        engine.addDirective("paraLike", ParaLikeDirective.class);
    }

    @Override
    public void onStart() {
        super.onStart();
        WishAdminStart.me().init();
    }
}
