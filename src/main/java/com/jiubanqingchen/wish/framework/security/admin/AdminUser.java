package com.jiubanqingchen.wish.framework.security.admin;


import com.jiubanqingchen.wish.framework.base.user.IUser;

import java.util.List;

/**
 * user 包含用户id 账号 名称 角色 主要部门
 *
 * @author light_dust
 * @since 2021/1/4 16:20
 */
public class AdminUser implements IUser {

    private String userId;

    private String userAccount;

    private String userName;

    private List<String> roleList;

    private String mainDeptId;

    private String mainDeptName;

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setUserAccount(String userAccount) {
        this.userAccount = userAccount;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setRoleList(List<String> roleList) {
        this.roleList = roleList;
    }

    public void setMainDeptId(String mainDeptId) {
        this.mainDeptId = mainDeptId;
    }

    public void setMainDeptName(String mainDeptName) {
        this.mainDeptName = mainDeptName;
    }

    @Override
    public String getUserId() {
        return this.userId;
    }

    @Override
    public String getUserAccount() {
        return this.userAccount;
    }

    @Override
    public String getUserName() {
        return this.userName;
    }

    @Override
    public List<String> getRoleList() {
        return this.roleList;
    }

    @Override
    public String getMainDeptId() {
        return this.mainDeptId;
    }

    @Override
    public String getMainDeptName() {
        return this.mainDeptName;
    }
}
