package com.jiubanqingchen.wish.framework.consts;

/**
 * @author light_dust
 * @since 2021/1/4 15:25
 */
public final class CacheKey {
    /**
     * 用户部门信息缓存
     */
    public final static String USER_DEPT = "wish:user_dept:#(userId)";
    /**
     * 用户角色信息缓存
     */
    public final static String USER_ROLE = "wish:user_role:#(userId)";
    /**
     * adminUser信息缓存
     */
    public final static String ADMIN_USER = "wish:admin_user:#(userId)";
    /**
     * 角色可使用接口资源缓存
     */
    public final static String ROLE_USE_INTERFACE_RESOURCE = "wish:role_use_resource:interface:#(roleId)";
    /**
     * 角色可使用菜单资源缓存
     */
    public final static String ROLE_USE_MENU_RESOURCE = "wish:role_use_resource:menu:#(roleId)";
    /**
     * 角色可分配接口资源缓存
     */
    public final static String ROLE_ASSIGN_INTERFACE_RESOURCE = "wish:role_assign_resource:interface:#(roleId)";
    /**
     * 角色可分配菜单资源缓存
     */
    public final static String ROLE_ASSIGN_MENU_RESOURCE = "wish:role_assign_resource:menu:#(roleId)";
    /**
     * 角色使用的接口authorityKey
     */
    public final static String ROLE_USE_INTERFACE_AUTHORITY="wish:role_use_authority:interface:#(roleId)";
}
