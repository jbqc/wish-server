package com.jiubanqingchen.wish.framework.consts;

/**
 * @author light_dust
 * @since 2020/12/25 16:16
 */
public class FileContentType {

    public static final String EXCEL="application/vnd.ms-excel";
}
