package com.jiubanqingchen.wish.framework.consts;

/**
 * 定义一些Key值
 *
 * @author light_dust
 * @since 2021/1/16 11:10
 */
public final class BaseKey {
    /**
     * 当前用户id key
     */
    public static final String CURRENT_USER_ID_KEY = "currentUserId";
    /**
     * 当前token key
     */
    public static final String CURRENT_TOKEN = "currentToken";
    /**
     * token key
     */
    public static final String TOKEN_KEY = "Access-Token";
    /**
     * 权限资源分类可使用
     */
    public static final String RESOURCE_CATEGORY_USE="USE";
    /**
     * 权限资源分类 可分配
     */
    public static final String RESOURCE_CATEGORY_ASSIGN="ASSIGN";
    /**
     * 权限资源类型 菜单
     */
    public static final String RESOURCE_TYPE_MENU="MENU";
    /**
     * 权限资源类型 后台接口
     */
    public static final String RESOURCE_TYPE_INTERFACE="INTERFACE";
}
