package com.jiubanqingchen.wish.framework.render.renderFactory;

import com.jfinal.render.Render;
import com.jfinal.render.RenderFactory;
import com.jiubanqingchen.wish.framework.enums.SystemStatus;
import com.jiubanqingchen.wish.framework.render.RenderJson;

import java.util.HashMap;
import java.util.Map;

/**
 * @author light_dust
 * @since 2020/12/10 21:26
 */
public class WishRenderFactory extends RenderFactory {
    @Override
    public Render getJsonRender() {
        return super.getJsonRender();
    }

    @Override
    public Render getJsonRender(String key, Object value) {
        Map<String, Object> map = new HashMap<>();
        map.put(key, value);
        return super.getJsonRender(new RenderJson(map));
    }

    @Override
    public Render getJsonRender(String[] attrs) {
        return super.getJsonRender(new RenderJson(attrs));
    }

    @Override
    public Render getJsonRender(String jsonText) {
        return super.getJsonRender(new RenderJson(jsonText));
    }

    @Override
    public Render getJsonRender(Object object) {
        if (object instanceof RenderJson) return super.getJsonRender(object);
        return super.getJsonRender(new RenderJson(object));
    }

    public Render getJsonRender(SystemStatus systemStatus) {
        return super.getJsonRender(new RenderJson(systemStatus));
    }

    public Render getJsonRender(SystemStatus systemStatus, Object data) {
        return super.getJsonRender(new RenderJson(systemStatus, data));
    }

    public Render getJsonRender(SystemStatus systemStatus, Object data,String causeMessage) {
        return super.getJsonRender(new RenderJson(systemStatus, data,causeMessage));
    }

}
