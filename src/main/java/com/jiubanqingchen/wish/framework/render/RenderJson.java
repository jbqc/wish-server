package com.jiubanqingchen.wish.framework.render;


import com.jiubanqingchen.wish.framework.enums.SystemStatus;

import java.io.Serializable;

public class RenderJson implements Serializable {
    private static final long serialVersionUID = 1L;

    private int code;

    private String msg;

    private Object data;

    private boolean success = true;

    private Long ts = System.currentTimeMillis();

    private String causeMessage;

    public RenderJson() {

    }

    public RenderJson(Object data) {
        this.code = SystemStatus.SUCCESS.getCode();
        this.msg = SystemStatus.SUCCESS.getMsg();
        this.data = data;
    }

    public RenderJson(SystemStatus systemStatus) {
        this.code = systemStatus.getCode();
        this.msg = systemStatus.getMsg();
    }

    public RenderJson(SystemStatus systemStatus, Object data) {
        this.code = systemStatus.getCode();
        this.msg = systemStatus.getMsg();
        this.data = data;
    }

    public RenderJson(SystemStatus systemStatus, Object data, String causeMessage) {
        this.code = systemStatus.getCode();
        this.msg = systemStatus.getMsg();
        this.data = data;
        this.causeMessage=causeMessage;
    }

    public RenderJson(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public RenderJson(int code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Long getTs() {
        return ts;
    }

    public void setTs(Long ts) {
        this.ts = ts;
    }

    public String getCauseMessage() {
        return causeMessage;
    }

    public void setCauseMessage(String causeMessage) {
        this.causeMessage = causeMessage;
    }
}
