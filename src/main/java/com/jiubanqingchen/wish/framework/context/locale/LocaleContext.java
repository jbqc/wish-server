package com.jiubanqingchen.wish.framework.context.locale;

import java.util.Locale;

/**
 * 语言
 * @author light_dust
 * @since 2021/5/21 17:23
 */
public interface LocaleContext {
    Locale getLocale();
}
