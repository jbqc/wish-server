package com.jiubanqingchen.wish.framework.context.locale;

import java.util.Locale;

/**
 * @author light_dust
 * @since 2021/5/26 10:56
 */
public class SimpleLocaleContext implements LocaleContext{

    private final Locale locale;

    public SimpleLocaleContext( Locale locale) {
        this.locale = locale;
    }

    @Override
    public Locale getLocale() {
        return this.locale;
    }

    @Override
    public String toString() {
        return (this.locale != null ? this.locale.toString() : "-");
    }
}
