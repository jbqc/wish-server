package com.jiubanqingchen.wish.framework.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 唯一列 用于一个表中只能有一条列为
 * @author light_dust
 * @since 2021/6/29 19:31
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface UniqueColumn {
    /**
     *列 多个列用,分割
     */
    String column();

    /**
     *错误信息 支持用国际化key
     */
    String[] errorKeyArray() default {};
}
