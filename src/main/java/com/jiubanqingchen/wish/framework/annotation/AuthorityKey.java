package com.jiubanqingchen.wish.framework.annotation;

import java.lang.annotation.*;

/**
 * @author light_dust
 * @since 2021/4/30 15:20
 */
@Inherited
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface AuthorityKey {
    String value();
}
