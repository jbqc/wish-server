package com.jiubanqingchen.wish.framework.file.word;

/**
 * @author light_dust
 * @since 2020/12/26 20:49
 */
public interface IWord {
    void download();
}
