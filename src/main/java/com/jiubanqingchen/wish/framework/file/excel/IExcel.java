package com.jiubanqingchen.wish.framework.file.excel;

/**
 * @author light_dust
 * @since 2020/12/26 17:40
 */
public interface IExcel {
    void download() throws Exception;
}
