package com.jiubanqingchen.wish.framework.upload;

import com.jfinal.upload.UploadFile;

/**
 * 上传接口
 *
 * @author light_dust
 * @since 2021/8/10 11:36
 */
public interface IWishUpload {
    /**
     * 上传文件 返回 文件上传路径
     * @param uploadFile 上传文件对象
     */
    String upload(UploadFile uploadFile);
}
