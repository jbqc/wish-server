package com.jiubanqingchen.wish.framework.support;

import com.jfinal.aop.Aop;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.CaseInsensitiveContainerFactory;
import com.jfinal.plugin.activerecord.Config;
import com.jfinal.plugin.activerecord.DbKit;
import com.jfinal.plugin.activerecord.IContainerFactory;
import com.jfinal.plugin.activerecord.cache.EhCache;
import com.jfinal.plugin.activerecord.dialect.MysqlDialect;
import com.jfinal.plugin.activerecord.dialect.OracleDialect;
import com.jfinal.plugin.activerecord.dialect.SqlServerDialect;
import com.jfinal.plugin.activerecord.sql.SqlKit;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jiubanqingchen.wish.kit.BlankKit;
import com.jiubanqingchen.wish.model.models.DataSource;
import com.jiubanqingchen.wish.org.dataSource.DataSourceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * 数据源支持类
 *
 * @author light_dust
 * @since 2021/7/2 16:27
 */
public class DataSourceSupport {
    private static final Logger logger = LoggerFactory.getLogger(DataSourceSupport.class);

    /**
     * todo 添加动态数据源
     */
    public void addDynamicDatasource() {
        logger.info("准备添加数据源>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        List<DataSource> list = Aop.get(DataSourceService.class).where("status=?", 1);
        if (BlankKit.notBlankList(list)) {
            Config config;
            for (DataSource ds : list) {
                switch (ds.getDataSourceType()) {
                    case "MYSQL":
                        config = createMysqlConfig(ds);
                        break;
                    case "SQLSERVER":
                        config = createSqlServerConfig(ds);
                        break;
                    case "ORACLE":
                        config = createOracleConfig(ds);
                        break;
                    default:
                        config = null;
                        break;
                }
                if (config == null) {
                    logger.info("为支持数据源类型【{}】，无法新增！！！数据源：名称【{}】编码【{}】", ds.getDataSourceType(), ds.getDataSourceName(), ds.getDataSourceCode());
                    continue;
                }
                logger.info("正在添加【{}】数据源：名称【{}】编码【{}】", ds.getDataSourceType(), ds.getDataSourceName(), ds.getDataSourceCode());
                DbKit.addConfig(config);
            }
        }
        logger.info("添加数据源结束>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
    }

    /**
     * 创建mysql数据源对象
     *
     * @param ds 系统配置的多数据源信息
     */
    public Config createMysqlConfig(DataSource ds) {
        DruidPlugin druidPlugin = createDruidPlugin(ds);
        Config config = new Config(ds.getDataSourceCode(), druidPlugin.getDataSource(), new MysqlDialect(), true, false, 4, IContainerFactory.defaultContainerFactory, new EhCache());
        addSqlTemplate(config, ds.getSqlTemplatePath());
        return config;
    }

    /**
     * 创建sqlserver数据源对象
     *
     * @param ds 系统配置的多数据源信息
     */
    public Config createSqlServerConfig(DataSource ds) {
        DruidPlugin druidPlugin = createDruidPlugin(ds);
        Config config = new Config(ds.getDataSourceCode(), druidPlugin.getDataSource(), new SqlServerDialect(), true, false, 4, new CaseInsensitiveContainerFactory(true), new EhCache());
        addSqlTemplate(config, ds.getSqlTemplatePath());
        return config;
    }

    /**
     * 创建数据源对象
     *
     * @param ds 系统配置的多数据源信息
     */
    public Config createOracleConfig(DataSource ds) {
        DruidPlugin druidPlugin = createDruidPlugin(ds);
        Config config = new Config(ds.getDataSourceCode(), druidPlugin.getDataSource(), new OracleDialect(), true, false, 4, new CaseInsensitiveContainerFactory(true), new EhCache());
        addSqlTemplate(config, ds.getSqlTemplatePath());
        return config;
    }

    /**
     * 创建druid连接池
     *
     * @param ds 系统配置的多数据源信息
     */
    private DruidPlugin createDruidPlugin(DataSource ds) {
        DruidPlugin druidPlugin = new DruidPlugin(ds.getConnUrl(), ds.getUserName(), ds.getUserPassword());
        druidPlugin.start();
        return druidPlugin;
    }

    /**
     * 添加模板文件支持
     *
     * @param config jfinal  Config
     * @param path   模板文件地址
     */
    private void addSqlTemplate(Config config, String path) {
        //如果配置了模板文件则启用模板文件
        if (StrKit.notBlank(path)) {
            SqlKit sqlKit = config.getSqlKit();
            sqlKit.addSqlTemplate(path);
            sqlKit.parseSqlTemplate();//必须加上这个方法，不然获取不到key与值
        }
    }
}
