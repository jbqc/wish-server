package com.jiubanqingchen.wish.framework.exception;

/**
 * @author light_dust
 * @since 2021/4/27 16:40
 */
public class WishExceptionKey {
    //model 相关
    public static final String MODEL_OPERATE_ERROR="system.model.operate.error";
    public static final String MODEL_INSERT_ERROR="system.model.insert.error";
    public static final String MODEL_UPDATE_ERROR="system.model.update.error";
    public static final String MODEL_ID_IS_NULL_ERROR="system.model.id.is.null";
    public static final String MODEL_ID_QUERY_FAIL_CAUSE_NULL="system.model.id.query.fail.cause.null";
    public static final String MODEL_ID_INCONSISTENT_NUMBER_ERROR="system.model.id.inconsistent.number.error";
    public static final String MODEL_CREATE_DAO_ERROR = "system.model.create.dao.error";
    public static final String MODEL_UNIQUE_COLUMN_CHECK_COLUMN_NOT_EMPTY = "system.model.unique.column.check.column.not.empty";
    public static final String MODEL_UNIQUE_COLUMN_CHECK_ALREADY_EXISTS = "system.model.unique.column.check.already.exists";


    //文件相关
    public static final String FILE_NOT_FOUND="system.file.not.found";
    //用户相关
    public static final String USER_ACCOUNT_OR_PASSWORD_ERROR = "system.user.account.or.password.error";
    public static final String USER_NOT_ASSIGN_ROLE = "system.user.not.assign.role";
    public static final String USER_ENCRYP_PASSWORD_IS_NOT_NULL = "system.user.encryp.password.is.not.null";
    public static final String USER_TOKEN_TIME_OUT = "system.user.token.timeout";
    public static final String USER_TOKEN_CREATE_FAIL = "system.user.token.create.fail";
    public static final String USER_ACCOUNT_ALREADY_EXISTS = "system.user.account.already.exists";

    //sql相关
    public static final String SQL_ERROR = "system.sql.error";
    public static final String SQL_KEY_IS_NULL = "system.sql.key.is.null";
    //权限相关
    public static final String AUTHORITY_IS_RE = "system.authority.is.re";
    public static final String AUTHORITY_DEMO_NOT_ALLOWED_DELETE = "system.authority.demo.not.allowed.delete";
    public static final String AUTHORITY_CONTROLLER_NOT_AUTHORITY_kEY="system.authority.controller.not.authorityKey";
    public static final String AUTHORITY_METHOD_NOT_AUTHORITY_kEY="system.authority.method.not.authorityKey";
    public static final String AUTHORITY_USER_NOT_ALLOWED="system.authority.user.not.allowed";
    //定时任务
    public static final String JOB_RUN_ONCE_ERROR = "system.job.run.once.error";
    public static final String JOB_ADD_ERROR = "system.job.add.error";
    public static final String JOB_NO_CLASS_ERROR = "system.job.no.class.error";
    public static final String JOB_CLASS_NOT_ASSIGNABLE_FORM_ERROR = "system.job.class.not.assignable.form.error";
    public static final String JOB_GET_CRON_TRIGGER_ERROR = "system.job.get.cron.trigger.error";
    public static final String JOB_TRIGGER_ERROR = "system.job.trigger.error";
    public static final String JOB_SCHEDULE_ERROR = "system.job.schedule.error";
    public static final String JOB_PAUSE_ERROR = "system.job.pause.error";
    public static final String JOB_RESUME_ERROR = "system.job.resume.error";
    public static final String JOB_REMOVE_ERROR = "system.job.remove.error";
    public static final String JOB_UPDATE_CRON_EXPRESSION_ERROR = "system.job.update.cron.expression.error";
    public static final String JOB_NOT_FOUND="system.job.no.found";

    //工具类
    public static final String KIT_AMAP_URL_NOT_FOUND = "system.kit.amap.url.not.found";
    public static final String KIT_AMAP_KEY_NOT_FOUND="system.kit.amap.key.not.found";

    //多数据源管理
    public static final String ORG_DATA_SOURCE_CODE_ALREADY_EXISTS="org.data.source.code.already.exists";


}
