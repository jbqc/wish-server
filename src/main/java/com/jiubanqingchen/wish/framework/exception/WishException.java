package com.jiubanqingchen.wish.framework.exception;

import com.jiubanqingchen.wish.kit.I18nKit;

/**
 * 抛出指定异常
 *
 * @author light_dust
 * @since 2020/12/22 15:01
 */
public class WishException extends RuntimeException {

    public WishException(String message) {
        super(I18nKit.getLocalMessage(message));
    }

    /**
     * 你可以使用这个构造方法来搞定异常需要变量替换的问题,这个是为后台国际化抛出异常做的一个准备工作 能够更友好的替换掉句子中的特定的名称
     * <pre>new WishException("not find class {} ","AController")=>not find class AController</pre>
     *
     * @param message 异常信息
     * @param paras   参数(可选)
     */
    public WishException(String message, Object... paras) {
        super(I18nKit.getLocalMessage(message, paras));
    }

    public WishException(String message, Throwable cause, Object... paras) {
        super(I18nKit.getLocalMessage(message, paras), cause);
    }

    public WishException(String message, Throwable cause) {
        super(I18nKit.getLocalMessage(message), cause);
    }


}
