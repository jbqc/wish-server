package com.jiubanqingchen.wish.framework.base.controller;

import com.jiubanqingchen.wish.framework.base.user.IUser;

import javax.servlet.http.HttpServletRequest;

/**
 * @author light_dust
 * @since 2021/1/4 17:24
 */
public interface IAdminService {

    IUser getAdminUser(HttpServletRequest request);

    String getCurrentToken(HttpServletRequest request);
}
