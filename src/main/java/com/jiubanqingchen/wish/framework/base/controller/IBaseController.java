package com.jiubanqingchen.wish.framework.base.controller;

import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Record;
import com.jiubanqingchen.wish.framework.base.user.IUser;
import com.jiubanqingchen.wish.framework.enums.SystemStatus;

import java.util.List;

/**
 * BaseController接口 最核心的controller class
 *
 * @author light_dust
 * @since 2020/12/10 20:28
 */
public interface IBaseController {

    /**
     * 获取前台传入
     */
    Record getRawDataToRecord();

    <T> T getRawDataToObject(Class<T> tClass);

    <T> List<T> getRawDataToObjectArray(Class<T> tClass);

    Record getExtDataToRecord();

    Kv getQueryParams();

    void renderJson(SystemStatus systemStatus);

    void renderJson(SystemStatus systemStatus, Object data);

    void renderJson(SystemStatus systemStatus, Object data, String causeMessage);

    IUser getCurrentUser() throws Exception;

    String getCurrentToken();
}
