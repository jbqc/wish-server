package com.jiubanqingchen.wish.framework.base.service;

import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jiubanqingchen.wish.framework.base.user.IUser;

import java.util.List;

/**
 * 实体类service
 *
 * @author light_dust
 * @since 2020/12/22 14:40
 */
public interface IModelService<M extends Model<M>> extends IBaseService {

    void afterDelete(M model, Record record, IUser _usr) throws Exception;

    void afterInsert(M model, Record record, IUser _usr) throws Exception;

    void afterSave(M model, Record record, IUser _usr) throws Exception;

    void afterUpdate(M model, Record record, IUser _usr) throws Exception;

    boolean delete(M model, Record record, IUser _usr) throws Exception;

    boolean deleteBatch(List<M> modelList, Record record, IUser _usr) throws Exception;

    boolean deleteWhere(String where, Object... paras) throws Exception;

    M insert(M model, Record record, IUser _usr) throws Exception;

    boolean insertBatch(List<M> modelList, Record record, IUser _usr) throws Exception;

    void preDelete(M model, Record record, IUser _usr) throws Exception;

    void preInsert(M model, Record record, IUser _usr) throws Exception;

    void preUpdate(M model, Record record, IUser _usr) throws Exception;

    List<M> where(String where, Object... paras) throws Exception;

    M whereFirst(String where, Object... paras) throws Exception;

    void uniqueColumnCheck(String column,String errorKey,M model)throws Exception;

    M update(M model, Record record, IUser _usr) throws Exception;

    boolean updateBatch(List<M> modelList, Record record, IUser _usr) throws Exception;

    List<M> findList(Kv kv) throws Exception;

    List<M> findList(String sqlKey, Kv kv) throws Exception;

    M findById(String id, Kv kv) throws Exception;

    M findById(String sqlKey, String id, Kv kv) throws Exception;

    Page<M> findPage(Kv kv) throws Exception;

    Page<M> findPage(String sqlKey, Kv kv) throws Exception;

    M newModel( Record record, IUser _usr) throws Exception;
}
