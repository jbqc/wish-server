package com.jiubanqingchen.wish.framework.base.controller;

/**
 * model controller 相关
 *
 * @author light_dust
 * @since 2020/12/22 15:20
 */
public interface IModelController {

    void add() throws Exception;

    void addBatch() throws Exception;

    void delete() throws Exception;

    void deleteBatch() throws Exception;

    void list() throws Exception;

    void id() throws Exception;

    void page() throws Exception;

    void update() throws Exception;

    void updateBatch() throws Exception;

    void downloadExcel() throws Exception;

    void newModel() throws Exception;
}
