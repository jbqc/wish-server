package com.jiubanqingchen.wish.framework.base.user;

import java.io.Serializable;
import java.util.List;

/**
 * @author light_dust
 * @since 2020/12/24 17:15
 */
public interface IUser extends Serializable {

    /**
     * 用户id

     */
    String getUserId();

    /**
     * 用户账号

     */
    String getUserAccount();

    /**
     * 用户名称

     */
    String getUserName();

    /**
     * 用户角色id list

     */
    List<String> getRoleList();

    /**
     * 用户主要部门id

     */
    String getMainDeptId();

    /**
     * 用户主要部门名称

     */
    String getMainDeptName();
}
