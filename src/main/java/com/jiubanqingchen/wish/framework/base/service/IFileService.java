package com.jiubanqingchen.wish.framework.base.service;

import com.jfinal.kit.Kv;

import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * @author light_dust
 * @since 2020/12/24 14:37
 */
public interface IFileService {

    void downloadExcel(HttpServletResponse response, Kv kv) throws Exception;

    Map getExcelHeadMap();
}
