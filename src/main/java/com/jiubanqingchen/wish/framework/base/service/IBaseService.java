package com.jiubanqingchen.wish.framework.base.service;

import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.DbTemplate;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import java.util.List;

/**
 * 通用service处理类
 * @author light_dust
 * @since 2020/12/20 10:35
 */
public interface IBaseService {

    List<Record> list(String dataSource, Kv kv) throws Exception;

    List<Record> list(String dataSource, String sqlKey, Kv kv) throws Exception;

    Record id(String dataSource, String id, Kv kv) throws Exception;

    Record id(String dataSource, String sqlKey, String id, Kv kv) throws Exception;

    Page<Record> page(String dataSource, int pageNum, int limit, Kv kv) throws Exception;

    Page<Record> page(String dataSource, String sqlKey, int pageNum, int limit, Kv kv) throws Exception;

    DbTemplate getSqlTemplate(String dataSource,String sqlKey, Kv kv) throws Exception;

    String getHideColumns();

    String getShowColumns();
}
