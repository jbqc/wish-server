package com.jiubanqingchen.wish.framework.enums;

import com.jiubanqingchen.wish.kit.I18nKit;

/**
 * 系统状态码枚举
 * @author light_dust
 * @since 2020/12/10 21:11
 */
public enum SystemStatus {
    /**
     * --------------------------------------------自定义状态码----------------------------------------------
     */

    LOGIN_SUCCESS(10001, "system.status.login.success"),

    LOGIN_ERROR(10002, "system.status.login.error"),

    ERROR_AUTHORITY(10003, "system.status.login.more"),

    LOGIN_MORE(10004, "system.status.error.authority"),

    LOGOUT(10005, "system.status.logout"),

    CAPTCHA_ERROR(10006, "system.status.captcha.error"),

    ERROR(50000, "system.status.error"),

    /**
     * --------------------------------------------表格处理状态码----------------------------------------------
     */

    SUCCESS(60000,"system.status.success"),

    INSERT_SUCCESS(60001, "system.status.insert.success"),

    DELETE_SUCCESS(60002, "system.status.delete.success"),

    UPDATE_SUCCESS(60003, "system.status.update.success");

    private Integer code;

    private String msg;

    SystemStatus(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public static String getName(Integer code) {
        for (SystemStatus sysCode : SystemStatus.values()) {
            if (sysCode.getCode().equals(code)) {
                return sysCode.msg;
            }
        }
        return null;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return I18nKit.getLocalMessage(msg);
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

}
