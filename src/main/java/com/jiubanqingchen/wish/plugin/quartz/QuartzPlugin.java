package com.jiubanqingchen.wish.plugin.quartz;

import com.jfinal.kit.Kv;
import com.jfinal.plugin.IPlugin;
import com.jiubanqingchen.wish.model.models.CronTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author light_dust
 * date 2020/5/20
 */
public class QuartzPlugin implements IPlugin {
    private static final Logger logger = LoggerFactory.getLogger(QuartzPlugin.class);

    @Override
    public boolean start() {
        try {
            logger.info("定时任务正在启动");
            QuartzManager.start();//启动任务
            addAllJob();//添加所有任务
        } catch (Exception e) {
            logger.info(e.getMessage());
            e.printStackTrace();
        }
        return true;
    }

    @Override
    public boolean stop() {
        try {
            QuartzManager.stop();
            logger.info("定时任务关闭结束");
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return false;
    }

    public void addAllJob() {
        QuartzManager.addAllJob(CronTask.dao.template("org_cronTask.list", Kv.by("active", true)).find());
    }

}
