package com.jiubanqingchen.wish.plugin.quartz;

import com.jfinal.ext.kit.DateKit;
import com.jiubanqingchen.wish.framework.exception.WishException;
import com.jiubanqingchen.wish.framework.exception.WishExceptionKey;
import com.jiubanqingchen.wish.model.models.CronTask;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * 定时任务管理类
 *
 * @author light_dust
 * @since 2020/5/21
 */
public class QuartzManager {

    private static final Logger logger = LoggerFactory.getLogger(QuartzManager.class);

    private static Scheduler scheduler;

    public static void init() {
        try {
            SchedulerFactory schedulerFactory = new StdSchedulerFactory("quartz.properties");
            //实例化调度器
            scheduler = schedulerFactory.getScheduler();
        } catch (Exception e) {
            logger.error("QuartzManager init failed", e);
        }

    }

    public static void start() {
        if (scheduler == null) init();
        try {
            if (!scheduler.isStarted()) {
                scheduler.start();
            }
        } catch (SchedulerException e) {
            logger.error("QuartzManager start failed", e);
        }
    }

    public static void stop() {
        if (scheduler == null)
            throw new NullPointerException("You can't stop this scheduler because this scheduler is null");
        try {
            if (!scheduler.isShutdown()) {
                scheduler.shutdown();
            }
        } catch (SchedulerException e) {
            logger.error("QuartzManager stop failed", e);
        }
    }

    /**
     * 添加定时任务
     *
     * @param cronTask 任务对象
     */
    public static void addJob(CronTask cronTask) throws WishException {
        if(scheduler==null)return;
        //得到计划任务表达式
        String cronExpression = cronTask.getCronExpression();
        //得到执行类名称
        String className = cronTask.getClassName();
        String cronTaskName = cronTask.getCronTaskName();
        JobKey key = getJobKey(cronTask.getCronTaskId());
        Class<?> cls;
        try {
            cls = Class.forName(className);
        } catch (ClassNotFoundException e) {
            logger.warn(WishExceptionKey.JOB_NO_CLASS_ERROR, className);
            throw new WishException(WishExceptionKey.JOB_ADD_ERROR, e.getCause(), className);
        }

        if (Job.class.isAssignableFrom(cls)) {
            Class<? extends Job> jobClass = (Class<? extends Job>) cls;
            addJob(key, cronTaskName, cronExpression, jobClass);
        } else {
            throw new WishException(WishExceptionKey.JOB_CLASS_NOT_ASSIGNABLE_FORM_ERROR, className);
        }

    }

    /**
     * 添加一个任务
     *
     * @param jobKey         任务key
     * @param cronTaskName   任务名称
     * @param cronExpression 表达式
     * @param jobClass       调用的class
     */
    public static void addJob(JobKey jobKey, String cronTaskName, String cronExpression, Class<? extends Job> jobClass) throws WishException {
        if(scheduler==null)return;
        if (jobClass != null) {
            // try {
            JobBuilder build = JobBuilder.newJob(jobClass);
            //创建JobDetail实例
            JobDetail jobDetail = build.withIdentity(jobKey).build();

            //创建trigger实例
            TriggerBuilder<Trigger> triggerBuilder = TriggerBuilder.newTrigger();
            CronScheduleBuilder cron = CronScheduleBuilder.cronSchedule(cronExpression).withMisfireHandlingInstructionDoNothing();

            Trigger trigger = triggerBuilder.withIdentity(getTriggerKey(jobKey.toString())).withSchedule(cron).build();
            try {
                scheduler.scheduleJob(jobDetail, trigger);
            } catch (SchedulerException e) {
                logger.error(jobKey.getName() + " ERROR:" + e.getMessage(), e);
                throw new WishException(WishExceptionKey.JOB_SCHEDULE_ERROR, e.getCause(), jobKey.getName(), cronTaskName);
            }
            logger.info("{} 将在 {} 运行...\r\n", jobKey.getName() + cronTaskName, DateKit.toStr(trigger.getNextFireTime(), DateKit.timeStampPattern));
        }
    }

    /**
     * 初始化所有任务
     */
    public static void addAllJob(List<CronTask> cronTaskList) {
        if (cronTaskList == null || cronTaskList.isEmpty()) {
            logger.info("没有配置后台定时任务工作计划!");
            return;
        }
        for (CronTask cronTask : cronTaskList) {
            addJob(cronTask);
        }
    }

    /**
     * 暂停任务
     *
     * @param cronTask 任务对象
     */
    public static void pauseJob(CronTask cronTask) {
        if(scheduler==null)return;
        JobKey jobKey = getJobKey(cronTask.getCronTaskId());
        try {
            if (scheduler.checkExists(jobKey)) {
                scheduler.pauseJob(jobKey);//暂停任务
                logger.info("任务: {} 暂停成功", cronTask.getCronTaskName());
            }
        } catch (SchedulerException e) {
            logger.warn("pause Job cronTaskName: {} failed", jobKey.getName() + cronTask.getCronTaskName(), e);
            throw new WishException(WishExceptionKey.JOB_PAUSE_ERROR, e.getCause(), cronTask.getCronTaskName());
        }
    }

    /**
     * 恢复任务
     *
     * @param cronTask 任务对象
     */
    public static void resumeJob(CronTask cronTask) throws WishException {
        if(scheduler==null)return;
        JobKey jobKey = getJobKey(cronTask.getCronTaskId());
        try {
            if (scheduler.checkExists(jobKey)) {
                scheduler.resumeJob(jobKey);//重启任务
                logger.info("任务:{" + cronTask.getCronTaskName() + "}恢复成功");
            }
        } catch (SchedulerException e) {
            logger.warn("pause Job resumeJob: {} failed", jobKey.getName() + cronTask.getCronTaskName(), e);
            throw new WishException(WishExceptionKey.JOB_RESUME_ERROR, e.getCause(), cronTask.getCronTaskName());
        }
    }

    /**
     * 执行一次定时任务
     *
     * @param cronTask 任务对象
     */
    public static void runOnceJob(CronTask cronTask) throws WishException {
        if(scheduler==null)return;
        String cronTaskName = cronTask.getCronTaskName();
        JobKey jobKey = getJobKey(cronTask.getCronTaskId());
        TriggerKey triggerKey = getTriggerKey(jobKey.toString());
        CronTrigger trigger;
        try {
            trigger = (CronTrigger) scheduler.getTrigger(triggerKey);
        } catch (SchedulerException e) {
            throw new WishException(WishExceptionKey.JOB_GET_CRON_TRIGGER_ERROR, e.getCause(), triggerKey, cronTaskName);
        }
        if (trigger == null) {//如果当前任务不存在则新增
            addJob(cronTask);//新增任务 直接暂停
            pauseJob(cronTask);
        }
        try {
            scheduler.triggerJob(jobKey);
        } catch (SchedulerException e) {
            throw new WishException(WishExceptionKey.JOB_TRIGGER_ERROR, e.getCause(), jobKey, cronTaskName);
        }
        logger.info("任务:{" + cronTaskName + "}执行一次成功");
    }

    /**
     * 删除某一个任务
     *
     * @param cronTask 任务对象
     */
    public static void removeJob(CronTask cronTask) {
        if(scheduler==null)return;
        JobKey jobKey = getJobKey(cronTask.getCronTaskId());
        TriggerKey triggerKey = getTriggerKey(jobKey.toString());
        try {
            if (scheduler.checkExists(triggerKey)) {
                scheduler.pauseTrigger(triggerKey);
                if (scheduler.checkExists(jobKey)) {
                    scheduler.unscheduleJob(triggerKey);
                    scheduler.deleteJob(jobKey);
                }
            }
            logger.info("任务:{" + cronTask.getCronTaskName() + "}移除成功");
        } catch (SchedulerException e) {
            logger.warn("remove Job jobName: {} failed", jobKey.getName() + cronTask.getCronTaskName(), e);
            throw new WishException(WishExceptionKey.JOB_REMOVE_ERROR, e.getCause(), cronTask.getCronTaskName());
        }
    }

    /**
     * 更改任务的执行规则
     *
     * @param cronTask 任务对象
     */
    public static void updateCronExpression(CronTask cronTask) throws WishException {
        if(scheduler==null)return;
        TriggerKey triggerKey = getTriggerKey(cronTask);
        try {
            CronTrigger trigger = (CronTrigger) scheduler.getTrigger(triggerKey);
            if (trigger == null) {
                return;
            }
            String oldCronExpression = trigger.getCronExpression();
            String cronExpression = cronTask.getCronExpression();
            if (!oldCronExpression.equalsIgnoreCase(cronTask.getCronExpression())) {
                // trigger已存在，则更新相应的定时设置
                CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule(cronExpression).withMisfireHandlingInstructionDoNothing();
                // 按新的cronExpression表达式重新构建trigger
                trigger = trigger.getTriggerBuilder().withIdentity(triggerKey).withSchedule(scheduleBuilder).build();
                // 按新的trigger重新设置job执行
                scheduler.rescheduleJob(triggerKey, trigger);
                logger.info("任务: {} 修改执行规则成功", cronTask.getCronTaskName());
            }
        } catch (SchedulerException e) {
            throw new WishException(WishExceptionKey.JOB_UPDATE_CRON_EXPRESSION_ERROR, e.getCause(), cronTask.getCronTaskName());
        }
    }

    /**
     * 获取任务状态
     *
     * @param cronTask NONE: 不存在
     *                 NORMAL: 正常
     *                 PAUSED: 暂停
     *                 COMPLETE:完成
     *                 ERROR : 错误
     *                 BLOCKED : 阻塞
     * @since 2018年5月21日 下午2:13:45
     */
    public static String getTriggerState(CronTask cronTask) {
        if(scheduler==null)return null;
        TriggerKey triggerKey = getTriggerKey(cronTask);
        String name = null;
        try {
            Trigger.TriggerState triggerState = scheduler.getTriggerState(triggerKey);
            name = triggerState.name();
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
        return name;
    }

    /**
     * 得到JobKey
     *
     * @param cronTaskId 任务id
     */
    public static JobKey getJobKey(String cronTaskId) {
        return JobKey.jobKey(getJobKeyName(cronTaskId), getJobKeyGroup(cronTaskId));
    }


    /**
     * 得到triggerKey
     *
     * @param jobKeyStr 得到任务key
     */
    private static TriggerKey getTriggerKey(String jobKeyStr) {
        return TriggerKey.triggerKey(getTriggerName(jobKeyStr), getTriggerGroupName(jobKeyStr));
    }

    private static TriggerKey getTriggerKey(CronTask cronTask) {
        return getTriggerKey(getJobKey(cronTask.getCronTaskId()).toString());
    }

    private static String getJobKeyName(String cronTaskId) {
        return JobConstant.JOB_NAME + cronTaskId;
    }

    private static String getJobKeyGroup(String cronTaskId) {
        return JobConstant.JOB_GROUP + cronTaskId;
    }

    private static String getTriggerName(String jobKeyStr) {
        return JobConstant.TRIGGER_NAME + jobKeyStr;
    }

    private static String getTriggerGroupName(String jobKeyStr) {
        return JobConstant.TRIGGER_GROUP + jobKeyStr;
    }

}
