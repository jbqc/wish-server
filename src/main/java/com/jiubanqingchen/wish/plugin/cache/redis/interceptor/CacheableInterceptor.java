package com.jiubanqingchen.wish.plugin.cache.redis.interceptor;

import cn.hutool.core.annotation.AnnotationUtil;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.plugin.redis.Cache;
import com.jiubanqingchen.wish.plugin.cache.redis.annotation.Cacheable;
import com.jiubanqingchen.wish.plugin.cache.redis.kit.CacheKit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Method;

/**
 * cacheable 拦截器 处理缓存数据
 * @author light_dust
 * @since 2020/9/29 14:43
 * @version 0.0.1
 */
public class CacheableInterceptor implements Interceptor {
    private static final Logger logger = LoggerFactory.getLogger(CacheableInterceptor.class);

    @Override
    public void intercept(Invocation inv) {
        Method method = inv.getMethod();
        Cacheable cacheable = method.getAnnotation(Cacheable.class);
        if (cacheable == null) {
            inv.invoke();
            return;
        }
        Object[] args = inv.getArgs();//得到参数数组

        String cacheName = AnnotationUtil.getAnnotationValue(method, Cacheable.class, "name"); //得到缓存名称
        String key = AnnotationUtil.getAnnotationValue(method, Cacheable.class, "key");//得到缓存key值
        int liveSeconds = AnnotationUtil.getAnnotationValue(method, Cacheable.class, "liveSeconds");//得到缓存存放时间 单位/秒
        String cacheKey = CacheKit.me().createCacheKey(method, args, key);//得到redis缓存key值 根据一定的创建规则创建

        //得到缓存cache对象 以及缓存值
        Cache cache = CacheKit.me().getCache(cacheName);
        Object object = cache.get(cacheKey);//获取缓存数据
        if (object == null) {//如果数据为空则回去
            inv.invoke();
            Object obj = inv.getReturnValue();
            if (liveSeconds == 0) {
                cache.set(cacheKey, obj);
            } else {
                cache.setex(cacheKey, liveSeconds, obj);
            }
            return;
        }
        inv.setReturnValue(object);
    }
}
