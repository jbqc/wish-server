package com.jiubanqingchen.wish.plugin.cache.redis.annotation;

import java.lang.annotation.*;

/**
 * redis注解 开启缓存数据 此处借鉴jboot
 * @author light_dust
 * @since 2020/9/29 11:15
 * @version 0.0.1
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface Cacheable {

    /**
     * 缓存名称
     */
    String name() default "system";//默认缓存为system

    /**
     * 缓存的key
     */
    String key();

    /**
     * 缓存时间，单位秒
     * 默认情况下，缓存永久有效
     */
    int liveSeconds() default 0; // 0，系统配置默认，默认情况下永久有效

    /**
     * 是否对 null 值进行缓存
     */
    boolean nullCacheEnable() default false;

    /**
     * 是否对返回的数据进行 Copy 后返回
     */
    boolean returnCopyEnable() default false;

    /**
     * 在什么情况下不进行缓存
     * 这里编写的是 JFinal 模板引擎的表达式
     */
    String unless() default "";

}
