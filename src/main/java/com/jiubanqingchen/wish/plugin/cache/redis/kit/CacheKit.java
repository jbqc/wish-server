package com.jiubanqingchen.wish.plugin.cache.redis.kit;


import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.redis.Cache;
import com.jfinal.plugin.redis.Redis;
import com.jfinal.template.Engine;
import com.jiubanqingchen.wish.kit.ApplicationKit;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * CacheableInterceptor 工具类
 *
 * @author light_dust
 * @version 0.0.1
 * @since 2020/10/9 15:46
 */
public class CacheKit {
    private static final CacheKit me = new CacheKit();

    public static CacheKit me() {
        return me;
    }

    /**
     * 创建缓存key
     *
     * @param method 方法名
     * @param args   参数
     * @param key    指定的key值
     * @since 2020/10/9 15:50
     **/
    public String createCacheKey(Method method, Object[] args, String key) {
        return cacheEngineRender(key, method, args);
    }

    /**
     * 创建缓存key
     *
     * @param template key表达式
     * @param record   表达式的值
     */
    public String createCacheKey(String template, Record record) {
        return createCacheKey(template, record.getColumns());
    }

    /**
     * 创建缓存key
     *
     * @param template key表达式
     * @param dataMap  表达式的值
     */
    public String createCacheKey(String template, Map<String, Object> dataMap) {
        return new Engine().getTemplateByString(template).renderToString(dataMap);
    }

    /**
     * 渲染模板语法得到值
     *
     * @param template  模板语法 jfinal enjoy #(xx)
     * @param method    方法
     * @param arguments 参数
     * @since 2020/10/10 9:48
     **/
    public String cacheEngineRender(String template, Method method, Object[] arguments) {
        Map<String, Object> dataMap = new HashMap<>();
        int x = 0;
        for (Parameter p : method.getParameters()) {
            if (!p.isNamePresent()) {
                // 必须通过添加 -parameters 进行编译，才可以获取 Parameter 的编译前的名字
                throw new RuntimeException(" Maven or IDE config is error. see https://www.jfinal.com/doc/3-3 ");
            }
            dataMap.put(p.getName(), arguments[x++]);
        }
        return new Engine().getTemplateByString(template).renderToString(dataMap);
    }

    /**
     * 得到当前名称的redis 缓存
     *
     * @param cacheName 缓存名称 可以为空
     * @since 2020/10/9 15:59
     **/
    public Cache getCache(String cacheName) {
        if (StrKit.isBlank(cacheName)) {
            return getCache();
        } else {
            return Redis.use(cacheName);
        }
    }

    /**
     * 获取默认cache
     *
     * @return redis cache对象
     */
    public Cache getCache() {
        return Redis.use(ApplicationKit.me().getRedisCacheName());
    }


    /**
     * 获取当前前缀的所有缓存key
     *
     * @param cacheName 缓存名称
     * @param prefixKey 缓存key前缀
     */
    public String[] getKeysByPrefixKey(String cacheName, String prefixKey) {
        Set<String> keys = getCache(cacheName).keys(prefixKey + "*");
        if (keys == null || keys.isEmpty()) return null;
        return keys.toArray(new String[0]);
    }

    /**
     * 删除当前前缀的所有缓存key
     *
     * @param cacheName 缓存名称
     * @param prefixKey 缓存key前缀
     */
    public void deleteKeysByPrefixKey(String cacheName, String prefixKey) {
        Cache cache = getCache(cacheName);
        Set<String> keys = cache.keys(prefixKey + "*");
        if (keys != null && !keys.isEmpty()) {
            cache.del(keys.toArray());
        }
    }

    /**
     * 删除所有缓存
     *
     * @param cacheName 缓存名称
     */
    public void deleteAllCache(String cacheName) {
        Cache cache = getCache(cacheName);
        cache.flushAll();
    }
}
