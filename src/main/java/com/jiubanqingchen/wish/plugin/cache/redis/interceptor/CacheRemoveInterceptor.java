package com.jiubanqingchen.wish.plugin.cache.redis.interceptor;

import cn.hutool.core.annotation.AnnotationUtil;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jiubanqingchen.wish.plugin.cache.redis.annotation.CacheRemove;
import com.jiubanqingchen.wish.plugin.cache.redis.kit.CacheKit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Method;

/**
 * redis拦截器 清理缓存数据
 * @author light_dust
 * @since 2020/10/13 10:42
 * @version 0.0.1
 */
public class CacheRemoveInterceptor implements Interceptor {
     private static final Logger logger = LoggerFactory.getLogger(CacheRemoveInterceptor.class);

    @Override
    public void intercept(Invocation inv) {
        Method method = inv.getMethod();
        CacheRemove cacheRemove = method.getAnnotation(CacheRemove.class);
        inv.invoke();
        if (cacheRemove != null) {
            Object[] args = inv.getArgs();//得到参数数组
            String cacheName = AnnotationUtil.getAnnotationValue(method, CacheRemove.class, "name"); //得到缓存名称
            String key = AnnotationUtil.getAnnotationValue(method, CacheRemove.class, "key");//得到缓存key值
            String cacheKey = CacheKit.me().createCacheKey(method, args, key);//得到redis缓存key值 根据一定的创建规则创建
            logger.info("cacheKey:" + cacheKey);
            CacheKit.me().getCache(cacheName).del(cacheKey);
        }
    }
}
