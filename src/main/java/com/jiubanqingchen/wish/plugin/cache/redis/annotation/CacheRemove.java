package com.jiubanqingchen.wish.plugin.cache.redis.annotation;

import java.lang.annotation.*;

@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface CacheRemove {
    /**
     * 缓存名称
     *

     */
    String name() default "system";//默认缓存为system

    /**
     * 缓存的key
     *

     */
    String key();
}
