package com.jiubanqingchen.wish.plugin.rpt;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 解析enjoy指令
 */
public class EnjoyTool {

    /**
     * 匹配表达式
     *
     * @param str html字符串
     * @param pattern 表达式
     */
    public static List<String> patternExpression(String str, String pattern) {
        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(str);
        List<String> matchList = null;
        while (m.find()) {
            if (matchList == null) matchList = new ArrayList<>();
            /*
             * 自动遍历打印所有结果 group方法打印捕获的组内容，以正则的括号角标从1开始计算，我们这里要第2个括号里的 值， 所以取 m.group(2)，
             * m.group(0)取整个表达式的值，如果越界取m.group(4),则抛出异常
             */
            System.out.println("Found value: " + m.group(2));
            matchList.add(m.group(2));
            //return m.group(2);
        }
        return matchList;
    }

    /**
     * 解析enjoy表达式 得到属性值
     * 例子：#(for) #(name) =>List<String> [for,name]
     *
     * @param str html字符串
     * @return 匹配的属性集合
     */
    public static List<String> patternEnjoyExpression(String str) {
        String pattern = "(#\\()(.*?)(\\))";
        return patternExpression(str, pattern);
    }

    /**
     * 匹配判断是否有#for标签
     *
     * @param str html字符串
     */
    public static List<String> patternEnjoyForExpression(String str) {
        String pattern = "(#for\\()(.*?)(\\))";
        return patternExpression(str, pattern);
    }

    /**
     * 判断是否包含#end标签
     *
     * @param str html字符串
     */
    public boolean containEnjoyEndExpression(String str) {
        return str.contains("#end");
    }

    /**
     * 判断是否未end标签
     *
     * @param str html字符串
     */
    public static boolean isEndLabelExpression(String str) {
        return ("#end".equals(str));
    }

    /**
     * 删除在字符串中最后一次出现的字符串
     *
     * @param str       字符串
     * @param removeStr 被删除字符串
     * @return 移除被删除字符串的字符串
     */
    public static String removeLastIndexOf(String str, String removeStr) {
        int replaceLen = removeStr.length();
        int lastStart = str.lastIndexOf(removeStr);
        int lastEnd = lastStart + replaceLen;
        return str.substring(0, lastStart) + str.substring(lastEnd);
    }

    /**
     * 处理特殊字符
     */
    public static String  handlingSpecialChar(String str){
        String resString=str.replaceAll("&lt;","<");
        resString=resString.replaceAll("&gt;",">");
        return resString;
    }
}
