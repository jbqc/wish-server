package com.jiubanqingchen.wish.plugin.easyexcel.convert;

import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.enums.CellDataTypeEnum;
import com.alibaba.excel.metadata.CellData;
import com.alibaba.excel.metadata.GlobalConfiguration;
import com.alibaba.excel.metadata.property.ExcelContentProperty;
import com.jfinal.kit.StrKit;

import java.sql.Date;

/**
 * @author light_dust
 * @since 2021/5/11 15:10
 */
public class SqlDateStringConverter implements Converter<Date> {
    @Override
    public Class<Date> supportJavaTypeKey() {
        return Date.class;
    }

    @Override
    public CellDataTypeEnum supportExcelTypeKey() {
        return CellDataTypeEnum.STRING;
    }

    @Override
    public Date convertToJavaData(CellData cellData, ExcelContentProperty contentProperty, GlobalConfiguration globalConfiguration) throws Exception {
        String s = cellData.getStringValue();
        return StrKit.isBlank(s) ? null : Date.valueOf(s);
    }

    @Override
    public CellData<String> convertToExcelData(Date value, ExcelContentProperty contentProperty, GlobalConfiguration globalConfiguration) throws Exception {
        return value == null ? null : new CellData<>(value.toString());
    }
}
