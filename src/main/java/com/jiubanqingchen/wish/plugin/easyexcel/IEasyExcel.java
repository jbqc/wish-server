package com.jiubanqingchen.wish.plugin.easyexcel;


import com.jiubanqingchen.wish.framework.file.excel.IExcel;

/**
 * @author light_dust
 * @since 2020/12/26 17:41
 */
public interface IEasyExcel extends IExcel {
}
