package com.jiubanqingchen.wish.plugin.easyexcel.style.column;

import com.alibaba.excel.metadata.CellData;
import com.alibaba.excel.metadata.Head;
import com.alibaba.excel.write.metadata.holder.WriteSheetHolder;
import com.alibaba.excel.write.style.column.AbstractColumnWidthStyleStrategy;
import com.jfinal.kit.PropKit;
import org.apache.poi.ss.usermodel.Cell;

import java.util.List;

/**
 * 自定义列宽
 * @author light_dust
 * @since 2020/12/26 12:41
 */
public class CustomWidthStyleStrategy extends AbstractColumnWidthStyleStrategy {

    private final int columnWidth;

    @Override
    protected void setColumnWidth(WriteSheetHolder writeSheetHolder, List<CellData> list, Cell cell, Head head, Integer integer, Boolean aBoolean) {
        writeSheetHolder.getSheet().setColumnWidth(cell.getColumnIndex(), columnWidth * 256);
    }

    public CustomWidthStyleStrategy() {
        this.columnWidth = PropKit.getInt("wish.excel.width",24);
    }

    public CustomWidthStyleStrategy(int columnWidth) {
        this.columnWidth = columnWidth;
    }

}
