package com.jiubanqingchen.wish.plugin.easyexcel;

import com.jfinal.plugin.IPlugin;
import com.jiubanqingchen.wish.plugin.easyexcel.convert.CustomConverterLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author light_dust
 * @since 2020/12/25 15:49
 */
public class EasyExcelPlugin implements IPlugin {
    private final Logger logger= LoggerFactory.getLogger(EasyExcelPlugin.class);
    @Override
    public boolean start() {
        try {
            new CustomConverterLoader().init();
        }catch (IllegalAccessException e){
            logger.error("EasyExcel plugin start fail!",e);
        }
        return true;
    }

    @Override
    public boolean stop() {
        return true;
    }
}
