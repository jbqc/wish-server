package com.jiubanqingchen.wish.plugin.easyexcel.json;

import com.jiubanqingchen.wish.kit.IntKit;
import com.jiubanqingchen.wish.plugin.easyexcel.consts.ExcelCode;

import java.io.Serializable;

/**
 * @author light_dust
 * @since 2021/5/17 17:02
 */
public class WebSocketExcelJson implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 返回状态码
     */
    private int code;
    /**
     * 返回信息
     */
    private String msg;
    /**
     * 总数
     */
    private int total;
    /**
     * 次数,下标
     */
    private int index;
    /**
     * 进度百分比
     */
    private double percent;

    public WebSocketExcelJson() {

    }

    public WebSocketExcelJson(int code,String msg) {
        this.code = code;
        this.msg = msg;
    }

    public WebSocketExcelJson(String msg) {
        this.code = ExcelCode.SUCCESS_CODE;
        this.msg = msg;
    }

    public WebSocketExcelJson(int index, int total, String msg) {
        this.index = index;
        this.total = total;
        this.code = ExcelCode.SUCCESS_CODE;
        this.msg = msg;
        //计算百分比
        this.percent = IntKit.calcPercent(index, total);
    }


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public double getPercent() {
        return percent;
    }

    public void setPercent(double percent) {
        this.percent = percent;
    }
}
