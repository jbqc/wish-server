package com.jiubanqingchen.wish.plugin.easyexcel.consts;

/**
 * @author light_dust
 * @since 2021/5/17 17:27
 */
public class ExcelCode {
    /** 成功状态码*/
    public static final int SUCCESS_CODE = 200;
    /** 失败状态码*/
    public static final int ERROR_CODE=500;
}
