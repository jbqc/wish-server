package com.jiubanqingchen.wish.plugin.cos.qcloud;

import cn.hutool.core.date.DateUtil;
import com.jfinal.upload.UploadFile;
import com.jiubanqingchen.wish.framework.upload.IWishUpload;

import java.util.Date;

/**
 * @author light_dust
 * @since 2021/8/10 11:39
 */
public class CosUpload implements IWishUpload {

    /**
     * 地区名称
     */
    private final String regionName;
    /**
     * 储存桶名称
     */
    private final String bucketName;
    /**
     * 储存文件夹根地址
     */
    private final String folderBasePath;

    public CosUpload(String regionName, String bucketName, String folderBasePath) {
        this.regionName = regionName;
        this.bucketName = bucketName;
        this.folderBasePath = folderBasePath;
    }

    @Override
    public String upload(UploadFile uploadFile) {
        String foldUrl = createFolderPath();
        String newFileName = rename(uploadFile);
        String uploadFileUrl = foldUrl + newFileName;
        //创建cosUploadFile对象
        CosUploadFile cosUploadFile = new CosUploadFile(this.regionName, this.bucketName, uploadFileUrl, uploadFile.getFile());
        //上传到储存桶
        CosSupport.me().UploadFileToBucket(cosUploadFile);
        //清理上传文件
        uploadFile.getFile().deleteOnExit();
        return uploadFileUrl;
    }

    /**
     * 创建文件上传的文件夹路径 不同的上传可以使用不同的传输
     */
    public String createFolderPath() {
        return folderBasePath + DateUtil.format(new Date(), "yyyy-MM-dd") + "/";
    }

    /**
     * 文件重命名 按照一定的规则
     *
     * @param uploadFile 上传文件对象
     */
    public String rename(UploadFile uploadFile) {
        String fileName = uploadFile.getFileName();
        return DateUtil.format(new Date(), "yyyyMMddHHmmss") + fileName.substring(fileName.lastIndexOf("."));
    }
}
