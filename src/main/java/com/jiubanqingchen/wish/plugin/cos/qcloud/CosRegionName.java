package com.jiubanqingchen.wish.plugin.cos.qcloud;

/**
 * @author light_dust
 * @since 2021/7/31 11:59
 */

/**
 * 腾讯云地域名称key
 */
public class CosRegionName {
    /**公有云 广州*/
    public static final String GUANG_ZHOU="ap-guangzhou";
    /**公有云 广州*/
    public static final String BEI_JING="ap-beijing";
    /**公有云 广州*/
    public static final String NAN_JING="ap-nanjing";
    /**公有云 广州*/
    public static final String SHANG_HAI="ap-shanghai";
    /**公有云 广州*/
    public static final String CHENG_DU="ap-chengdu";
    /**公有云 广州*/
    public static final String CHONG_QING="ap-chongqing";
}
