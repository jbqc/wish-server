package com.jiubanqingchen.wish.plugin.cos.qcloud;

import java.io.File;

public class CosUploadFile {
    /**
     * 地区名称
     *
     * @see CosRegionName
     */
    private String regionName;
    /**
     * 储存桶名
     */
    private String bucketName;
    /**
     * 储存url 全路径
     */
    private String url;
    /**
     * 文件对象
     */
    private File file;

    public CosUploadFile(String regionName, String bucketName, String url, File file) {
        this.regionName = regionName;
        this.bucketName = bucketName;
        this.url = url;
        this.file = file;
    }

    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }
}
