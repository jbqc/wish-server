package com.jiubanqingchen.wish.plugin.cos.qcloud;

import com.jfinal.kit.PropKit;
import com.qcloud.cos.COSClient;
import com.qcloud.cos.ClientConfig;
import com.qcloud.cos.auth.BasicCOSCredentials;
import com.qcloud.cos.auth.COSCredentials;
import com.qcloud.cos.http.HttpProtocol;
import com.qcloud.cos.model.PutObjectRequest;
import com.qcloud.cos.region.Region;

/**
 * @author light_dust
 * @since 2021/7/31 11:53
 */
public class CosSupport {
    private static final CosSupport me = new CosSupport();

    public static CosSupport me() {
        return me;
    }

    /**
     * 获取地区储cos
     *
     * @param regionName 地域名称
     */
    public COSClient getCosClientByRegionName(String regionName) {
        // 1 初始化用户身份信息（secretId, secretKey）。
        // SECRETID和SECRETKEY请登录访问管理控制台进行查看和管理
        String secretId = PropKit.get("tencent.cloud.secretId");
        String secretKey = PropKit.get("tencent.cloud.secretKey");
        COSCredentials cred = new BasicCOSCredentials(secretId, secretKey);
        // 2 设置 bucket 的地域, COS 地域的简称请参照 https://cloud.tencent.com/document/product/436/6224
        // clientConfig 中包含了设置 region, https(默认 http), 超时, 代理等 set 方法, 使用可参见源码或者常见问题 Java SDK 部分。
        Region region = new Region(regionName);
        ClientConfig clientConfig = new ClientConfig(region);
        // 这里建议设置使用 https 协议
        clientConfig.setHttpProtocol(HttpProtocol.https);
        // 3 生成 cos 客户端。
        return new COSClient(cred, clientConfig);
    }

    /**
     * 上传文件到腾讯云cos
     */
    public void UploadFileToBucket(CosUploadFile cosUploadFile){
        PutObjectRequest putObjectRequest = new PutObjectRequest(cosUploadFile.getBucketName(), cosUploadFile.getUrl(), cosUploadFile.getFile());
        getCosClientByRegionName(cosUploadFile.getRegionName()).putObject(putObjectRequest);
    }
}
