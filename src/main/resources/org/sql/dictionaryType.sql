#sql("list")###数据字典子项查询
SELECT t1.* FROM wish_dictionary_type as t1 WHERE 1=1
    #@dictionaryTypeWheres()
    #@order(prop,sort,"order by sortNo")
#end

#define dictionaryTypeWheres()
    #if(id)
        AND t1.dictionaryTypeId=#para(id)
    #end
    #if(dictionaryTypeId)
        AND t1.dictionaryTypeId=#para(dictionaryTypeId)
    #end

    #if(codeItemId)
        AND t1.codeItemId #paraLike(codeItemId)
    #end
    #if(codeItemName)
        AND t1.codeItemName #paraLike(codeItemName)
    #end
#end