###@author light_dust_generator
###@since 2021-06-10 21:31:08
#sql("list")
SELECT t1.* FROM wish_demo as t1 WHERE 1=1
    #@DemoWheres()
#end

#define DemoWheres()
        #if(demoId)
            AND t1.demoId=#para(demoId)
        #end
        #if(demoName)
            AND t1.demoName #paraLike(demoName)
        #end
        #if(demoDesc)
            AND t1.demoDesc #paraLike(demoDesc)
        #end
#end

