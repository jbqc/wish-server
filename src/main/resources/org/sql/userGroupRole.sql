###基础查询
#sql("list")
select t1.*,
       CASE
           WHEN userGroupType = 'DEPT' THEN
               t2.deptName
           WHEN userGroupType = 'USER' THEN
               t3.userName
           END AS userGroupName
        ,getDictionaryDesc('ORG_USER_ROLE_GROUP_TYPE',t1.userGroupType) as userGroupTypeDesc,
       t4.roleName, t4.roleDescription
from wish_user_group_role t1
         LEFT JOIN wish_dept t2 ON t1.userGroupId = t2.deptId
         LEFT JOIN wish_user t3 ON t1.userGroupId = t3.userId
         LEFT JOIN wish_role t4 ON t1.roleId=t4.roleId
WHERE 1=1
  #@userGroupRoleWheres()
  #@order(prop,sort,"order by t1.userGroupType")
#end

#define userGroupRoleWheres()
  #if(id)
    AND t1.userGroupRoleId=#para(id)
  #end

  #if(userGroupId)
    AND t1.userGroupId=#para(userGroupId)
  #end

  #if(userGroupType)
    AND t1.userGroupType=#para(userGroupType)
  #end

  #if(roleId)
    AND t1.roleId=#para(roleId)
  #end
  #if(userId)
    AND t1.userGroupId=#para(userId)
  #end
#end

###查询用户的角色信息
#sql("userRoleList")
select t1.roleId,
       t1.roleName,
       t1.roleType,
       t1.isValid,
       t1.roleDescription,
       getDictionaryDesc('ORG_ROLE_TYPE', t1.roleType) as roleTypeDesc,
       t2.userGroupRoleId,
       t2.userGroupType
from wish_role as t1
         left join wish_user_group_role as t2 on t1.roleId = t2.roleId
     #@userRoleListWheres()
#end

#define userRoleListWheres()
    where 1=1
     #if(userId)
      and t2.userGroupType = 'USER'
      and t2.userGroupId =#para(userId)
    #end
    #if(isValid)
        and t1.isValid=#(isValid)
    #end
#end