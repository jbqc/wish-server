#sql("list")###用户查询sql
    SELECT t1.*
    ,getDictionaryDesc('ORG_USER_SEX',t1.userSex) as userSexDesc
    FROM wish_user t1
    #@userWheres()
    #@order(prop,sort,"order by t1.userAccount")
#end

#define userWheres()
    WHERE 1=1
    #if(userId)
        AND t1.userId=#para(userId)
    #end
    #if(userAccount)
        AND t1.userAccount #paraLike(userAccount)
    #end
    #if(userName)
        AND t1.userName #paraLike(userName)
    #end
    #if(userSex)
        AND t1.userSex=#para(userSex)
    #end
    #if(likeAll)
        AND (t1.userAccount #paraLike(likeAll) or t1.userName #paraLike(likeAll))
    #end

    #if(deptId && deptId !="qc")###链表查询当前部门下的用户信息(如果为轻尘科技则查询所有数据)
        AND exists (select 1 from wish_user_dept fud where fud.userId=t1.userId and fud.deptId=#para(deptId))
    #end

    #if(excludeRoleId)###排除已经与当前角色关联的用户
        AND not exists (select 1 from wish_user_group_role as t2 where t2.roleId=#para(excludeRoleId) and t2.userGroupId=t1.userId)
    #end
#end

