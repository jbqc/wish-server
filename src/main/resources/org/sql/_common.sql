###配置通用函数
#定义默认排序prop:数据库列字段 sort:asc desc defaultOrder:默认的order by排序
#define order(prop,sort,defaultOrder)
#if(prop)
order by #(prop) #(sort)
    #else
        #(defaultOrder)
    #end
#end

###in查询
#define genIn(ids)
            in (
	#for (id : ids)
		#(for.first ? "" : ",") #para(id)
	#end
   )
#end