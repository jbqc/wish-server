#sql("list")###接口查询sql
    SELECT t1.*,getDictionaryDesc('ORG_APP_ACTION_TYPE',t1.actionType) as actionTypeDesc
    FROM wish_app_action t1
    #@actionWheres()
    #@order(prop,sort,"order by t1.sortNo")
#end

#define actionWheres()
    WHERE 1=1
    #if(actionId)
        AND t1.actionId=#para(actionId)
    #end
    #if(parentId)
      AND t1.parentId=#para(parentId)
    #end
    #if(actionType)
      AND t1.actionType=#para(actionType)
    #end
    #if(authorityKey)
      AND t1.authorityKey=#para(authorityKey)
    #end
    #if(authorityKeyLike)
      AND t1.authorityKey #paraLike(authorityKeyLike)
    #end
    #if(actionName)
      AND t1.actionName #paraLike(actionName)
    #end
#end