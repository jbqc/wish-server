#sql("list")
select t1.*
from wish_resource_authority as t1 WHERE 1 = 1
     #@resourceAuthorityWheres()
#end

#define resourceAuthorityWheres()
  #if(roleId)
     AND t1.roleId = #para(roleId)
  #end
#end

#查询多个资源id关联的角色
#sql("findRoleIdByResourceIds")
SELECT DISTINCT roleId FROM wish_resource_authority WHERE resourceId #@genIn(resourceIds);
#end