###@author light_dust_generator
###@since 2021-08-23 14:21:49
#sql("list")
SELECT t1.*,getDictionaryDesc('ORG_NOTICE_TYPE',t1.type) as typeDesc FROM wish_notice as t1 WHERE 1=1
    #@NoticeWheres()
#end

#define NoticeWheres()
        #if(noticeId)
            AND t1.noticeId=#para(noticeId)
        #end
        #if(title)
            AND t1.title #paraLike(title)
        #end
        #if(type)
            AND t1.type=#para(type)
        #end
        #if(status)
            AND t1.status = #(status)
        #end
#end

