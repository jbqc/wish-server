#sql("list")###角色查询sql
SELECT t1.*
     ,getDictionaryDesc('ORG_ROLE_TYPE',t1.roleType) as roleTypeDesc
FROM wish_role as t1
   #@roleWheres()
#end

#define roleWheres()
     WHERE 1=1
    #if(roleId)
        AND t1.roleId=#para(roleId)
    #end

    #if(roleName)
        AND t1.roleName #paraLike(roleName)
    #end
    #if(isValid)
        AND t1.isValid = #(isValid)
    #end
    #if(roleType)
        AND t1.roleType = #paraLike(roleType)
    #end

    #if(includeUserId)
        AND EXISTS(SELECT 1 FROM wish_user_group_role as t2 where t1.roleId=t2.roleId and t2.userGroupType='USER' and t2.userGroupId=#para(includeUserId))
    #end

    #if(excludeUserId)
        AND NOT EXISTS(SELECT 1 FROM wish_user_group_role as t2 where t1.roleId=t2.roleId and t2.userGroupType='USER' and t2.userGroupId=#para(excludeUserId))
    #end
#end

