###权限管理部分sql
#sql("findUserDept")###查询用户部门 包括部门名称
    select t1.* from wish_dept as t1 where t1.valid=true and exists(select 1 from wish_user_dept t2 where t2.userId=? and  t1.deptId=t2.deptId);
#end

#sql("findUserMainDept")###查询用户部门 包括部门名称
    select t1.* from wish_dept as t1 where t1.valid=true and exists(select 1 from wish_user_dept t2 where t2.userId=? and t2.main=true and  t1.deptId=t2.deptId);
#end

#sql("findUserRole")###查询用户角色(这里包括用户的角色 以及部门的角色)
    select t1.* from wish_role t1 inner join wish_user_group_role t2 on t2.roleId=t1.roleId
    where ((t2.userGroupId=? and t2.userGroupType='USER')
    OR (t2.userGroupType='DEPT' and EXISTS (SELECT 1 FROM wish_user_dept t3 WHERE t3.valid=true and  t3.userId =? AND t3.deptId = t2.userGroupId)));
#end

#sql("findRoleAuthority")###查询角色可使用的接口权限key 这个查询是为后台准备的 用来做接口鉴权
    select t1.authorityKey from wish_app_action as t1 where EXISTS (select 1 from wish_resource_authority as t2 where t2.roleId=? and t2.resourceCategory=? and t2.resourceType=? and t1.actionId=t2.resourceId);
#end

#sql("findRoleResource")###查询角色可使用的菜单,接口权限资源id
    select t1.resourceId from wish_resource_authority as t1 where t1.roleId=? and t1.resourceCategory=? and t1.resourceType=?;
#end

#sql("findRoleAssignAuthorityKey")###查询角色可分配的权限key
    select t1.authorityKey from wish_menu as t1 where EXISTS (select 1 from wish_resource_authority as t2 where t2.roleId=? and t2.resourceCategory='ASSIGN'
    and t2.resourceType!=? and t1.menuId=t2.resourceId);
#end