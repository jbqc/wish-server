###@author light_dust_generator
###@since 2021-06-30 11:44:50
#sql("list")
SELECT t1.* FROM wish_data_source as t1 WHERE 1=1
    #@DataSourceWheres()
#end

#define DataSourceWheres()
        #if(dataSourceId)
            AND t1.dataSourceId=#para(dataSourceId)
        #end
        #if(likeAll)
            AND (t1.dataSourceCode #paraLike(likeAll) or t1.dataSourceName #paraLike(likeAll) )
        #end
        #if(status)
            AND t1.status=#para(status)
        #end
        #if(dataSourceType)
            AND t1.dataSourceType=#para(dataSourceType)
        #end
#end

