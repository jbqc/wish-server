###引入wish-org所有sql
###通用功能
#include("_common.sql")

###用户管理
#namespace("org_user")
    #include("user.sql")
#end

###部门管理
#namespace("org_dept")
    #include("dept.sql")
#end

###角色管理
#namespace("org_role")
    #include("role.sql")
#end

###用户部门管理
#namespace("org_userDept")
    #include("userDept.sql")
#end

###权限模块管理
#namespace("org_auth")
    #include("auth.sql")
#end

###数据字典子项
#namespace("org_dictionary")
    #include("dictionary.sql")
#end

###数据字典类型
#namespace("org_dictionaryType")
    #include("dictionaryType.sql")
#end

###角色用户组
#namespace("org_userGroupRole")
    #include("userGroupRole.sql")
#end

###角色权限
#namespace("org_resourceAuthority")
    #include("resourceAuthority.sql")
#end

###菜单权限
#namespace("org_menu")
    #include("menu.sql")
#end

###接口
#namespace("org_appAction")
    #include("appAction.sql")
#end


###定时任务
#namespace("org_cronTask")
    #include("cronTask.sql")
#end

###演示环境
#namespace("org_demo")
    #include("demo.sql")
#end

###登陆日志
#namespace("org_loginLog")
    #include("loginLog.sql")
#end

###多数据源管理
#namespace("org_dataSource")
    #include("dataSource.sql")
#end

###多数据源管理
#namespace("org_cache")
    #include("cache.sql")
#end

###通知管理
#namespace("org_notice")
    #include("notice.sql")
#end