/*
 Navicat Premium Data Transfer

 Source Server         : wishdb轻尘1号
 Source Server Type    : MySQL
 Source Server Version : 80025
 Source Host           : 1.14.164.133:3306
 Source Schema         : wishdb

 Target Server Type    : MySQL
 Target Server Version : 80025
 File Encoding         : 65001

 Date: 09/07/2021 11:29:32
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for wish_app_action
-- ----------------------------
DROP TABLE IF EXISTS `wish_app_action`;
CREATE TABLE `wish_app_action`  (
  `actionId` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '接口主键',
  `actionName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '接口名称',
  `actionType` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '接口类型(类/方法)',
  `sortNo` int NOT NULL COMMENT '排序',
  `authorityKey` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '权限key',
  `parentId` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父接口id',
  `createPerson` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `modifyPerson` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `modifyTime` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`actionId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '接口管理' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of wish_app_action
-- ----------------------------
INSERT INTO `wish_app_action` VALUES ('06532df903114f699387c217ad081459', '多数据源管理下载', 'Function', 101205, 'org:dataSource:download', 'e078f6cfb5a64fc6904d18420438379e', 'jiubanqingchen@2020', '2021-07-08 02:53:52', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('06a12f5ef11f40f295341d72f364151b', '部门管理编辑', 'Function', 100203, 'org:dept:edit', 'bd2ac7059e0747688bd5824fbb300478', 'jiubanqingchen@2020', '2021-04-28 22:04:11', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('0778dde0fcb943dfa454305c23d56fe4', '接口管理', 'Controller', 1009, 'org:appAction', '0', 'jiubanqingchen@2020', '2021-04-28 22:11:07', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('08fc4af6db674b32bd986b28f6f4d9fd', '部门管理删除', 'Function', 100204, 'org:dept:delete', 'bd2ac7059e0747688bd5824fbb300478', 'jiubanqingchen@2020', '2021-04-28 22:04:11', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('0a008bacf58a4110b6e946552ba3ed14', '多数据源管理新增', 'Function', 101202, 'org:dataSource:add', 'e078f6cfb5a64fc6904d18420438379e', 'jiubanqingchen@2020', '2021-07-08 02:53:52', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('0a3bcd49ed664423ac7f966c545ea747', '用户部门管理新增', 'Function', 100402, 'org:userDept:add', 'e2cba158d6424765b886e29123183386', 'jiubanqingchen@2020', '2021-04-28 22:07:40', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('0f987d2175f04a96acb05e0c317a57ed', '角色用户组管理编辑', 'Function', 100403, 'org:userGroupRole:edit', 'bcdb01d804874974b89d895a23540328', 'jiubanqingchen@2020', '2021-04-28 22:06:16', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('12ed5cebbc2147e98dee76a5a5cae4fc', '数据字典管理', 'Controller', 1008, 'org:dictionary', '0', 'jiubanqingchen@2020', '2021-04-28 22:10:14', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('138361d0bc7044c3956fe091f3a9128b', '用户部门管理删除', 'Function', 100404, 'org:userDept:delete', 'e2cba158d6424765b886e29123183386', 'jiubanqingchen@2020', '2021-04-28 22:07:40', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('1b79c5a50c904de189fb1bb6262ecc18', '接口管理查看', 'Function', 100901, 'org:appAction:view', '0778dde0fcb943dfa454305c23d56fe4', 'jiubanqingchen@2020', '2021-04-28 22:11:10', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('20f410e71d2344db928b089af9809f5a', '数据字典类型管理编辑', 'Function', 100703, 'org:dictionaryType:edit', 'f33eca367cab417eb8ecf72c88495324', 'jiubanqingchen@2020', '2021-04-28 22:10:18', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('2d3eb0a4d5964e3683b8108549c1ff9b', '角色管理新增', 'Function', 100202, 'org:role:add', 'a2793ab8a44447eb9980fbf0a0bdcee3', 'jiubanqingchen@2020', '2021-04-27 03:26:08', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('2d77826b327944cd808db15b52ac8446', '用户部门管理上传', 'Function', 100406, 'org:userDept:upload', 'e2cba158d6424765b886e29123183386', 'jiubanqingchen@2020', '2021-04-28 22:07:40', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('35f19436148a4b5dbbcee46a5d9c2f17', '部门管理查看', 'Function', 100201, 'org:dept:view', 'bd2ac7059e0747688bd5824fbb300478', 'jiubanqingchen@2020', '2021-04-28 22:04:11', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('39f540ab47e94a1392bb89621997f424', '多数据源管理编辑', 'Function', 101203, 'org:dataSource:edit', 'e078f6cfb5a64fc6904d18420438379e', 'jiubanqingchen@2020', '2021-07-08 02:53:52', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('3ae4c4cf176f40cdad4e6d2846a4c40c', '角色管理编辑', 'Function', 100203, 'org:role:edit', 'a2793ab8a44447eb9980fbf0a0bdcee3', 'jiubanqingchen@2020', '2021-04-27 03:26:08', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('40745a8383e64b4ca04b61120f1231b1', '接口管理下载', 'Function', 100905, 'org:appAction:download', '0778dde0fcb943dfa454305c23d56fe4', 'jiubanqingchen@2020', '2021-04-28 22:11:10', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('41346cff23b44a769565d28b2a18f7e4', '多数据源管理上传', 'Function', 101206, 'org:dataSource:upload', 'e078f6cfb5a64fc6904d18420438379e', 'jiubanqingchen@2020', '2021-07-08 02:53:52', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('45362da3c7d34bfab0321d404ab54be5', '数据字典类型管理下载', 'Function', 100705, 'org:dictionaryType:download', 'f33eca367cab417eb8ecf72c88495324', 'jiubanqingchen@2020', '2021-04-28 22:10:18', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('4ab8506b8d7d44a595bf3fa1d4bfa9ee', '登录日志删除', 'Function', 101104, 'org:loginLog:delete', '642b4bdbcf004c96bd778327f9831e4e', 'jiubanqingchen@2020', '2021-06-25 04:16:21', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('4b0fffab36ae40bf97c3f64b46f24558', '定时任务查看', 'Function', 101001, 'org:cronTask:view', 'a70721bc4ef04ba3a5b55455aecfea08', 'jiubanqingchen@2020', '2021-05-06 22:54:30', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('4e8cdcde56c64996a62a67c04efa87e9', '角色用户组管理删除', 'Function', 100404, 'org:userGroupRole:delete', 'bcdb01d804874974b89d895a23540328', 'jiubanqingchen@2020', '2021-04-28 22:06:16', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('5501985060e54dacaeeb6f4ec1a4f67d', '数据字典管理上传', 'Function', 100806, 'org:dictionary:upload', '12ed5cebbc2147e98dee76a5a5cae4fc', 'jiubanqingchen@2020', '2021-04-28 22:10:22', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('575f3feb6c644c049d320540f7d7753a', '多数据源管理删除', 'Function', 101204, 'org:dataSource:delete', 'e078f6cfb5a64fc6904d18420438379e', 'jiubanqingchen@2020', '2021-07-08 02:53:52', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('5992bedb31ad4f2383c10bb6aabbd77f', '资源权限管理上传', 'Function', 100606, 'org:resourceAuthority:upload', 'f4b9f9e3a2874a0e90cbe1b7679d1f52', 'jiubanqingchen@2020', '2021-04-28 22:07:49', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('5f3a343ad76e40109aaba20d87e23dff', '资源权限管理新增', 'Function', 100602, 'org:resourceAuthority:add', 'f4b9f9e3a2874a0e90cbe1b7679d1f52', 'jiubanqingchen@2020', '2021-04-28 22:07:49', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('642b4bdbcf004c96bd778327f9831e4e', '登录日志', 'Controller', 1011, 'org:loginLog', '0', 'jiubanqingchen@2020', '2021-06-25 04:16:16', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('6997931b9c28472d9a58a26c97707e41', '数据字典管理编辑', 'Function', 100803, 'org:dictionary:edit', '12ed5cebbc2147e98dee76a5a5cae4fc', 'jiubanqingchen@2020', '2021-04-28 22:10:22', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('6cb0f73b409c4ef7ac5dce9793361e07', '数据字典类型管理新增', 'Function', 100702, 'org:dictionaryType:add', 'f33eca367cab417eb8ecf72c88495324', 'jiubanqingchen@2020', '2021-04-28 22:10:18', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('6f5bcabe2c5540ccbd9712f835245115', '用户管理新增', 'Function', 100102, 'org:user:add', '7806e6ee9d064fe58c2f75a2cfe24f36', 'jiubanqingchen@2020', '2021-04-27 02:45:24', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('71b7946aef2b4488b6bd73558fd8b46c', '今夕何夕博客', 'Controller', 1013, 'external:jxhxBlog', '0', 'jiubanqingchen@2020', '2021-07-08 03:18:34', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('77dc4349c06e4a4e926290b2252716d2', '角色用户组管理上传', 'Function', 100406, 'org:userGroupRole:upload', 'bcdb01d804874974b89d895a23540328', 'jiubanqingchen@2020', '2021-04-28 22:06:16', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('7806e6ee9d064fe58c2f75a2cfe24f36', '用户管理', 'Controller', 1001, 'org:user', '0', 'jiubanqingchen@2020', '2021-04-25 20:18:49', 'jiubanqingchen@2020', '2021-04-27 04:22:19');
INSERT INTO `wish_app_action` VALUES ('7834e8fd2fed4de6895f4015df03e45e', '数据字典管理查看', 'Function', 100801, 'org:dictionary:view', '12ed5cebbc2147e98dee76a5a5cae4fc', 'jiubanqingchen@2020', '2021-04-28 22:10:22', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('78572dd835604cdd9eb4efbf562dd804', '接口管理新增', 'Function', 100902, 'org:appAction:add', '0778dde0fcb943dfa454305c23d56fe4', 'jiubanqingchen@2020', '2021-04-28 22:11:10', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('797f2f8db7a44e40895dfa43c8ee3ace', '用户管理删除', 'Function', 100104, 'org:user:delete', '7806e6ee9d064fe58c2f75a2cfe24f36', 'jiubanqingchen@2020', '2021-04-27 02:45:24', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('7ad7f1e10c1543a9a98f8ae105410a8f', '角色用户组管理新增', 'Function', 100402, 'org:userGroupRole:add', 'bcdb01d804874974b89d895a23540328', 'jiubanqingchen@2020', '2021-04-28 22:06:16', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('7b4505c829d147f5b8f5a95192054f23', '角色管理删除', 'Function', 100204, 'org:role:delete', 'a2793ab8a44447eb9980fbf0a0bdcee3', 'jiubanqingchen@2020', '2021-04-27 03:26:08', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('7dbd44ebe6eb47b6a8e107195032dcb4', '数据字典类型管理查看', 'Function', 100701, 'org:dictionaryType:view', 'f33eca367cab417eb8ecf72c88495324', 'jiubanqingchen@2020', '2021-04-28 22:10:18', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('7f2ec445d1d1487593b0ea1d9f4dcb29', '资源权限管理下载', 'Function', 100605, 'org:resourceAuthority:download', 'f4b9f9e3a2874a0e90cbe1b7679d1f52', 'jiubanqingchen@2020', '2021-04-28 22:07:49', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('810f55bf1582478c949c63f30795fb6e', '定时任务下载', 'Function', 101005, 'org:cronTask:download', 'a70721bc4ef04ba3a5b55455aecfea08', 'jiubanqingchen@2020', '2021-05-06 22:54:31', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('81dcb6279b3e48a89350ee8bdc241e1f', '数据字典管理下载', 'Function', 100805, 'org:dictionary:download', '12ed5cebbc2147e98dee76a5a5cae4fc', 'jiubanqingchen@2020', '2021-04-28 22:10:22', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('89f25774306942ac97ecec6ef298fa6b', '资源权限管理查看', 'Function', 100601, 'org:resourceAuthority:view', 'f4b9f9e3a2874a0e90cbe1b7679d1f52', 'jiubanqingchen@2020', '2021-04-28 22:07:49', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('8a98927341314b97ae0669ca84fce977', '多数据源管理查看', 'Function', 101201, 'org:dataSource:view', 'e078f6cfb5a64fc6904d18420438379e', 'jiubanqingchen@2020', '2021-07-08 02:53:52', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('8aab45d641a54604a393dfe1005991bc', '用户管理上传', 'Function', 100106, 'org:user:upload', '7806e6ee9d064fe58c2f75a2cfe24f36', 'jiubanqingchen@2020', '2021-04-27 02:45:24', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('8cca0845375e4ffab75c0210c1083234', '定时任务新增', 'Function', 101002, 'org:cronTask:add', 'a70721bc4ef04ba3a5b55455aecfea08', 'jiubanqingchen@2020', '2021-05-06 22:54:30', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('8e7fac772c4a42098a18c1701cdc6590', '用户管理下载', 'Function', 100105, 'org:user:download', '7806e6ee9d064fe58c2f75a2cfe24f36', 'jiubanqingchen@2020', '2021-04-27 02:45:24', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('8e83532fc6d04c7b954cdda34fd7a022', '角色管理下载', 'Function', 100205, 'org:role:download', 'a2793ab8a44447eb9980fbf0a0bdcee3', 'jiubanqingchen@2020', '2021-04-27 03:26:08', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('90cb9f3c7b7c4796b7e07496a5a5598b', '资源权限管理编辑', 'Function', 100603, 'org:resourceAuthority:edit', 'f4b9f9e3a2874a0e90cbe1b7679d1f52', 'jiubanqingchen@2020', '2021-04-28 22:07:49', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('935e8d57e97b468098159d9f4dd42b71', '登录日志查看', 'Function', 101101, 'org:loginLog:view', '642b4bdbcf004c96bd778327f9831e4e', 'jiubanqingchen@2020', '2021-06-25 04:16:21', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('943073c6900e41b8b5f7bba5197c4827', '用户管理编辑', 'Function', 100103, 'org:user:edit', '7806e6ee9d064fe58c2f75a2cfe24f36', 'jiubanqingchen@2020', '2021-04-27 02:45:24', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('9e8afd61092f417fa832daee11bace3a', '角色用户组管理下载', 'Function', 100405, 'org:userGroupRole:download', 'bcdb01d804874974b89d895a23540328', 'jiubanqingchen@2020', '2021-04-28 22:06:16', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('a2793ab8a44447eb9980fbf0a0bdcee3', '角色管理', 'Controller', 1003, 'org:role', '0', 'jiubanqingchen@2020', '2021-04-27 03:26:03', 'jiubanqingchen@2020', '2021-04-28 22:04:03');
INSERT INTO `wish_app_action` VALUES ('a6eea37b72344b928c42ce62a015d86b', '角色管理上传', 'Function', 100206, 'org:role:upload', 'a2793ab8a44447eb9980fbf0a0bdcee3', 'jiubanqingchen@2020', '2021-04-27 03:26:08', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('a70721bc4ef04ba3a5b55455aecfea08', '定时任务', 'Controller', 1010, 'org:cronTask', '0', 'jiubanqingchen@2020', '2021-05-06 22:54:26', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('a928ff7d42404e71a8456c56fd51a9a8', '用户部门管理编辑', 'Function', 100403, 'org:userDept:edit', 'e2cba158d6424765b886e29123183386', 'jiubanqingchen@2020', '2021-04-28 22:07:40', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('a9e9f4c6497b45ebad083577ed1b1b79', '角色用户组管理查看', 'Function', 100401, 'org:userGroupRole:view', 'bcdb01d804874974b89d895a23540328', 'jiubanqingchen@2020', '2021-04-28 22:06:16', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('aa49a7b5ef4d46dd9e25b0962129082e', '用户部门管理查看', 'Function', 100401, 'org:userDept:view', 'e2cba158d6424765b886e29123183386', 'jiubanqingchen@2020', '2021-04-28 22:07:40', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('ad833b153f084add834d3e7e007b7996', '数据字典类型管理上传', 'Function', 100706, 'org:dictionaryType:upload', 'f33eca367cab417eb8ecf72c88495324', 'jiubanqingchen@2020', '2021-04-28 22:10:19', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('b6af52e4ed4945dea16998f0e51b95c0', '部门管理下载', 'Function', 100205, 'org:dept:download', 'bd2ac7059e0747688bd5824fbb300478', 'jiubanqingchen@2020', '2021-04-28 22:04:11', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('bcdb01d804874974b89d895a23540328', '角色用户组管理', 'Controller', 1005, 'org:userGroupRole', '0', 'jiubanqingchen@2020', '2021-04-28 22:06:12', 'jiubanqingchen@2020', '2021-04-28 22:07:36');
INSERT INTO `wish_app_action` VALUES ('bd2ac7059e0747688bd5824fbb300478', '部门管理', 'Controller', 1002, 'org:dept', '0', 'jiubanqingchen@2020', '2021-04-28 22:03:53', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('be9446d734c84d9884c7eebd4427733f', '登录日志编辑', 'Function', 101103, 'org:loginLog:edit', '642b4bdbcf004c96bd778327f9831e4e', 'jiubanqingchen@2020', '2021-06-25 04:16:21', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('bed3a070ca3d40e790a9eb171d506e13', '登录日志上传', 'Function', 101106, 'org:loginLog:upload', '642b4bdbcf004c96bd778327f9831e4e', 'jiubanqingchen@2020', '2021-06-25 04:16:21', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('c2a355de5f96407aa27c3ece3891db53', '定时任务编辑', 'Function', 101003, 'org:cronTask:edit', 'a70721bc4ef04ba3a5b55455aecfea08', 'jiubanqingchen@2020', '2021-05-06 22:54:30', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('c3efe551805b4dc9b265c02d73c5b4f5', '用户管理查看', 'Function', 100101, 'org:user:view', '7806e6ee9d064fe58c2f75a2cfe24f36', 'jiubanqingchen@2020', '2021-04-27 02:45:24', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('c53ddcbf655c46be8ae0f8aced495444', '接口管理上传', 'Function', 100906, 'org:appAction:upload', '0778dde0fcb943dfa454305c23d56fe4', 'jiubanqingchen@2020', '2021-04-28 22:11:10', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('ca360262267a47f0a886dffce9ed9e6a', '部门管理新增', 'Function', 100202, 'org:dept:add', 'bd2ac7059e0747688bd5824fbb300478', 'jiubanqingchen@2020', '2021-04-28 22:04:11', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('cde32932ef144479a437529b05963fe5', '资源权限管理删除', 'Function', 100604, 'org:resourceAuthority:delete', 'f4b9f9e3a2874a0e90cbe1b7679d1f52', 'jiubanqingchen@2020', '2021-04-28 22:07:49', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('d2b657bd09b34bb786ef7a32ad3d7370', '定时任务上传', 'Function', 101006, 'org:cronTask:upload', 'a70721bc4ef04ba3a5b55455aecfea08', 'jiubanqingchen@2020', '2021-05-06 22:54:31', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('d3329bc3db3f4ac0892a7b26531bfdbc', '部门管理上传', 'Function', 100206, 'org:dept:upload', 'bd2ac7059e0747688bd5824fbb300478', 'jiubanqingchen@2020', '2021-04-28 22:04:11', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('d53927d31b7b456a81fd7ff3e8f1c7d8', '数据字典类型管理删除', 'Function', 100704, 'org:dictionaryType:delete', 'f33eca367cab417eb8ecf72c88495324', 'jiubanqingchen@2020', '2021-04-28 22:10:18', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('d76a1f1428d34b6ba168f4f474616f69', '登录日志下载', 'Function', 101105, 'org:loginLog:download', '642b4bdbcf004c96bd778327f9831e4e', 'jiubanqingchen@2020', '2021-06-25 04:16:21', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('d7da85594ad44d12bbfc212b3cfabd9f', '今夕何夕博客查看', 'Function', 1, 'external:jxhxBlog:view', '71b7946aef2b4488b6bd73558fd8b46c', 'jiubanqingchen@2020', '2021-07-08 03:19:03', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('d8e49cedf2ca43e18a4a16bf6e1af28e', '定时任务删除', 'Function', 101004, 'org:cronTask:delete', 'a70721bc4ef04ba3a5b55455aecfea08', 'jiubanqingchen@2020', '2021-05-06 22:54:30', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('e078f6cfb5a64fc6904d18420438379e', '多数据源管理', 'Controller', 1012, 'org:dataSource', '0', 'jiubanqingchen@2020', '2021-07-08 02:53:47', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('e2cba158d6424765b886e29123183386', '用户部门管理', 'Controller', 1004, 'org:userDept', '0', 'jiubanqingchen@2020', '2021-04-28 22:07:31', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('e398542886de4336919e9f7b4157f523', '用户部门管理下载', 'Function', 100405, 'org:userDept:download', 'e2cba158d6424765b886e29123183386', 'jiubanqingchen@2020', '2021-04-28 22:07:40', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('e5dcdfdcd1d24dd4a2f6728a24423344', '登录日志新增', 'Function', 101102, 'org:loginLog:add', '642b4bdbcf004c96bd778327f9831e4e', 'jiubanqingchen@2020', '2021-06-25 04:16:21', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('ef38f5834a664a1c900934ee41c1b90a', '接口管理删除', 'Function', 100904, 'org:appAction:delete', '0778dde0fcb943dfa454305c23d56fe4', 'jiubanqingchen@2020', '2021-04-28 22:11:10', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('f2a55255154e41e1aa9fe12c799fde0d', '角色管理查看', 'Function', 100201, 'org:role:view', 'a2793ab8a44447eb9980fbf0a0bdcee3', 'jiubanqingchen@2020', '2021-04-27 03:26:08', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('f33eca367cab417eb8ecf72c88495324', '数据字典类型管理', 'Controller', 1007, 'org:dictionaryType', '0', 'jiubanqingchen@2020', '2021-04-28 22:09:51', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('f4b9f9e3a2874a0e90cbe1b7679d1f52', '资源权限管理', 'Controller', 1006, 'org:resourceAuthority', '0', 'jiubanqingchen@2020', '2021-04-28 22:06:50', 'jiubanqingchen@2020', '2021-04-28 22:07:48');
INSERT INTO `wish_app_action` VALUES ('fc735d17ef85434cbb65b31500f2dbda', '数据字典管理新增', 'Function', 100802, 'org:dictionary:add', '12ed5cebbc2147e98dee76a5a5cae4fc', 'jiubanqingchen@2020', '2021-04-28 22:10:22', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('fd336584126e4c1b8053316e4549b7a7', '数据字典管理删除', 'Function', 100804, 'org:dictionary:delete', '12ed5cebbc2147e98dee76a5a5cae4fc', 'jiubanqingchen@2020', '2021-04-28 22:10:22', NULL, NULL);
INSERT INTO `wish_app_action` VALUES ('ff4227a05d84411ab0527ed8858a1075', '接口管理编辑', 'Function', 100903, 'org:appAction:edit', '0778dde0fcb943dfa454305c23d56fe4', 'jiubanqingchen@2020', '2021-04-28 22:11:10', NULL, NULL);

-- ----------------------------
-- Table structure for wish_attachment
-- ----------------------------
DROP TABLE IF EXISTS `wish_attachment`;
CREATE TABLE `wish_attachment`  (
  `fileId` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '文件id',
  `fileName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件名称',
  `mimeType` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件mime',
  `fileExt` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件后缀名',
  `filePath` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件路径',
  `busiId` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '关联id',
  `itemType` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '业务类型',
  `size` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '附件大小',
  `createPerson` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `modifyPerson` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `modifyTime` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`fileId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '附件管理' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of wish_attachment
-- ----------------------------

-- ----------------------------
-- Table structure for wish_cron_task
-- ----------------------------
DROP TABLE IF EXISTS `wish_cron_task`;
CREATE TABLE `wish_cron_task`  (
  `cronTaskId` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '任务id',
  `cronTaskNo` int NOT NULL COMMENT '任务编号',
  `cronTaskName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '任务名称',
  `description` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `cronExpression` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '任务表达式',
  `className` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '执行类',
  `active` tinyint(1) NULL DEFAULT NULL COMMENT '是否激活',
  `createPerson` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `modifyPerson` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `modifyTime` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`cronTaskId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '定时任务' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of wish_cron_task
-- ----------------------------

-- ----------------------------
-- Table structure for wish_data_source
-- ----------------------------
DROP TABLE IF EXISTS `wish_data_source`;
CREATE TABLE `wish_data_source`  (
  `dataSourceId` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '数据源id',
  `dataSourceCode` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '数据源编码',
  `dataSourceName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '数据源名称',
  `dataSourceDesc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '数据源描述',
  `dataSourceType` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '数据源类型',
  `userName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户账号',
  `userPassword` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户密码',
  `connUrl` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '连接字符串',
  `status` tinyint(1) NULL DEFAULT NULL COMMENT '状态是否生效',
  `sqlTemplatePath` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模板文件地址',
  `createPerson` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `modifyPerson` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `modifyTime` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`dataSourceId`) USING BTREE,
  UNIQUE INDEX `datasourceCode`(`dataSourceCode`) USING BTREE COMMENT '数据源编码不能重复'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '多数据源管理' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wish_data_source
-- ----------------------------

-- ----------------------------
-- Table structure for wish_demo
-- ----------------------------
DROP TABLE IF EXISTS `wish_demo`;
CREATE TABLE `wish_demo`  (
  `demoId` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `demoName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '演示名称',
  `demoDesc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '演示描述',
  `demoDate` datetime(0) NULL DEFAULT NULL COMMENT '演示时间',
  `demoPerson` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '演示人',
  `createPerson` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `modifyPerson` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `modifyTime` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`demoId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '演示' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of wish_demo
-- ----------------------------

-- ----------------------------
-- Table structure for wish_dept
-- ----------------------------
DROP TABLE IF EXISTS `wish_dept`;
CREATE TABLE `wish_dept`  (
  `deptId` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '部门id',
  `deptCode` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '部门编码',
  `deptName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '部门名称',
  `deptFullName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '部门全称',
  `deptType` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门类型',
  `parentId` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父部门id',
  `sortNo` int NULL DEFAULT NULL COMMENT '排序',
  `valid` tinyint(1) NULL DEFAULT NULL COMMENT '是否有效',
  `treePath` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '树路径',
  `enabledTime` datetime(0) NULL DEFAULT NULL COMMENT '启用时间',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `createPerson` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `modifyTime` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `modifyPerson` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`deptId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '部门表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of wish_dept
-- ----------------------------
INSERT INTO `wish_dept` VALUES ('dept:child1', 'child:2021', '生计部', '生计部', 'main', 'qc', 123, 1, 'qc.dept:child1', '2021-01-04 16:00:16', NULL, NULL, '2021-04-16 09:00:11', 'jiubanqingchen@2020');
INSERT INTO `wish_dept` VALUES ('qc', 'qc', '轻尘科技', '轻尘科技有限公司', NULL, '0', 1, 1, 'qc', '2030-11-29 17:18:55', NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for wish_dictionary
-- ----------------------------
DROP TABLE IF EXISTS `wish_dictionary`;
CREATE TABLE `wish_dictionary`  (
  `dictionaryId` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典主键',
  `codeItemId` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '类型ID',
  `codeId` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字典ID',
  `codeName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `sortNo` int NULL DEFAULT NULL COMMENT '排序',
  `isUpdate` varchar(4) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否维护',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `createPerson` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `modifyTime` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `modifyPerson` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`dictionaryId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '数据字典' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of wish_dictionary
-- ----------------------------
INSERT INTO `wish_dictionary` VALUES ('0ac1389706fc4e4ba28dc97ebf8e936c', 'ORG_DATASOURCE_TYPE', 'ORACLE', 'ORACLE', 2, NULL, '2021-06-30 02:11:44', 'jiubanqingchen@2020', NULL, NULL);
INSERT INTO `wish_dictionary` VALUES ('23695a18a5ae449dbb0bb54626c7ef67', 'CREATE_SHOW_FORM_TYPE', 'input', '输入框', 1, NULL, '2019-12-25 21:38:29', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', NULL, NULL);
INSERT INTO `wish_dictionary` VALUES ('26f13727546c43779e5ec276fae96d42', 'ORG_ROLE_TYPE', 'MANAGER_ROLE', '管理角色', 1, NULL, '2021-04-16 09:03:54', 'jiubanqingchen@2020', NULL, NULL);
INSERT INTO `wish_dictionary` VALUES ('2ec7222ff4284c9b9c6a4d6eb3abc675', 'CREATE_QUERY_TYPE', 'jqcx', '精确查询', 2, NULL, '2019-12-25 21:36:50', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', NULL, NULL);
INSERT INTO `wish_dictionary` VALUES ('2f9d140729d34f1b9476b40cc359c325', 'CREATE_QUERY_TYPE', 'like', '模糊查询', 1, NULL, '2019-12-25 21:36:36', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', NULL, NULL);
INSERT INTO `wish_dictionary` VALUES ('32c4d234ee5c4705ab0a8ac33cabcb8b', 'CREATE_SHOW_FORM_TYPE', 'radio', '单选框(true/false)', 3, NULL, '2019-12-25 21:39:09', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2019-12-25 21:39:24', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2');
INSERT INTO `wish_dictionary` VALUES ('355a91da663f4e4fa72387eb9e245f71', 'CREATE_SHOW_FORM_TYPE', 'date', '日期框', 2, NULL, '2019-12-25 21:38:50', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', NULL, NULL);
INSERT INTO `wish_dictionary` VALUES ('569669832e9243adab0935a014aa8f2e', 'ORG_RESOURCE_TYPE', 'INTERFACE', '接口', 2, NULL, '2021-04-14 06:49:48', 'jiubanqingchen@2020', NULL, NULL);
INSERT INTO `wish_dictionary` VALUES ('5f96030a11b949409e3d3adc3418baa5', 'ORG_DATASOURCE_TYPE', 'SQLSERVER', 'SQLSERVER', 3, NULL, '2021-06-30 02:12:07', 'jiubanqingchen@2020', NULL, NULL);
INSERT INTO `wish_dictionary` VALUES ('66656ab051644059942a50b39b22e861', 'ORG_APP_ACTION_TYPE', 'Controller', '类', 1, NULL, '2021-04-26 02:39:04', 'jiubanqingchen@2020', NULL, NULL);
INSERT INTO `wish_dictionary` VALUES ('7010de099dcd4cf2a438438dddc2273c', 'ORG_DEPT_TYPE', 'main', '主要部门', 1, NULL, '2019-12-25 21:50:25', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', NULL, NULL);
INSERT INTO `wish_dictionary` VALUES ('72917cd083e74bcd89b47e8b7e05e077', 'CREATE_SHOW_FORM_TYPE', 'dictionarySelect', '下拉框(数据字典)', 4, NULL, '2019-12-25 21:40:02', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', NULL, NULL);
INSERT INTO `wish_dictionary` VALUES ('74e88d897c6a4831aa6e217b4771d034', 'ORG_APP_ACTION_TYPE', 'Function', '方法', 2, NULL, '2021-04-26 02:39:19', 'jiubanqingchen@2020', NULL, NULL);
INSERT INTO `wish_dictionary` VALUES ('7b2be6336799469ab78949f012f6f9ce', 'ORG_MENU_TYPE', 'BUTTON', '资源', 2, NULL, '2020-01-02 19:25:22', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-05-08 11:08:30', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2');
INSERT INTO `wish_dictionary` VALUES ('8637c9a81a2943aa9db7899451386adf', 'TEST_TYPE', 'TEST_TYPE_ONE', '一', 1, NULL, '2021-06-18 04:57:17', 'jiubanqingchen@2020', '2021-06-18 04:57:36', 'jiubanqingchen@2020');
INSERT INTO `wish_dictionary` VALUES ('8817dea570e34c7b98a7fbb0f6a72ea8', 'ORG_USER_SEX', 'WOMAN', '女', 221, NULL, '2020-05-11 17:54:49', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2021-04-14 03:44:24', 'jiubanqingchen@2020');
INSERT INTO `wish_dictionary` VALUES ('aaa24085b6cc47caa12a08977db11ded', 'ORG_RESOURCE_TYPE', 'MENU', '菜单', 1, NULL, '2021-04-14 06:49:03', 'jiubanqingchen@2020', NULL, NULL);
INSERT INTO `wish_dictionary` VALUES ('acd79feb93c342baa020ffea6534e291', 'ORG_DATASOURCE_TYPE', 'MYSQL', 'MYSQL', 1, NULL, '2021-06-30 02:11:18', 'jiubanqingchen@2020', NULL, NULL);
INSERT INTO `wish_dictionary` VALUES ('b2aefb58cd8946c6bef18031f3f86e66', 'ORG_USER_SEX', 'MAN', '男', 1, NULL, '2020-05-11 17:54:31', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', NULL, NULL);
INSERT INTO `wish_dictionary` VALUES ('def0211d8dbb4bc58afcc8a19b779a17', 'ORG_USER_ROLE_GROUP_TYPE', 'USER', '用户', 1, NULL, '2020-01-03 09:27:28', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', NULL, NULL);
INSERT INTO `wish_dictionary` VALUES ('df6b3fe9798f4190b6a8cf179e6c26cd', 'ORG_USER_ROLE_GROUP_TYPE', 'DEPT', '部门', 2, NULL, '2020-01-03 09:27:42', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', NULL, NULL);
INSERT INTO `wish_dictionary` VALUES ('e16e41bbebd647cf90f0da7427c42422', 'CREATE_QUERY_TYPE', 'dictionarySelect', '数据字典查询', 3, NULL, '2019-12-25 21:37:14', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', NULL, NULL);
INSERT INTO `wish_dictionary` VALUES ('e3f6cc4105674510b034b22032a9c4de', 'ORG_DEPT_TYPE', 'special', '特殊部门', 2, NULL, '2019-12-25 21:50:56', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', NULL, NULL);
INSERT INTO `wish_dictionary` VALUES ('f4811b540327449dba816fbf62e40968', 'ORG_MENU_TYPE', 'MENU', '菜单', 1, NULL, '2020-01-02 19:25:07', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', NULL, NULL);
INSERT INTO `wish_dictionary` VALUES ('f979ae47652649e4b580a70a8caeac2c', 'CREATE_SHOW_FORM_TYPE', 'textarea', '文本框', 5, NULL, '2019-12-25 21:40:21', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', NULL, NULL);
INSERT INTO `wish_dictionary` VALUES ('fbdf8d678c9a4676990b13f928807267', 'ORG_ROLE_TYPE', 'BUSINESS_ROLE', '业务角色', 2, NULL, '2021-04-16 09:04:03', 'jiubanqingchen@2020', NULL, NULL);

-- ----------------------------
-- Table structure for wish_dictionary_type
-- ----------------------------
DROP TABLE IF EXISTS `wish_dictionary_type`;
CREATE TABLE `wish_dictionary_type`  (
  `dictionaryTypeId` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '数据字典类型id',
  `codeItemId` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典类别标识',
  `codeItemName` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典类别名称',
  `parentId` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '数据字典类型父id',
  `sortNo` int NULL DEFAULT NULL COMMENT '类型排序',
  `isUpdate` tinyint(1) NULL DEFAULT NULL COMMENT '能否被更新',
  `createPerson` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `modifyPerson` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `modifyTime` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`dictionaryTypeId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典类型' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of wish_dictionary_type
-- ----------------------------
INSERT INTO `wish_dictionary_type` VALUES ('3aff9350685f4d418c04a04c4366fa43', 'ORG_ROLE_TYPE', '角色类型', NULL, 106, NULL, 'jiubanqingchen@2020', '2021-04-16 09:03:31', NULL, NULL);
INSERT INTO `wish_dictionary_type` VALUES ('52f7f3bc4cd947259ff790a8a3135f88', 'ORG_USER_SEX', '用户性别', NULL, 100, 1, 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-05-11 17:54:13', 'jiubanqingchen@2020', '2021-04-14 06:51:09');
INSERT INTO `wish_dictionary_type` VALUES ('70e046c8d718428590fd71449094ae9f', 'CREATE_SHOW_FORM_TYPE', '表单展现方式', NULL, 201, 1, 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2019-12-25 21:38:10', 'jiubanqingchen@2020', '2021-04-16 08:57:25');
INSERT INTO `wish_dictionary_type` VALUES ('8eb5c187280548a3a626255489ee7904', 'ORG_DATASOURCE_TYPE', '数据源类型', NULL, 301, NULL, 'jiubanqingchen@2020', '2021-06-29 22:42:56', NULL, NULL);
INSERT INTO `wish_dictionary_type` VALUES ('913ff70d37d647fca165113b3f960556', 'TEST_TYPE', '测试类型', NULL, 100101, NULL, 'jiubanqingchen@2020', '2021-06-18 04:56:45', 'jiubanqingchen@2020', '2021-06-18 04:56:52');
INSERT INTO `wish_dictionary_type` VALUES ('92ec02aa349642478ea7f87104d65935', 'ORG_USER_ROLE_GROUP_TYPE', '角色用户关联类型', NULL, 102, 1, 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-01-03 09:27:02', 'jiubanqingchen@2020', '2021-04-14 06:50:23');
INSERT INTO `wish_dictionary_type` VALUES ('c274893480c743ddbe3668bb852c8217', 'ORG_RESOURCE_TYPE', '资源类型', NULL, 101, NULL, 'jiubanqingchen@2020', '2021-04-14 06:48:22', NULL, NULL);
INSERT INTO `wish_dictionary_type` VALUES ('dc110e626668430a90aeea8afb10be00', 'ORG_MENU_TYPE', '菜单类型', NULL, 104, 1, 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-01-02 19:24:52', 'jiubanqingchen@2020', '2021-04-14 06:50:40');
INSERT INTO `wish_dictionary_type` VALUES ('e5067af6afa841519bdc261d69126d77', 'ORG_APP_ACTION_TYPE', '接口类型', NULL, 107, NULL, 'jiubanqingchen@2020', '2021-04-26 02:38:26', NULL, NULL);
INSERT INTO `wish_dictionary_type` VALUES ('e928fba1c7f94b08a77a65bff2d7cd47', 'ORG_DEPT_TYPE', '部门类型', NULL, 105, 1, 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2019-12-25 21:50:10', 'jiubanqingchen@2020', '2021-04-16 08:57:57');
INSERT INTO `wish_dictionary_type` VALUES ('ee24335ccc694e94839158c19094fc47', 'CREATE_QUERY_TYPE', '查询方式', NULL, 202, 1, 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2019-12-25 21:35:47', 'jiubanqingchen@2020', '2021-04-16 08:57:33');

-- ----------------------------
-- Table structure for wish_login_log
-- ----------------------------
DROP TABLE IF EXISTS `wish_login_log`;
CREATE TABLE `wish_login_log`  (
  `loginLogId` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '登录日志id',
  `loginUserName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '登录名称',
  `loginIp` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '登录ip',
  `loginArea` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '登录地址',
  `browser` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '浏览器',
  `os` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作系统',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '登录日期',
  `modifyPerson` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  PRIMARY KEY (`loginLogId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户登录日志' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of wish_login_log
-- ----------------------------

-- ----------------------------
-- Table structure for wish_menu
-- ----------------------------
DROP TABLE IF EXISTS `wish_menu`;
CREATE TABLE `wish_menu`  (
  `menuId` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单id',
  `menuName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单name',
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单title',
  `keepAlive` tinyint(1) NULL DEFAULT NULL COMMENT '是否缓存',
  `path` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'path',
  `component` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组件地址',
  `redirect` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '跳转地址',
  `hidden` tinyint(1) NULL DEFAULT NULL COMMENT '是否显示',
  `menuType` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单类型',
  `authorityKey` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限标识',
  `sortNo` int NULL DEFAULT NULL COMMENT '菜单排序',
  `icon` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单图标',
  `parentId` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父菜单id',
  `createPerson` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `modifyPerson` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `modifyTime` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`menuId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '菜单表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of wish_menu
-- ----------------------------
INSERT INTO `wish_menu` VALUES ('111', '11', '41341', 1, '141', '111', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for wish_resource_authority
-- ----------------------------
DROP TABLE IF EXISTS `wish_resource_authority`;
CREATE TABLE `wish_resource_authority`  (
  `resourceAuthorityId` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '资源权限id',
  `roleId` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色id',
  `resourceId` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '资源id',
  `resourceCategory` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '资源分类(区分可分配与可使用)',
  `resourceType` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '资源类型(区分是否为菜单还是资源)',
  `createPerson` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建人',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `modifyPerson` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `modifyTime` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`resourceAuthorityId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色资源关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of wish_resource_authority
-- ----------------------------
INSERT INTO `wish_resource_authority` VALUES ('01155307628e435eaad5c6af308a02ea', '27b15135642f4153847cb90823e4798f', 'error:01', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:54:32', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('021513c20ab74410b922ad4f9e154eec', '6d1d3299266e44b997a948fc94d9c890', '20f410e71d2344db928b089af9809f5a', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:08', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('03dffbb989474f4e92732ddf718943b7', '27b15135642f4153847cb90823e4798f', 'org:00:03', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:54:32', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('0462ad21977f4567b046d2807074dc4c', '6d1d3299266e44b997a948fc94d9c890', 'd53927d31b7b456a81fd7ff3e8f1c7d8', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:08', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('048bfca4694541ada25f29757d64f954', '27b15135642f4153847cb90823e4798f', 'dashboard:02', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:54:32', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('04d1ad21acd54f748ab72f8b0983e856', '27b15135642f4153847cb90823e4798f', 'org:08:03', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:54:32', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('08574fafb6d94e509f0a411dc78a7a70', '27b15135642f4153847cb90823e4798f', 'assembly', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:54:32', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('09ccf2cefbc443ef854791f801e568db', '27b15135642f4153847cb90823e4798f', 'error', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:54:32', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('0b0d0947e41b4deba9d7044051496a3d', '6d1d3299266e44b997a948fc94d9c890', '6cb0f73b409c4ef7ac5dce9793361e07', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:08', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('0b3e7666c59046faac501ba78b0bba8d', '6d1d3299266e44b997a948fc94d9c890', '77dc4349c06e4a4e926290b2252716d2', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:07', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('0c589aa3a4834c45b4f5ef210c435f39', '6d1d3299266e44b997a948fc94d9c890', 'org:07:00', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:52:03', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('0ef6431dbff94511a72b652cf3f4a971', '6d1d3299266e44b997a948fc94d9c890', 'assembly:00', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:52:03', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('0fc0b5d361ba477eaef817b56c55b649', '6d1d3299266e44b997a948fc94d9c890', '9e8afd61092f417fa832daee11bace3a', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:07', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('0fdf1287275e4ee2b878ac05e9408d61', '6d1d3299266e44b997a948fc94d9c890', '4ab8506b8d7d44a595bf3fa1d4bfa9ee', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:07', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('131db60704524ff8a4fbf6e65e260042', '6d1d3299266e44b997a948fc94d9c890', '45362da3c7d34bfab0321d404ab54be5', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:08', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('1757ab1efe854b2fa6ac8bd4dbe91bd4', '6d1d3299266e44b997a948fc94d9c890', '138361d0bc7044c3956fe091f3a9128b', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:07', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('189cf67d877849989d44796f29253c34', '6d1d3299266e44b997a948fc94d9c890', 'org:02', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:52:03', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('18b6bc7de5764637b98f533d96899a46', '6d1d3299266e44b997a948fc94d9c890', '0f987d2175f04a96acb05e0c317a57ed', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:07', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('18e58e7a046c4ae7a4f65e9cdc6f4261', '6d1d3299266e44b997a948fc94d9c890', 'f2a55255154e41e1aa9fe12c799fde0d', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:07', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('1989699740774600b5eb44aba33e1d18', '6d1d3299266e44b997a948fc94d9c890', 'org:05', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:52:03', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('1a6ea6edce8b412fa9329fc689d77491', '6d1d3299266e44b997a948fc94d9c890', 'ca360262267a47f0a886dffce9ed9e6a', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:08', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('1cb5ce1c11794976a372d9c8463f7dc2', '6d1d3299266e44b997a948fc94d9c890', 'org:01', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:52:03', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('1fc6c61fc96043529a91373e182afd92', '6d1d3299266e44b997a948fc94d9c890', '935e8d57e97b468098159d9f4dd42b71', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:07', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('216ee6edb7974d99ac247c168d053464', '6d1d3299266e44b997a948fc94d9c890', '642b4bdbcf004c96bd778327f9831e4e', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:07', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('21ed359f7bbf4ad0993257622738def2', '6d1d3299266e44b997a948fc94d9c890', 'cde32932ef144479a437529b05963fe5', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:08', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('23cf95cea4004e1592921a81f08a0ba5', '6d1d3299266e44b997a948fc94d9c890', '0778dde0fcb943dfa454305c23d56fe4', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:07', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('26d443f9b5df42c8be645bd8d3ae4c59', '6d1d3299266e44b997a948fc94d9c890', '6f5bcabe2c5540ccbd9712f835245115', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:07', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('298a236847cb4dbaaa2274d7e45eaded', '27b15135642f4153847cb90823e4798f', 'org:04', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:54:32', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('2ab512afacd24e5e855eee2576329212', '6d1d3299266e44b997a948fc94d9c890', '78572dd835604cdd9eb4efbf562dd804', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:07', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('2bc1c4ae3ab04d7187e6f64dd8b7642a', '27b15135642f4153847cb90823e4798f', 'org:01:01', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:54:32', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('2ec4802ac0d7401ea763100684bd0003', '6d1d3299266e44b997a948fc94d9c890', 'error', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:52:03', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('2f13e10887a94737b1dabee31382a4bc', '27b15135642f4153847cb90823e4798f', '4b0fffab36ae40bf97c3f64b46f24558', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:26', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('30cd4102bd9e48c396d4b680552a4a51', '6d1d3299266e44b997a948fc94d9c890', '06532df903114f699387c217ad081459', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:08', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('31839161f95e45df9bdf1b1c1a513335', '6d1d3299266e44b997a948fc94d9c890', 'org:01:00', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:52:03', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('33208fa18f3945bf8b3d4561db5f5867', '27b15135642f4153847cb90823e4798f', 'org:05:00', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:54:32', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('334e78436b4247a3a7f4cf189ab9fc7b', '27b15135642f4153847cb90823e4798f', '7834e8fd2fed4de6895f4015df03e45e', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:26', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('33a94f38329545b1abccd05fe4af65d8', '6d1d3299266e44b997a948fc94d9c890', '7806e6ee9d064fe58c2f75a2cfe24f36', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:07', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('3475ea9f1a6a47e6854ed148d7875056', '6d1d3299266e44b997a948fc94d9c890', 'org', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:52:03', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('3481479224284720b686f2d3d1461c77', '27b15135642f4153847cb90823e4798f', 'c3efe551805b4dc9b265c02d73c5b4f5', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:26', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('38ffaa1f4654488ba35dc8795fc16705', '6d1d3299266e44b997a948fc94d9c890', '7f2ec445d1d1487593b0ea1d9f4dcb29', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:08', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('3914396cc75d4b25a96750a003fdb169', '6d1d3299266e44b997a948fc94d9c890', 'c3efe551805b4dc9b265c02d73c5b4f5', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:07', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('3c9ec594d3f0482abb5421d2203fcd5e', '6d1d3299266e44b997a948fc94d9c890', '90cb9f3c7b7c4796b7e07496a5a5598b', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:07', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('3f5c88dfddbe4e7285634716935824f8', '6d1d3299266e44b997a948fc94d9c890', '5501985060e54dacaeeb6f4ec1a4f67d', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:08', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('3fdc3fc709714eaeb2cd375d3e8fc5d0', '6d1d3299266e44b997a948fc94d9c890', '8e83532fc6d04c7b954cdda34fd7a022', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:07', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('41f7d342fb994f9d9602480065f045a0', '6d1d3299266e44b997a948fc94d9c890', 'org:02:00', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:52:03', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('469122c7d53e47668df658cef6be9b54', '6d1d3299266e44b997a948fc94d9c890', '7b4505c829d147f5b8f5a95192054f23', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:07', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('492863dbd526401e8478dc5906422aea', '6d1d3299266e44b997a948fc94d9c890', 'assembly:01', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:52:03', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('499ab0bbd4794346b60ba3e34e26ecbf', '6d1d3299266e44b997a948fc94d9c890', '797f2f8db7a44e40895dfa43c8ee3ace', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:07', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('4ab5ad254233434ea74da965842f386c', '6d1d3299266e44b997a948fc94d9c890', 'bed3a070ca3d40e790a9eb171d506e13', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:07', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('4c6eebd0bad043ba8ad5bd93404fc8b8', '6d1d3299266e44b997a948fc94d9c890', 'dashboard:02', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:52:03', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('4e1120d56966436b91e438f957e110d7', '27b15135642f4153847cb90823e4798f', 'org:01', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:54:32', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('50152ff20982414a8619d1c0b27d7b13', '6d1d3299266e44b997a948fc94d9c890', 'org:04:01', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:52:03', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('510610c1f37a4ca8ada3aa602ed7fbe0', '6d1d3299266e44b997a948fc94d9c890', '3ae4c4cf176f40cdad4e6d2846a4c40c', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:07', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('53ed72fcc70b4b7f8525ee214f1f2f33', '6d1d3299266e44b997a948fc94d9c890', 'assembly', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:52:03', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('58267aad42c04b0981e46263c022c375', '6d1d3299266e44b997a948fc94d9c890', 'org:05:00', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:52:03', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('588268e0def74275b05d2318b7f31463', '27b15135642f4153847cb90823e4798f', '89f25774306942ac97ecec6ef298fa6b', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:26', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('5958d2dc173f47b5b42612f2391c92ba', '6d1d3299266e44b997a948fc94d9c890', '06a12f5ef11f40f295341d72f364151b', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:08', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('59659a98f50640fd91ce9ba72c8dd219', '6d1d3299266e44b997a948fc94d9c890', 'd2b657bd09b34bb786ef7a32ad3d7370', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:07', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('5b5f94c1acec4743a7dc8c54907907ea', '6d1d3299266e44b997a948fc94d9c890', '40745a8383e64b4ca04b61120f1231b1', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:07', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('6305debb389144df94731326d12be294', '6d1d3299266e44b997a948fc94d9c890', '41346cff23b44a769565d28b2a18f7e4', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:08', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('6653ee43c58e4dc3918aacde1705faba', '6d1d3299266e44b997a948fc94d9c890', 'dashboard:00', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:52:03', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('68021e8c2ecb45cbabc45846eee9c609', '27b15135642f4153847cb90823e4798f', 'org:01:00', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:54:32', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('6b2407b66ac34079b8dc313191c0b5f4', '6d1d3299266e44b997a948fc94d9c890', 'd76a1f1428d34b6ba168f4f474616f69', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:07', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('6b5ca04c4aee42eab0e4eb775e8e87e3', '6d1d3299266e44b997a948fc94d9c890', 'd3329bc3db3f4ac0892a7b26531bfdbc', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:08', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('6cde4a90748a433e9afbe031bbed07d9', '27b15135642f4153847cb90823e4798f', '35f19436148a4b5dbbcee46a5d9c2f17', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:26', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('6dc323d885ec4c1d968f442e0b24b221', '27b15135642f4153847cb90823e4798f', '8a98927341314b97ae0669ca84fce977', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:26', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('6ea1d961bf3a4952836e9a21cef0197c', '27b15135642f4153847cb90823e4798f', 'f2a55255154e41e1aa9fe12c799fde0d', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:26', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('720dcf35c98b4a77974a64b76075848e', '6d1d3299266e44b997a948fc94d9c890', 'e398542886de4336919e9f7b4157f523', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:07', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('7291d03670ba45fb93c4ad800a067bb6', '6d1d3299266e44b997a948fc94d9c890', 'error:02', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:52:03', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('72f6b166d3474ae6a8e6bc2354e908b4', '27b15135642f4153847cb90823e4798f', 'dashboard', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:54:32', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('739005ad1f2c486ea4e2196a956430c8', '27b15135642f4153847cb90823e4798f', 'org:08:00', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:54:32', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('76251063a14141d6a1d9f1f7baf9745c', '6d1d3299266e44b997a948fc94d9c890', 'org:00:01', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:52:03', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('7969198b48454d2ab6821acaaddabe57', '6d1d3299266e44b997a948fc94d9c890', 'error:01', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:52:03', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('7b2fd4252e6b43dc88d3902d8d21bcc4', '6d1d3299266e44b997a948fc94d9c890', '810f55bf1582478c949c63f30795fb6e', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:07', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('7cf6e66422e349b1afd402b593e0f075', '6d1d3299266e44b997a948fc94d9c890', 'dashboard:01', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:52:03', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('7d2e47180b0c41d89f27b617d43dabdb', '6d1d3299266e44b997a948fc94d9c890', '8a98927341314b97ae0669ca84fce977', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:08', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('7eca4535290147de9a30f729e06473bc', '6d1d3299266e44b997a948fc94d9c890', 'c53ddcbf655c46be8ae0f8aced495444', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:07', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('82a92f94db1a4519b3d4e6b819233f17', '6d1d3299266e44b997a948fc94d9c890', 'org:00:02', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:52:03', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('8300aa9bb0274c6d9a8f49cfddf123c1', '6d1d3299266e44b997a948fc94d9c890', '5992bedb31ad4f2383c10bb6aabbd77f', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:08', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('8312ec349850468ea53387ef241b076c', '6d1d3299266e44b997a948fc94d9c890', '5f3a343ad76e40109aaba20d87e23dff', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:07', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('860f76d238b6417f84d50f21d68356fc', '6d1d3299266e44b997a948fc94d9c890', '89f25774306942ac97ecec6ef298fa6b', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:07', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('895f1eea0079462da3de33bc93766484', '27b15135642f4153847cb90823e4798f', 'error:00', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:54:32', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('8cabfcbc8a1e41c385bd16245e6e8518', '6d1d3299266e44b997a948fc94d9c890', 'bd2ac7059e0747688bd5824fbb300478', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:08', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('8d394de443cd4ad2a47bc43116bb173d', '6d1d3299266e44b997a948fc94d9c890', '943073c6900e41b8b5f7bba5197c4827', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:07', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('8eca133771fc4a47885ac428e9544d6f', '27b15135642f4153847cb90823e4798f', 'assembly:01', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:54:32', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('90b9893781534681a145a22a1fb0f639', '6d1d3299266e44b997a948fc94d9c890', 'org:04', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:52:03', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('90c0b437491a42cf9913e57e917b8814', '6d1d3299266e44b997a948fc94d9c890', 'org:01:01', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:52:03', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('92f7fc11d0e145cb8e4b28750dd2d66e', '6d1d3299266e44b997a948fc94d9c890', '12ed5cebbc2147e98dee76a5a5cae4fc', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:08', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('93ecc54aca524e77b0b38ffd675662d8', '6d1d3299266e44b997a948fc94d9c890', 'a6eea37b72344b928c42ce62a015d86b', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:07', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('94157d21c3f44e82821139c522a6364c', '27b15135642f4153847cb90823e4798f', 'assembly:00', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:54:32', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('944db7e2232345a19c0fedcb9f0272f6', '6d1d3299266e44b997a948fc94d9c890', '71b7946aef2b4488b6bd73558fd8b46c', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:08', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('94d69cf4165c48c78665f4c41cebd7f5', '6d1d3299266e44b997a948fc94d9c890', '2d3eb0a4d5964e3683b8108549c1ff9b', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:07', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('9655b3bdfc0f416494dc31baff0eab91', '27b15135642f4153847cb90823e4798f', 'org:07:00', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:54:32', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('9d9d0662fde94c84bf1ddc2dd6bda3b4', '27b15135642f4153847cb90823e4798f', 'org:04:00', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:54:32', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('9e5680f87e2648959e52f7c8f66b7659', '6d1d3299266e44b997a948fc94d9c890', 'f33eca367cab417eb8ecf72c88495324', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:08', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('9fa886ef39814b489127274a1f82ce94', '6d1d3299266e44b997a948fc94d9c890', 'b6af52e4ed4945dea16998f0e51b95c0', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:08', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('a0c4ddd3b5ae4359b3bde6ea55667e89', '6d1d3299266e44b997a948fc94d9c890', 'fd336584126e4c1b8053316e4549b7a7', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:08', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('a22aebba24ee4cdf82284770dee7a830', '27b15135642f4153847cb90823e4798f', 'aa49a7b5ef4d46dd9e25b0962129082e', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:26', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('a251309cb21f499a971976270fb4f0af', '6d1d3299266e44b997a948fc94d9c890', 'e5dcdfdcd1d24dd4a2f6728a24423344', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:07', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('a31eab721f0745128cc79e032847ee58', '27b15135642f4153847cb90823e4798f', 'org', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:54:32', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('a69a5ee7111f4c99b2ebe7d4c3107904', '27b15135642f4153847cb90823e4798f', 'org:08:02', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:54:32', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('a7032668d4724bb5a895329fa494cae3', '6d1d3299266e44b997a948fc94d9c890', 'a9e9f4c6497b45ebad083577ed1b1b79', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:07', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('a7d8d9200b0d42a7ab15817baa94418c', '27b15135642f4153847cb90823e4798f', 'dashboard:01', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:54:32', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('a8909790257d4c31a2136a17ad5bd89a', '6d1d3299266e44b997a948fc94d9c890', 'org:08:00', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:52:03', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('a8ac0cbbb51848c785351a201f8fd186', '27b15135642f4153847cb90823e4798f', 'error:02', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:54:32', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('a99c44ff23fb4e0dad790d180b2da12b', '6d1d3299266e44b997a948fc94d9c890', 'ff4227a05d84411ab0527ed8858a1075', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:07', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('aa5eb208297f482598738725d2409bda', '6d1d3299266e44b997a948fc94d9c890', '8e7fac772c4a42098a18c1701cdc6590', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:07', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('ac586156def4453a8b28fe77d5562709', '6d1d3299266e44b997a948fc94d9c890', 'aa49a7b5ef4d46dd9e25b0962129082e', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:07', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('affa693534e445f38e0fb1e161afe4a7', '6d1d3299266e44b997a948fc94d9c890', 'org:08:03', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:52:03', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('b0b3af5d76e54022902f3bb18c4281a2', '6d1d3299266e44b997a948fc94d9c890', '0a008bacf58a4110b6e946552ba3ed14', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:08', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('b30a1156a24b48bcabdf1ee04eb68ab8', '6d1d3299266e44b997a948fc94d9c890', 'fc735d17ef85434cbb65b31500f2dbda', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:08', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('b5942d2d22af4c6f9794d9b52715b8c7', '6d1d3299266e44b997a948fc94d9c890', '7dbd44ebe6eb47b6a8e107195032dcb4', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:08', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('b652d54980af4b468779163438bbdcd2', '6d1d3299266e44b997a948fc94d9c890', '8aab45d641a54604a393dfe1005991bc', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:07', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('b6940e3a5a7e4c8d893796602fe3afc0', '6d1d3299266e44b997a948fc94d9c890', 'ad833b153f084add834d3e7e007b7996', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:08', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('b6992e836f8d4fa28984a0d76bbe1ec8', '6d1d3299266e44b997a948fc94d9c890', 'org:08', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:52:03', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('b7b2ce98b60f400dbc57934c5a7922a1', '6d1d3299266e44b997a948fc94d9c890', 'org:04:00', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:52:03', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('b849e15d038641b4a2ea71ffecff0c48', '6d1d3299266e44b997a948fc94d9c890', 'e078f6cfb5a64fc6904d18420438379e', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:08', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('bb0b949a11954d488840f2064fa2d025', '6d1d3299266e44b997a948fc94d9c890', '08fc4af6db674b32bd986b28f6f4d9fd', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:08', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('bc79f9f7ffb0447aa945e317f2405f71', '27b15135642f4153847cb90823e4798f', 'org:02:01', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:54:32', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('bf2cf14802a948a690d30857ac90912a', '27b15135642f4153847cb90823e4798f', '1b79c5a50c904de189fb1bb6262ecc18', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:26', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('c0d99a29e3f94301aab5733f5c4cc1a8', '27b15135642f4153847cb90823e4798f', 'a9e9f4c6497b45ebad083577ed1b1b79', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:26', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('c4ea8281afbc4613bd3e0066e865ba86', '27b15135642f4153847cb90823e4798f', 'org:04:01', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:54:32', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('c538788139724587a7c625ffe6d24dc2', '6d1d3299266e44b997a948fc94d9c890', 'error:00', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:52:03', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('c5ef38c983ef4395b444cc52b9e330a3', '6d1d3299266e44b997a948fc94d9c890', 'ef38f5834a664a1c900934ee41c1b90a', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:07', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('c97f7854cba9408d9c82fff76dd310eb', '6d1d3299266e44b997a948fc94d9c890', 'c2a355de5f96407aa27c3ece3891db53', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:07', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('ca07a1b3c6fe4449a42e17a91ff03be8', '6d1d3299266e44b997a948fc94d9c890', 'bcdb01d804874974b89d895a23540328', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:07', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('cbebf16f69524fe495a400f7dbc1e5b6', '6d1d3299266e44b997a948fc94d9c890', 'd7da85594ad44d12bbfc212b3cfabd9f', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:08', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('cdebba1b59544ac8947ede0ffea8c379', '6d1d3299266e44b997a948fc94d9c890', 'org:00:03', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:52:03', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('cdfee9ffe8be45c6a1603dd658a8452a', '6d1d3299266e44b997a948fc94d9c890', '4b0fffab36ae40bf97c3f64b46f24558', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:07', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('d0cd575e4c59461a872c1794b3585240', '6d1d3299266e44b997a948fc94d9c890', 'org:07', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:52:03', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('d1dd1327a5d04e7ba3262798272038c1', '6d1d3299266e44b997a948fc94d9c890', '8cca0845375e4ffab75c0210c1083234', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:07', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('d22a25da982c4defa31befaedf439ec9', '6d1d3299266e44b997a948fc94d9c890', '81dcb6279b3e48a89350ee8bdc241e1f', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:08', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('d2a6dd71c1d4475da6098c1a63f3dd13', '6d1d3299266e44b997a948fc94d9c890', '1b79c5a50c904de189fb1bb6262ecc18', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:07', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('d4bd62aa0b984a7ba54de826fc87d80f', '27b15135642f4153847cb90823e4798f', 'org:00:02', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:54:32', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('d95098764bc34b38bf17215440cd1712', '27b15135642f4153847cb90823e4798f', 'dashboard:00', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:54:32', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('da5bf430f52d44a58f153b8795880e6a', '27b15135642f4153847cb90823e4798f', '7dbd44ebe6eb47b6a8e107195032dcb4', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:26', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('dd045c07f0a145b3b71422791d3a1949', '27b15135642f4153847cb90823e4798f', 'org:00', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:54:32', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('dd0709221a4f4176945decf6edf1eda6', '27b15135642f4153847cb90823e4798f', '71b7946aef2b4488b6bd73558fd8b46c', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:26', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('de9ca1617539451b9db4a1ce32f743e2', '6d1d3299266e44b997a948fc94d9c890', '39f540ab47e94a1392bb89621997f424', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:08', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('df9a89e610ba44a5a8e89ab9a058ae91', '6d1d3299266e44b997a948fc94d9c890', 'be9446d734c84d9884c7eebd4427733f', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:07', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('e003aa9b0be843b1b78ead50a137a513', '6d1d3299266e44b997a948fc94d9c890', 'e2cba158d6424765b886e29123183386', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:07', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('e160e4e8cc6a47aa9ac83a79a71514ef', '6d1d3299266e44b997a948fc94d9c890', 'a70721bc4ef04ba3a5b55455aecfea08', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:07', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('e3b594a71bad442e867cbfd14dcb3890', '6d1d3299266e44b997a948fc94d9c890', 'dashboard', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:52:03', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('e3e738a84494477d8c035419b38b4ed5', '6d1d3299266e44b997a948fc94d9c890', 'org:02:01', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:52:03', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('e5d7f803d4df4206823042e3be0b519f', '6d1d3299266e44b997a948fc94d9c890', '575f3feb6c644c049d320540f7d7753a', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:08', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('e64840f33ee44c7ab0f33ce144498768', '6d1d3299266e44b997a948fc94d9c890', 'a2793ab8a44447eb9980fbf0a0bdcee3', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:07', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('e7cd3d9e018249b3a5885ffc51847f24', '27b15135642f4153847cb90823e4798f', 'org:00:01', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:54:32', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('e7fe524247d64bc2aa5865339d35ba35', '6d1d3299266e44b997a948fc94d9c890', 'a928ff7d42404e71a8456c56fd51a9a8', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:07', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('e89b115a599f4801b4d27210f9d798a9', '6d1d3299266e44b997a948fc94d9c890', '2d77826b327944cd808db15b52ac8446', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:07', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('e96ab6069ea34046955dbfe6ef256dbc', '6d1d3299266e44b997a948fc94d9c890', 'd8e49cedf2ca43e18a4a16bf6e1af28e', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:07', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('e995f975ad44497f88df6211a92fa9f7', '27b15135642f4153847cb90823e4798f', '935e8d57e97b468098159d9f4dd42b71', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:26', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('eb7f4c0ec4ca434997a35132d4730cb0', '27b15135642f4153847cb90823e4798f', 'd7da85594ad44d12bbfc212b3cfabd9f', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:26', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('f236fe01adde4b91b1438f1558f95123', '6d1d3299266e44b997a948fc94d9c890', '35f19436148a4b5dbbcee46a5d9c2f17', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:08', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('f2a2d4da01ce43e5ab9e771563442458', '27b15135642f4153847cb90823e4798f', 'org:07', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:54:32', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('f3769b90f3b84c349e0add5db5a955a2', '27b15135642f4153847cb90823e4798f', 'org:05', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:54:32', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('f4bf905e48854e3a89c16515042d8afc', '6d1d3299266e44b997a948fc94d9c890', '7834e8fd2fed4de6895f4015df03e45e', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:08', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('f51b6686544441329e404bb60f11dc71', '27b15135642f4153847cb90823e4798f', 'org:02:00', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:54:32', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('f559b1de7c6b4f538a4f62661f27137b', '6d1d3299266e44b997a948fc94d9c890', '6997931b9c28472d9a58a26c97707e41', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:08', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('f581bc74c3ca4d7a991d36f58fd7f744', '6d1d3299266e44b997a948fc94d9c890', '7ad7f1e10c1543a9a98f8ae105410a8f', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:07', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('f5bc03596dc94e85b2bc7ac3f443e945', '6d1d3299266e44b997a948fc94d9c890', 'org:08:02', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:52:03', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('f6e6febb273a4def9758540f00c6e1f4', '6d1d3299266e44b997a948fc94d9c890', 'org:00', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:52:03', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('f9024fdea36e4755bf112f8223af2291', '27b15135642f4153847cb90823e4798f', 'org:02', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:54:32', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('fab0baaf59a348aaa09757f7d7df4dc4', '6d1d3299266e44b997a948fc94d9c890', '0a3bcd49ed664423ac7f966c545ea747', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:07', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('fc2dfd5e50ee4d02b1ee4694cff03b0c', '27b15135642f4153847cb90823e4798f', 'org:08', 'USE', 'MENU', 'jiubanqingchen@2020', '2021-07-08 02:54:32', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('fc8e7f885b0642229062a5a95ccf0608', '6d1d3299266e44b997a948fc94d9c890', '4e8cdcde56c64996a62a67c04efa87e9', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:07', NULL, NULL);
INSERT INTO `wish_resource_authority` VALUES ('fd80d7cbfaa146abad9028007e690ce7', '6d1d3299266e44b997a948fc94d9c890', 'f4b9f9e3a2874a0e90cbe1b7679d1f52', 'USE', 'INTERFACE', 'jiubanqingchen@2020', '2021-07-08 08:08:07', NULL, NULL);

-- ----------------------------
-- Table structure for wish_role
-- ----------------------------
DROP TABLE IF EXISTS `wish_role`;
CREATE TABLE `wish_role`  (
  `roleId` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色id',
  `roleName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色名称',
  `roleType` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色类型',
  `roleLevel` int NULL DEFAULT NULL COMMENT '角色等级',
  `roleDescription` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色描述',
  `isValid` tinyint(1) NULL DEFAULT NULL COMMENT '能否修改',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `createPerson` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `modifyTime` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `modifyPerson` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`roleId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of wish_role
-- ----------------------------
INSERT INTO `wish_role` VALUES ('27b15135642f4153847cb90823e4798f', '演示角色', 'BUSINESS_ROLE', NULL, '演示环境使用角色', 1, '2021-04-30 03:02:46', 'jiubanqingchen@2020', NULL, NULL);
INSERT INTO `wish_role` VALUES ('6d1d3299266e44b997a948fc94d9c890', '管理员', 'MANAGER_ROLE', 5, '管理角色，拥有所有权限的增删改功能', 1, '2021-04-16 09:04:58', 'jiubanqingchen@2020', '2021-04-25 03:19:09', 'jiubanqingchen@2020');

-- ----------------------------
-- Table structure for wish_user
-- ----------------------------
DROP TABLE IF EXISTS `wish_user`;
CREATE TABLE `wish_user`  (
  `userId` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户id',
  `userAccount` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户账号',
  `userName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户姓名',
  `userPhone` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户电话',
  `userPassword` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户密码',
  `userEmail` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户邮箱',
  `userBrithday` date NULL DEFAULT NULL COMMENT '用户生日',
  `userSex` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户性别',
  `userAvatar` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户头像id',
  `introduce` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '介绍',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `createPerson` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `modifyTime` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `modifyPerson` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`userId`) USING BTREE,
  UNIQUE INDEX `useraccount`(`userAccount`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'wihs_user用户表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of wish_user
-- ----------------------------
INSERT INTO `wish_user` VALUES ('01fc6bd6edb54fac85c939590f81a51c', 'demo', '演示账号', '17764131757', 'fxaD4+zKTI/8zIZ+gPRmZMvnF3F04DPN/dLZBoeZuHprAXbHhVcqEOi2uNAdpBByWndcUGCT0sf0mu3an/Y3VyGMWJxB9YLeI+6VqTbJu1hVw1a+Ux30QXRdsrMph8PxP67THitrCz2lBvYkbql16aKL8oK3aUthyC9hRCq2Ux0=', 'demo@qc.com', '1998-11-27', 'MAN', '1ef2bade56f2440487200910e2fa9412', '演示账号', '2021-01-26 11:21:12', 'jiubanqingchen@2020', '2021-07-08 03:03:34', 'jiubanqingchen@2020');
INSERT INTO `wish_user` VALUES ('jiubanqingchen@2020', 'admin', '管理员', '17764149196', 'xNRXd4Y5qxZHAIvp2vvRytYlioIVrw4DC0L0gMnG1rqV/3+matqsU6FToBfvk4a44XPHA3stiPDoiLmewd1KQrqU8e5fuS/X4+EFF4NVFjFhlBxn6cHvvx0i9DXeRokBCw3CDGD/edgO2DCJJdfl3QgQgRK9hc7r0A0PwdCd8IA=', '577955659@qq.com', '1998-12-13', 'MAN', '59e77891aa1148a7b3abda43ef19a2a8', NULL, '2020-12-31 09:30:16', NULL, '2021-07-08 03:00:16', 'jiubanqingchen@2020');

-- ----------------------------
-- Table structure for wish_user_dept
-- ----------------------------
DROP TABLE IF EXISTS `wish_user_dept`;
CREATE TABLE `wish_user_dept`  (
  `userDeptId` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户部门id',
  `userId` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户id',
  `deptId` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '部门id',
  `main` tinyint(1) NULL DEFAULT NULL COMMENT '是否主要部门',
  `valid` tinyint(1) NULL DEFAULT NULL COMMENT '是否有效',
  `timeEnd` date NULL DEFAULT NULL COMMENT '失效时间',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `createPerson` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `modifyTime` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `modifyPerson` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`userDeptId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户部门关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of wish_user_dept
-- ----------------------------
INSERT INTO `wish_user_dept` VALUES ('11610c16d1c74e24a8916f26c58fc61a', '01fc6bd6edb54fac85c939590f81a51c', 'dept:child1', 1, 1, NULL, '2021-04-30 03:11:49', 'jiubanqingchen@2020', '2021-04-30 03:11:52', 'jiubanqingchen@2020');
INSERT INTO `wish_user_dept` VALUES ('a86ad2a715c4439ea2af73c342a898f8', 'jiubanqingchen@2020', 'qc', 1, 1, '2029-03-25', '2021-03-26 09:38:17', 'jiubanqingchen@2020', '2021-04-15 22:53:11', 'jiubanqingchen@2020');

-- ----------------------------
-- Table structure for wish_user_group_role
-- ----------------------------
DROP TABLE IF EXISTS `wish_user_group_role`;
CREATE TABLE `wish_user_group_role`  (
  `userGroupRoleId` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色用户关联id',
  `roleId` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色id',
  `userGroupId` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户id或者部门id',
  `userGroupType` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '关联类型',
  `createPerson` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `modifyPerson` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `modifyTime` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`userGroupRoleId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户角色关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of wish_user_group_role
-- ----------------------------
INSERT INTO `wish_user_group_role` VALUES ('7c92faa9711743bebd1d0dc43e1e9167', '27b15135642f4153847cb90823e4798f', '01fc6bd6edb54fac85c939590f81a51c', 'USER', 'jiubanqingchen@2020', '2021-04-30 03:02:56', NULL, NULL);
INSERT INTO `wish_user_group_role` VALUES ('8080fdcc4e7140cab38182402fbda8fa', '6d1d3299266e44b997a948fc94d9c890', 'jiubanqingchen@2020', 'USER', 'jiubanqingchen@2020', '2021-04-16 23:24:09', NULL, NULL);

-- ----------------------------
-- Table structure for wish_cache
-- ----------------------------
CREATE TABLE `wish_cache` (
    `cacheId` varchar(32) NOT NULL COMMENT '缓存id',
    `cacheDesc` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '缓存描述',
    `cacheName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '缓存名称',
    `cacheExpression` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '缓存表达式',
    `sortNo` int NOT NULL COMMENT '排序',
    `createPerson` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '创建人',
    `createTime` datetime DEFAULT NULL COMMENT '创建时间',
    `modifyPerson` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '更新人',
    `modifyTime` datetime DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`cacheId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='缓存管理';

-- ----------------------------
-- Function structure for getDictionaryDesc
-- ----------------------------
DROP FUNCTION IF EXISTS `getDictionaryDesc`;
delimiter ;;
CREATE FUNCTION `getDictionaryDesc`(`_codeItemId` varchar(50),`_codeId` varchar(32))
 RETURNS varchar(500) CHARSET utf8
BEGIN
	DECLARE returnval VARCHAR(50) DEFAULT '';
	select codeName into returnval  from wish_dictionary where codeItemId=_codeItemId and codeId=_codeId;
	RETURN returnval;
END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
