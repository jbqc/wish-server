# wish-server

#### 介绍
wish-server 是一个基于jfinal框架的简单、快速的java开源项目，是一个能急速开发一个业务表的增删改查，导出excel等功能对的低代码平台，支持到用户接口级的权限管理（RBAC），支持国际化，定时任务等常用功能

#### 软件架构

##### 前端代码

[wish-web](https://gitee.com/jbqc/wish-web)

##### 演示环境

[https://web.jiubanqingchen.cn/](https://web.jiubanqingchen.cn/)  demo 123456

##### 项目成分

[JFinal](https://jfinal.com/) [JFinal-undertow](https://gitee.com/jfinal/jfinal-undertow) [Mysql](https://www.mysql.com/) [Druid](https://github.com/alibaba/druid) [Redis](https://redis.io/) [hutool](https://hutool.cn/) [fastjson](https://github.com/alibaba/fastjson) [Easyexcel](https://www.yuque.com/easyexcel) [quartz](http://www.quartz-scheduler.org/) [jsoup](https://jsoup.org/)

##### 系统需求

- JDK >= 1.8
- MySQL >= 8.0（5.x版本也行，不过我没用5.x版本了）
- Maven >= 3.0

##### 功能模块

- 用户管理
- 角色管理
- 部门管理
- 菜单管理
- 数据字典管理
- 定时任务
- 国际化
- 登录日志
- 代码生成(这个功能独立出去了一个项目 [wish-code-tool-server](https://gitee.com/jbqc/wish-code-tool-server))


#### 安装教程

1.  新建数据库wishdb(自己随意),执行docs目录下的wishdb_init.sql
2.  修改resource目录下的application.server.properties(数据源配置文件)

```properties
wish.datasource.jdbcUrl=jdbc:mysql://ip:port/wishdb?characterEncoding=utf8&useSSL=false&zeroDateTimeBehavior=convertToNull
wish.datasource.user=
wish.datasource.password=
```

1.  使用的web容器是undertow，直接运行com.jiubanqingchen.publish包下的Run文件即可
2.  如果需要修改端口信息请修改undertow.txt中的配置

#### 使用说明

如果你还不熟悉JFinal框架，那么我建议你先阅读一下[jFinal](https://jfinal.com/doc)的文档

wish-server主要由Controller,Serverce,Model(这个由jFinal的生成器生成)，还有sql模板文件组成

假设我们需要开发如下一个单表

```mysql
CREATE TABLE `wish_demo` (
  `demoId` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `demoName` varchar(100) DEFAULT NULL COMMENT '演示名称',
  `demoDesc` varchar(255) DEFAULT NULL COMMENT '演示描述',
  `demoDate` datetime DEFAULT NULL COMMENT '演示时间',
  `demoPerson` varchar(32) DEFAULT NULL COMMENT '演示人',
  `createPerson` varchar(32) DEFAULT NULL COMMENT '创建人',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `modifyPerson` varchar(32) DEFAULT NULL COMMENT '修改人',
  `modifyTime` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`demoId`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='演示';
```

我们需要有如下几个文件

- DemoController

```java
package com.jiubanqingchen.org.demo;

import com.jfinal.core.Path;
import AuthorityKey;
import ModelController;

/**
 * @author light_dust_generator
 * @since 2021-06-11 16:06:31
 */
@Path("/org/demo")//这个是jfinal提供的路由注解
@AuthorityKey("org:demo")//这个是用来做权限控制的注解
public class DemoController extends ModelController<DemoService, Demo> {
    //你可以什么都不做就能使用增删改查，导出excel等功能 这些操作在ModelController已经完成了 当然你也可以自己重写
}
```

- DemoService

```java
package com.jiubanqingchen.org.demo;

import SqlKey;
import ModelService;

/**
 * @author light_dust_generator
 * @since 2021-06-11 16:06:31
 */
@SqlKey("org_demo.list")//这个是获取查询sql(包括分页查询、查询所有、主键查询)的注解,org_demo.list的来源在下文中会体现出来
public class DemoService extends ModelService<Demo> {
    //与DemoController一样你可以什么都不做 也可以根据自己的业务重写
}
```

- demo.sql

```mysql
###@author light_dust_generator
###@since 2021-06-11 16:06:31
#sql("list")
SELECT t1.* FROM wish_demo as t1 WHERE 1=1
    #@DemoWheres()
#end

###查询条件
#define DemoWheres()
    #if(demoId)
   		AND t1.demoId=#para(demoId)
    #end
    #if(demoName)
    	AND t1.demoName #paraLike(demoName)
    #end
    #if(demoDesc)
    	AND t1.demoDesc #paraLike(demoDesc)
    #end
#end
```

- index.sql

我们需要在index.sql中将demo.sql定义出来，以方便系统调用，这个使用的是jfinal的模板语法

```mysql
#namespace("org_demo")###上文中的org_demo.list来源于这里的定义
	#include("demo.sql")
#end
```

为什么需要定义在index.sqld,因为我们在config中做了定义

```java
/**
 * 配置WishArp
 *
 * @param wishArp 默认数据源
 */
protected void configWishArp(ActiveRecordPlugin wishArp) {
    WishAdminMappingKit.mapping(wishArp);
    wishArp.addSqlTemplate("org/sql/index.sql");
    wishArp.setShowSql(prop.getBoolean("showSql", true));
    Engine engine = wishArp.getEngine();
    engine.setCompressorOn(' ');//设置sql 空格优化
    engine.addDirective("paraLike", ParaLikeDirective.class);
}
```

JFinal生成器生成的BaseDemo.java Demo.java文件这里就不做展示了

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

6.  https://gitee.com/gitee-stars/)
